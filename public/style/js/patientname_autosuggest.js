$(document).ready(function () {
    var name = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('op_patientname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/patientname?patientname=%DESC',
            wildcard: '%DESC'
        }
    });
    name.initialize();


    $('#name').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'name',
        displaykey: 'op_patientname_vc',
        source: name.ttAdapter()
    });


});
