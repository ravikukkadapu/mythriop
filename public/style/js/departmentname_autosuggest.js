$(document).ready(function () {
    var DepartmentName = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('dm_deptname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/departmentname?dept=%DESC',
            wildcard: '%DESC'
        }
    });
    DepartmentName.initialize();


    $('#DepartmentName').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'DepartmentName',
        displaykey: 'dm_deptname_vc',
        source: DepartmentName.ttAdapter()
    });


});
