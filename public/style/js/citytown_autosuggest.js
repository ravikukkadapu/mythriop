$(document).ready(function () {
    var city = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('de_city_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/citytownnames?city=%DESC',
            wildcard: '%DESC'
        }
    });
    city.initialize();


    $('#city').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'city',
        displaykey: 'de_city_vc',
        source: city.ttAdapter()
    });


});
