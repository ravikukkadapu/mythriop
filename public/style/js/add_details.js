var mibill = angular.module("mibill", []);
mibill.controller("LicenceDetailsController", function($scope) {
	$scope.licencedetails = [];

	$scope.addLicenceDetails = function(){
		$scope.licencedetails.push({
									'licenceType' : $scope.licenceType,
									'licenceStatus' : $scope.licenceStatus,
									'licenceNo' : $scope.licenceNo,
									'issueDate' : $scope.issueDate,
									'validTill' : $scope.validTill
								});
		// console.log($scope.licencedetails);
		$scope.licenceType = '';
		$scope.licenceStatus = '';
		$scope.licenceNo = '';
		$scope.issueDate = '';
		$scope.validTill = '';
	};

	$scope.removeLicenceItem = function(index){
		$scope.licencedetails.splice(index,1);
	};
});

mibill.controller("CapacityDetailsController", function($scope) {
	$scope.capacitydetails = [];

	$scope.addCapacityDetails = function(){
		$scope.capacitydetails.push({
									'bedType' : $scope.bedType,
									'bedStatus' : $scope.bedStatus,
									'noOfBeds' : $scope.noOfBeds,

								});
		// console.log($scope.capacitydetails);
		$scope.bedType = '';
		$scope.bedStatus = '';
		$scope.noOfBeds = '';

	};

	$scope.removeCapacityItem = function(index){
		$scope.capacitydetails.splice(index,1);
	};
});

mibill.controller("BankDetailsController", function($scope) {
	$scope.bankdetails = [];

	$scope.addBankDetails = function(){
		$scope.bankdetails.push({
									'bankName' : $scope.bankName,
									'accountNo' : $scope.accountNo,
									'ifcsCode' : $scope.ifcsCode,
									'address' : $scope.address,

								});
		// console.log($scope.capacitydetails);
		$scope.bankName = '';
		$scope.accountNo = '';
		$scope.ifcsCode = '';
		$scope.address = '';

	};

	$scope.removeBankItem = function(index){
		$scope.bankdetails.splice(index,1);
	};
});


mibill.controller("FacilityDetailsController", function($scope) {
	$scope.facilitydetails = [];

	$scope.addFacilityDetails = function(){
		$scope.facilitydetails.push({
									'facilityType' : $scope.facilityType,
									'facilityStatus' : $scope.facilityStatus,
									'associateName' : $scope.associateName,
									'tieupcenter' : $scope.tieupcenter,

								});
		// console.log($scope.facilitydetails);
		$scope.facilityType = '';
		$scope.facilityStatus = '';
		$scope.associateName = '';
		$scope.tieupcenter = '';

	};

	$scope.removeFacilityItem = function(index){
		$scope.facilitydetails.splice(index,1);
	};
});


mibill.controller("DoctorDetailsController", function($scope) {
	$scope.doctordetails = [];

	$scope.addDoctorDetails = function(){
		$scope.doctordetails.push({
									'doctorName' : $scope.doctorName,
									'departmentName' : $scope.departmentName,
									'designation' : $scope.designation,
									'specialization' : $scope.specialization,
									'visitType' : $scope.visitType,
									'status' : $scope.status,
								});
		// console.log($scope.facilitydetails);
		$scope.doctorName = '';
		$scope.departmentName = '';
		$scope.designation = '';
		$scope.specialization = '';
		$scope.visitType = '';
		$scope.status = '';
	};

	$scope.removeDoctorItem = function(index){
		$scope.doctordetails.splice(index,1);
	};
});

mibill.controller("RoomDetailsController", function($scope) {
	$scope.roomdetails = [];

	$scope.addRoomDetails = function(){
		$scope.roomdetails.push({
									'roomType' : $scope.roomType,
									'roomStatus' : $scope.roomStatus,
									'noOfRooms' : $scope.noOfRooms,
									'grade' : $scope.grade,

								});
		// console.log($scope.facilitydetails);
		$scope.roomType = '';
		$scope.roomStatus = '';
		$scope.noOfRooms = '';
		$scope.grade = '';

	};

	$scope.removeRoomItem = function(index){
		$scope.roomdetails.splice(index,1);
	};
});


mibill.controller("BillingController", function($scope) {
	$scope.clientdetails = [];

	$scope.addclientDetails = function(){
		$scope.clientdetails.push({
						'allotedclients' : $scope.allotedclients,
								});
		// console.log($scope.facilitydetails);
		$scope.allotedclients = '';
	};

	$scope.removeclientItem = function(index){
		$scope.clientdetails.splice(index,1);
	};
});

//====medical history page
// mibill.controller("AdviseDetailsController",function($scope)
// {
// 	$scope.advisedetails = [];

// 	$scope.addAdviseDetails = function(){
// 		$scope.advisedetails.push({
// 				'diagnosis' : $scope.diagnosis,
// 				'admission' : $scope.admission,
// 				'treatment' : $scope.treatment,
// 		});
// 		console.log($scope.treatment);
// 				$scope.diagnosis = '';
// 				$scope.admission = '';
// 				$scope.treatment = '';
// 	};
// 		$scope.removeAdviseItem = function(index){
// 			$scope.advisedetails.splice(index,1);
// 		};
// });

// mibill.controller("ComplaintController",function($scope)
// {
// 	$scope.complaintdetails = [];

// 	$scope.addComplaintDetails = function(){
// 		$scope.complaintdetails.push({
// 				'diagnosis' : $scope.diagnosis,
// 				'admission' : $scope.admission,
// 				'treatment' : $scope.treatment,
// 		});
// 		console.log($scope.treatment);
// 				$scope.diagnosis = '';
// 				$scope.admission = '';
// 				$scope.treatment = '';
// 	};
// 		$scope.removeComplaintItem = function(index){
// 			$scope.complaintdetails.splice(index,1);
// 		};
// });
