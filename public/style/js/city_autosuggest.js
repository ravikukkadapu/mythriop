$(document).ready(function () {
    var city = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('op_city_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/citynames?city=%DESC',
            wildcard: '%DESC'
        }
    });
    city.initialize();


    $('#city').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'city',
        displaykey: 'op_city_vc',
        source: city.ttAdapter()
    });


});
