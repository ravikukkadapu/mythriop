$(document).ready(function () {
    var doctorName = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('dd_drname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/doctorname?doctor=%DESC',
            wildcard: '%DESC'
        }
    });
    doctorName.initialize();


    $('#doctorName').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'doctorName',
        displaykey: 'dd_drname_vc',
        source: doctorName.ttAdapter()
    });


});
