$(document).ready(function () {
    var category = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('ctm_categoryname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/categorynames?name=%DESC',
            wildcard: '%DESC'
        }
    });
    category.initialize();


    $('#category').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'category',
        displaykey: 'ctm_categoryname_vc',
        source: category.ttAdapter()
    });


});
