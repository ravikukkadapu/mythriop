$(document).ready(function () {
    var area = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('de_area_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/dealersarea?areaname=%DESC',
            wildcard: '%DESC'
        }
    });
    area.initialize();

    $('#area').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'area',
        displaykey: 'de_area_vc',
        source: area.ttAdapter()
    });

});
