$(document).ready(function () {
    var hospname = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('de_agentname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/gethospinfeedback?name=%NAME',
            wildcard: '%NAME'
        }
    });
    hospname.initialize();


    $('#hospname').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'hospname',
        displaykey: 'de_agentname_vc',
        source: hospname.ttAdapter()
    });


});
