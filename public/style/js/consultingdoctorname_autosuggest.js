$(document).ready(function () {
    var consultingdoctor = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('dd_drname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/doctorname?doctor=%DESC',
            wildcard: '%DESC'
        }
    });
    consultingdoctor.initialize();


    $('#consultingdoctor').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'consultingdoctor',
        displaykey: 'dd_drname_vc',
        source: consultingdoctor.ttAdapter()
    });


});
