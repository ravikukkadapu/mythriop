jQuery(document).ready(function () {
    var promocode = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('pcd_promocode_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/promocodes?code=%DESC',
            wildcard: '%DESC'
        }
    });
    promocode.initialize();


    $('#promocode').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'promocode',
        displaykey: 'pcd_promocode_vc',
        source: promocode.ttAdapter()
    });


});
