$(document).ready(function () {
    var investgationname = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('stm_subtestname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mibill/invstDesc?investdesc=%DESC',
            wildcard: '%DESC'
        }
    });
    investgationname.initialize();


    $('#invstDesc').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'invstDesc',
        displaykey: 'tm_testname_vc',
        source: investgationname.ttAdapter()
    });


});
