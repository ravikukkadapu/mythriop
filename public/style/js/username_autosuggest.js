$(document).ready(function () {
    var userName = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('um_username_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mibill/userName?username=%NAME',
            wildcard: '%NAME'
        }
    });
    userName.initialize();


    $('#userName').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'userName',
        displaykey: 'um_username_vc',
        source: userName.ttAdapter()
    });


});
