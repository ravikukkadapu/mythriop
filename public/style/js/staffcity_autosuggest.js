$(document).ready(function () {
    var city = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('sm_city_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/staffcity?staffcityname=%DESC',
            wildcard: '%DESC'
        }
    });
    city.initialize();

    $('#city').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'city',
        displaykey: 'sm_city_vc',
        source: city.ttAdapter()
    });

});
