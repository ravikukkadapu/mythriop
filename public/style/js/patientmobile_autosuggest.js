$(document).ready(function () {
    var phoneno = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('op_contactno_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/mobilenos?num=%DESC',
            wildcard: '%DESC'
        }
    });
    phoneno.initialize();


    $('#phoneno').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'phoneno',
        displaykey: 'op_contactno_vc',
        source: phoneno.ttAdapter()
    });


});
