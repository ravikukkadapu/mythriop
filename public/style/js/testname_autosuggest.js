$(document).ready(function () {
    var testName = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('stm_subtestname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mibill/testname?test=%NAME',
            wildcard: '%NAME'
        }
    });
    testName.initialize();


    $('#testname').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'testname',
        displaykey: 'stm_subtestname_vc',
        source: testName.ttAdapter()
    });


});
