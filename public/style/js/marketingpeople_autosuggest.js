$(document).ready(function () {
    var name = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('sm_name_fl'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/marketingpeople?staff=%DESC',
            wildcard: '%DESC'
        }
    });
    name.initialize();


    $('#name').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'name',
        displaykey: 'sm_name_fl',
        source: name.ttAdapter()
    });


});
