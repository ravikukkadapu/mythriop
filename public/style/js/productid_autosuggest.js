$(document).ready(function () {
    var productid = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('ps_productid_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mibill/getproductid?productid=%Id',
            wildcard: '%Id'
        }
    });
    productid.initialize();


    $('#productid').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'productid',
        displaykey: 'ps_productid_vc',
        source: productid.ttAdapter()
    });

});
