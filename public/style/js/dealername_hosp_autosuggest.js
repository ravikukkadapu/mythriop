$(document).ready(function () {
    var agentname = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('de_agentname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/dealernameshosp?dealer=%DESC',
            wildcard: '%DESC'
        }
    });
    agentname.initialize();


    $('#agentname').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'agentname',
        displaykey: 'de_agentname_vc',
        source: agentname.ttAdapter()
    });


});
