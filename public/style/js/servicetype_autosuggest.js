$(document).ready(function () {
    var marketedBy = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('sm_name_fl'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/staffname?staff=%DESC',
            wildcard: '%DESC'
        }
    });
    marketedBy.initialize();


    $('#marketedBy').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'marketedBy',
        displaykey: 'sm_name_fl',
        source: marketedBy.ttAdapter()
    });


});
