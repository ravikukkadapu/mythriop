$(document).ready(function () {
    var deptname = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('dfm_deptname_vc'),
        queryTokenizer:Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/mythriop/deptfacname?dept=%DESC',
            wildcard: '%DESC'
        }
    });
    deptname.initialize();


    $('#deptname').typeahead({
        hint: true,
        highlight: true,
        minlength: 1
    }, {
        name: 'deptname',
        displaykey: 'dfm_deptname_vc',
        source: deptname.ttAdapter()
    });


});
