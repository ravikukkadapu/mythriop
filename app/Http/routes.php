<?php

use App\OpRegisterdata;
use App\OpTransaction;
use App\DepartmentDetails;
use App\DoctorDetails;
use App\ServiceTypeMaster;
use App\StaffMaster;
use App\QualificationMaster;
use App\UserMaster;
use App\AssociateTariffs;
use App\AgentCardIssueDetails;
use App\DealerMaster;
use App\Http\Requests\NewPasswordRequest;
use App\CategoryMaster;
use App\PromoCodeMaster;
use App\DepartmentFacilities;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



// Prefix all routes with mythriop
Route::group(['prefix' => 'mythriop'], function()
{



Route::get('/', function () {
    return view('easyoppage',['title'=>'Home ']);
});

Route::get('/login',function()
{

    return view('homepage',['title' => 'Login Page']);
});


Route::get('/adminlogin',function()
{
    return view('adminlogin',['title' => 'Admin Login']);
});



Route::get('/admin',function()
{
    return view('admin',['title' => 'Admin']);
});
Route::get('/user',function()
{
    return view('user',['title' => 'Hospital CLinic Page']);
});
Route::get('/busiuser',function()
{
    return view('busiuser',['title' => 'Business Head']);
});

Route::post('forgotpassword','LoginController@forgot');

Route::post('changepassword','LoginController@changepassword');


Route::post('savenewpassword','LoginController@savechangepassword');


// route for get back to login screen(cancel)
    Route::post('loginexit', function(){
        return Redirect::to('/mythriop/login');
    });


    Route::post('dologin','LoginController@dologin');

    // Route::post('admindologin','LoginController@admindologin');

//--op card register project
Route::get('/opregister',function ()
	{

    $value =  Session::get('username');
    $name  = DB::select("select um_associate_vc from user_master where um_username_vc = '$value'");
    $associatename =  $name[0]->um_associate_vc;
    $servicetype = DB::select("select distinct stm_servicetype_vc from servicetype_master where stm_status_vc= 'Active' order by stm_servicetype_vc");

    $cardinfo = DB::select("select distinct cd_servicetype_vc from card_details where cd_agentname_vc= '$associatename' ");

    $district =DB::select("select distinct district_name from district order by district_name");

		return view('opregister', ['title' => 'OP REGISTRATION','servicetypes'=>$servicetype,'district'=>$district,'associate'=>$associatename,'cardinfo'=>$cardinfo]);
	});


Route::get('/opreg',function ()
    {

    $value =  Session::get('username');
    $name  = DB::select("select um_associate_vc from user_master where um_username_vc = '$value'");
    $associatename =  $name[0]->um_associate_vc;
    // return $associatename;
    $servicetype = DB::select("select distinct stm_servicetype_vc from servicetype_master where stm_status_vc= 'Active' order by stm_servicetype_vc");

    $cardinfo = DB::select("select distinct cd_servicetype_vc from card_details where cd_agentname_vc= '$associatename' ");

    $district =DB::select("select distinct district_name from district order by district_name");

        return view('opreg', ['title' => 'OP REGISTRATION','servicetypes'=>$servicetype,'district'=>$district,'associate'=>$associatename,'cardinfo'=>$cardinfo]);
    });




Route::post('/addopdata','HomeController@storeopdata');

Route::get('/getheadname/{headcardno}','HomeController@getheadname');

Route::get('/getheadcardno/{headname}','HomeController@getheadcardno');

Route::get('/getserviceamount/{type}/{agentname}','HomeController@serviceamount');
Route::get('/serviceopamount/{type}/{agentname}','HomeController@serviceopamount');

Route::get('/editopcard/{cardno}','HomeController@edit');

Route::post('/opregcancel',function ()
	{
		return Redirect::to('/mythriop/opregister');
	});

Route::post('/updateopcard/{cardno}','HomeController@update');

Route::get('/deleteopcard/{cardno}','HomeController@delete');

Route::post('/opcard/{cardno}','HomeController@opcard');


    Route::get('staffname', function()
    {
        $value = Session::get('username');
    $name  = DB::select("select um_associate_vc from user_master where um_username_vc = '$value'");
    $associatename =  $name[0]->um_associate_vc;
        $staff = Input::get('staff');
        if($staff == null)
        {
            return Response::json([]);
        }
        $query = "select sm_name_fl from staff_master where sm_associatename_vc = '$associatename' and lower(sm_name_fl) LIKE lower('$staff%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->sm_name_fl);
        }
        return $items;

    });



    Route::get('citynames', function()
    {
        $city = Input::get('city');
        if($city == null)
        {
            return Response::json([]);
        }
        $query = "select distinct op_city_vc from op_registerdata where lower(op_city_vc) LIKE lower('$city%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->op_city_vc);
        }
        return $items;

    });


    Route::get('citytownnames', function()
    {
        $city = Input::get('city');
        if($city == null)
        {
            return Response::json([]);
        }
        $query = "select distinct de_city_vc from dealer_master where lower(de_city_vc) LIKE lower('$city%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->de_city_vc);
        }
        return $items;

    });


    Route::get('mobilenos', function()
    {
        $num = Input::get('num');
        if($num == null)
        {
            return Response::json([]);
        }
        $query = "select distinct op_contactno_vc from op_registerdata where lower(op_contactno_vc) LIKE lower('$num%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->op_contactno_vc);
        }
        return $items;

    });


//--end of op card register.


// for getting districts data from database
	Route::get('district/{id}', function($id)
	{
		$result = DB::select('Select district_name from district where state_name = ? order by district_name', array($id));
		return $result;
	});

    route::get('/getdoctorname/{dept}',function($deptname)
    {
        $name = Session::get('id');

        $result = DB::select("Select distinct dd_drname_vc from doctor_details where dd_department_vc = '$deptname' and dd_associatename_vc='$name'  order by dd_drname_vc" );
        return $result;
    });

//op transactions.

	Route::get('/optransactions',function ()
	{

        $val = Session::get('id');

		$data = DB::select("select *from patient_data order by td_opdate_dt");

		$deptnames = DB::select("select distinct dm_deptname_vc from department_master where dm_associatename_vc='$val' order by dm_deptname_vc");

        $doctor = DB::select("select distinct dd_drname_vc from doctor_details where dd_associatename_vc='$val' order by dd_drname_vc");

		return view('optransactions', ['title' => 'OP Card Transactions','data'=>$data,'department'=>$deptnames,'doctor'=>$doctor]);

	});

	Route::post('/addtransaction','TransactionController@store');

	Route::post('/getcardinfo/{cardno}','TransactionController@getcardinfo');

	Route::get('edittransactionop/{cardno}/{opdate}/{optime}','TransactionController@edit');

	Route::post('updatetransactionop/{cardno}/{opdate}/{optime}','TransactionController@update');

	Route::get('deletetransactionop/{cardno}/{opdate}/{optime}','TransactionController@delete');

	Route::post('/optransactioncancel',function ()
	{
		return Redirect::to('/mythriop/optransactions');
	});

    Route::post('/receipt','TransactionController@getreceipt');

//==for searching patient data by cardno or patientname or phone no of patient.


	Route::get('getbycardno/{cardno}','TransactionController@getbycardno');

	Route::get('getbyphoneno/{cardno}','TransactionController@getbyphoneno');

    Route::get('getbyname/{name}','TransactionController@getbyname');

//--for print the reports

	Route::get('/opdata',function ()
	{
		$result = [];
		$name = $fromdate = $searchtype = $todate = $servicetype=$status=$duration='';
        $service=DB::select("select distinct stm_servicetype_vc from servicetype_master  ");
        //  where stm_status_vc= 'Active';
        return view('opdata', ['title' => 'Reports','data'=>$result,'searchtype1'=>$searchtype,'name1'=>$name,'fromdate1'=>$fromdate,'todate1'=>$todate,'type'=>$service,'servicetype1'=>$servicetype,'status1'=>$status,'duration1'=>$duration]);
	});

	Route::post('/search','SearchController@getdata');

	Route::post('/print','SearchController@printreport');

//--for print the ASSOCIATE reports

    Route::get('/associatereports',function ()
    {
        $result = [];
        $name = $fromdate = $searchtype = $todate = $servicetype=$status=$duration ='';
        $service=DB::select("select distinct stm_servicetype_vc from servicetype_master ");
        // return $servicetype where stm_status_vc= 'Active';
        return view('associatereports', ['title' => 'Reports','data'=>$result,'searchtype1'=>$searchtype,'name1'=>$name,'fromdate1'=>$fromdate,'todate1'=>$todate,'type'=>$service,'servicetype1'=>$servicetype,'status1'=>$status,'duration1'=>$duration]);
    });

    Route::post('/associatesearch','AssociateReportsController@getdata');

    Route::post('/associateprint','AssociateReportsController@printreport');


    Route::post('associatereportcancel',function(){
        return Redirect::to('/mythriop/associatereports');
    });






// for getting name of patient

	    Route::get('patientname', function()
    {
    	$patientname = Input::get('patientname');
    	if($patientname == null)
    	{
    		return Response::json([]);
    	}

    	$query = "select op_patientname_vc from op_registerdata where lower(op_patientname_vc) LIKE lower('$patientname%')";
    	$results = DB::select($query);
        // return $results;
    	$items = [];
    	foreach($results as $res){
    		array_push($items,$res->op_patientname_vc);
    	}
    	return $items;
    });

//-- getting doctor name

	    Route::get('doctorname', function()
    {
        $assname = Session::get('username');
        $query = DB::select("select * from user_master where um_username_vc = '$assname'");
        $val = $query[0]->um_associate_vc;
    	$doctor = Input::get('doctor');
    	if($doctor == null)
    	{
    		return Response::json([]);
    	}
    	$query = "select dd_drname_vc from doctor_details where dd_associatename_vc = '$val' and lower(dd_drname_vc) LIKE lower('$doctor%')";
    	// return $query;
    	$results = DB::select($query);
    	$items = [];
    	foreach($results as $res){
    		array_push($items,$res->dd_drname_vc);
    	}
    	return $items;
    });
	//routes for department master

	Route::get('/departmentmaster', function ()
	{
        $name = Session::get('id');
        $query = DB::select("select * from dealer_master where de_agentid_vc = '$name'");
        $val = $query[0]->de_agentname_vc .' - '.$query[0]->de_area_vc;
        // return $name;
		$depts = DepartmentDetails::where('dm_associatename_vc',$name)->orderBy('dm_deptname_vc', 'asc')->get();

		return view('dept',['title'=>'Department Master','depts'=>$depts,'name'=>$name,'val'=>$val]);
	});

	Route::post('/adddept', 'DepartmentController@store');

	Route::get('/editdept/{departmentcode}','DepartmentController@edit');

	Route::post('/canceldept','DepartmentController@cancel');


	Route::post('/updatedept','DepartmentController@update');

	// Route::get('/deletedept/{departmentcode}','DepartmentController@delete');



    Route::get('/checkassociate/{associatename}/{name}',function($associatename,$name)
    {
        $query = DB::select("select * from user_master where um_associate_vc = '$associatename' and um_username_vc = '$name'");
        return $query;
    });


// end of dept master



	// Service to Masters->Doctor
	Route::get('doctormaster', function()
	{
        $name = Session::get('id');

		$doctor = DB::select("select * from doctor_details where dd_associatename_vc ='$name' order by dd_department_vc");

// return count($doctor);
		$deptnames = DB::select("select distinct dm_deptname_vc from department_master where dm_associatename_vc ='$name' order by dm_deptname_vc");

        $qualifiationdata = DB::select("select distinct qm_qualificaton_vc from qualificaton_master order by qm_qualificaton_vc");

		return view('doctormaster', ['title' => 'Hospital Registration',
			'doctordetail' => $doctor,'department'=>$deptnames,'qualifiationdata'=>$qualifiationdata,'dealerid'=>$name]);
	});

    route::get('getdoctorsfordept/{dept}',function($dept){
        $assname = Session::get('username');
        $result = DB::select('Select * from doctor_details where dd_department_vc = ? and dd_associatename_vc = ? order by dd_department_vc', array($dept,$assname));
        return $result;
    });

	//service for adding doctor-details
	Route::post('adddoctordetails', 'DoctorController@storedoctordetails');

	//Service for displaying doctordetails-data
	Route::get('editdoctordetails/{doctorid}', 'DoctorController@getdoctordetails');

	//Service for updating doctordetails-data
	Route::post('updatedoctordetails', 'DoctorController@updatedoctordetails');

    Route::get('deletedoctordetails/{doctorid}', 'DoctorController@deletedoctordetails');

    Route::post('/canceldoctor',function()
        {
        return Redirect::to('/mythriop/doctormaster');
        });
//--routes for contact us.
	// 	Route::get('contactus', function()
	// {
	// 	return view('contactus', ['title' => 'Contact Us']);
	// });

 //    Route::get('logincontactus', function()
 //    {
 //        return view('logincontactus', ['title' => 'Contact Us']);
 //    });

        Route::get('contactusadmin', function()
    {
        return view('contactusadmin', ['title' => 'Contact Us']);
    });

        Route::post('/sendquery','GuestController@sendquery');

//-- routes for service type masters


    Route::get('/servicetype',function ()
    {
        $name = Session::get('username');
    if($name == 'admin' )
    {
        $servicetype = ServiceTypeMaster::orderBy('stm_servicetype_vc','asc')->get();
        return view('servicetypemasters', ['title' => 'Service Type Master','data'=>$servicetype]);
    }
    else
    {
        return redirect()->back();
    }

    });

    Route::post('/addservicetype','MasterController@storeservicetype');

    Route::get('/editsertype/{type}','MasterController@gettype');

    Route::post('updatetypedetails/{code}','MasterController@updatetype');

        //Service for deleteing user-data
    Route::get('deletetypedetails/{code}', 'MasterController@deletetype');

        //Service for cancel user-data redirect to same-page
    Route::post('typecancel', function()
        {
            return Redirect::to('/mythriop/servicetype');
        });

        //Service for exit usermaster to main page(admin)
    Route::post('exituser', function()
        {
            return Redirect::to('/mythriop/admin');
        });
//-- routes for marketing staff masters


    Route::get('/marketingstaffmaster',function ()
    {
        $name  = Session::get('username');
        $query = DB::select("select * from user_master where um_username_vc = '$name'");
        $district =DB::select("select distinct district_name from district order by district_name");
        if($query[0]->um_associate_vc =='')
        {
             $associatename = 'admin';
        }
        else
        {
            $associatename = $query[0]->um_associate_vc;
        }
        // $associatename = $query[0]->um_associate_vc;
        $staffdetails = StaffMaster::where("sm_associatename_vc",$associatename)->orderBy('sm_state_vc','asc')->orderBy('sm_district_vc','asc')->orderBy('sm_city_vc','asc')->orderBy('sm_area_vc','asc')->get();

        return view('marketingstaffmaster', ['title' => 'Marketing Staff Master','details'=>$staffdetails,'district'=>$district]);
    });

    Route::get('getstaffbyarea/{query}',function($query){
        // return $query;
        $result =DB::select($query);
        return $result;
    });
    Route::post('/addstaff','MasterController@storestaff');

    Route::get('/displaystaffdata/{staffid}','MasterController@getstaff');

    Route::post('/marketingcancel',function(){
        return redirect('/mythriop/marketingstaffmaster');
    });
    Route::post('updatestaff/{code}','MasterController@updatestaff');

    Route::get('deletestaff/{code}','MasterController@deletestaff');

    Route::post('staffcancel', function()
        {
            return Redirect::to('/mythriop/marketingstaffmaster');
        });


    Route::any('checkstaff','GuestController@checkmarketing');

//-- routes for marketing staff masters


    Route::get('/associatemarketingstaff',function ()
    {
        $name  = Session::get('id');

        $district =DB::select("select distinct district_name from district order by district_name");

        if($name =='')
        {
             $associatename = 'admin';
        }
        else
        {
            $associatename = $name;
        }

        $staffdetails = StaffMaster::where("sm_associatename_vc",$associatename)->orderBy('sm_name_fl','asc')->get();
        // return $staffdetails;
        return view('associatemarketingstaff', ['title' => 'Marketing Staff Master','details'=>$staffdetails,'associatename'=>$name,'district'=>$district]);
    });

    Route::post('/addstaff','MasterController@storestaff');

    Route::get('/displaystaffdata/{staffid}','MasterController@getstaff');

    Route::post('/marketingcancel',function(){
        return redirect('/mythriop/marketingstaffmaster');
    });
    Route::post('updatestaff/{code}','MasterController@updatestaff');

    Route::get('deletestaff/{code}','MasterController@deletestaff');

    Route::post('staffcancel1', function()
        {
            return Redirect::to('/mythriop/associatemarketingstaff');
        });

    Route::post('exitamsd', function()
        {
            return Redirect::to('/mythriop/user');
        });





    // for  business staff master
    Route::get('/associatebusinessstaff',function ()
    {
        $name  = Session::get('username');
        $query = DB::select("select * from user_master where um_username_vc = '$name'");
    $district =DB::select("select distinct district_name from district order by district_name");
        if($query[0]->um_associate_vc =='')
        {
             $associatename = 'admin';
        }
        else{
        $associatename = $query[0]->um_associate_vc;
    }
        $staffdetails = StaffMaster::where("sm_associatename_vc",$associatename)->orderBy('sm_name_fl','asc')->get();
        return view('associatebusinessstaff', ['title' => 'Marketing Staff Master','details'=>$staffdetails,'associatename'=>$name,'district'=>$district]);
    });

    Route::post('staffcancel2', function()
        {
            return Redirect::to('/mythriop/associatebusinessstaff');
        });

    Route::post('exitamsdbus', function()
        {
            return Redirect::to('/mythriop/busiuser');
        });






    //--quaflication master
    Route::get('/qualificationmaster',function ()
    {
        $qualification = QualificationMaster::orderBy('qm_qualificatoncode_vc','asc')->get();
        return view('qualificationmaster', ['title' => 'Qualification Master','data'=>$qualification]);
    });

    Route::get('editquaflica/{code}','MasterController@getqualification');

    Route::post('/addqualification','MasterController@storequalification');

    Route::post('updatequalification','MasterController@updatequalification');

    Route::get('/deletequalification/{code}','MasterController@deletequalification');
    Route::post('/qualificationcancel',function(){
        return redirect('/mythriop/qualificationmaster');
    });




// SERVICES  TO USER MASTER PAGE
    // Service to Masters-> User
    Route::get('usermaster', function()
    {
    $name = Session::get('username');
// return $name;
    if($name == 'admin' )
    {
        $user = UserMaster::orderby('um_associate_vc','asc')->get();
        $associates = DB::select("select distinct de_agentname_vc from dealer_master order by de_agentname_vc ");
    }
    else
    {
        $assname = DB::select("select um_associate_vc from user_master where um_username_vc ='$name' and um_usertype_in ='0'");
         $val = $assname[0]->um_associate_vc;
        $user = UserMaster::where('um_associate_vc',$val)->where('um_usertype_in',1)->orderby('um_associate_vc','asc')->get();
        $associates = DB::select("select distinct de_agentname_vc from dealer_master where de_agentname_vc = '$val' order by de_agentname_vc ");
    }
        // return $user;

        // $associates = DB::select("select distinct de_agentname_vc from dealer_master order by de_agentname_vc ");

        return view('usermaster', ['title' => 'User Registration', 'user' => $user,'associate'=>$associates]);
    });

    route::get("getassociateusers/{associatename}",function($name){
        $data = DB::select("SELECT * FROM user_master where um_associate_vc='$name' order by um_associate_vc");
        return $data;
    });
        //Service for saving user-data
    Route::post('adduser', 'MasterController@store');

    //Service fro getting dropdownautosuggest for username
    Route::get('userName', function()
    {
        $username = Input::get('username');
        if($username == null)
        {
            return Response::json([]);
        }
        $results = UserMaster::where('um_username_vc', 'LIKE', $username . '%')->lists('um_username_vc');
        return $results;
    });

    route::get("getassociates/{associatetype}",function($type){
        $data = DB::select("SELECT de_agentname_vc FROM dealer_master where de_agenttype_vc='$type' order by de_agentname_vc asc");
        return $data;
    });


    Route::get('departmentname', function()
    {
    $assname = Session::get('username');
        $dept = Input::get('dept');

        if($dept == null)
        {
            return Response::json([]);
        }
        $query = "select dm_deptname_vc from department_master where dm_username_vc ='$assname' and  lower(dm_deptname_vc) LIKE lower('$dept%')";
        // return $query;
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->dm_deptname_vc);
        }
        return $items;

    });
        //Service for displaying user-data
    Route::get('displayuser/{usertype}/{username}', 'MasterController@getdetails');

        //Service for updating user-data
    Route::post('updateuserdetails', 'MasterController@update');

        //Service for deleteing user-data
    Route::get('deleteuserdetails/{usertype}/{username}', 'MasterController@destroy');

        //Service for cancel user-data redirect to same-page
    Route::post('usercancel', function()
        {
            return Redirect::to('/mythriop/usermaster');
        });

        //Service for exit usermaster to main page(admin)
    Route::post('exituser', function()
        {
            return Redirect::to('/mythriop/admin');
        });

// END OF SERVICES FOR USER MASTER PAGE
    Route::get('getname/{name}','SearchController@getallnames');
    Route::get('getnamebyass/{name}','SearchController@getallnamesass');


    Route::post('reportcancel', function()
        {
            return Redirect::to('/mythriop/opdata');
        });

    Route::get('logout','LoginController@logout');
    Route::get('adminlogout','LoginController@adminlogout');




    // for dealer master.

    Route::get('/dealermaster',function()
    {
        $district =DB::select("select distinct district_name from district  order by district_name  ");
        $name=Session::get('username');
        // return $name;
    if($name == 'admin' )
    {
        $deptfacilities =DepartmentFacilities::where('dfm_status_vc','Active')->orderBy('dfm_deptname_vc','asc')->get();
        $data = DealerMaster::where('de_status_vc','2')->orderby('de_agentname_vc','asc')->get();
        $categories = DB::select("select distinct ctm_categoryname_vc from category_master where ctm_categorystatus_vc='Active' order by ctm_categoryname_vc  ");
        return view('dealermaster',['title' => 'Dealer Master','district'=>$district,'data'=>$data,'categories'=>$categories,'deptfacilities'=>$deptfacilities]);
    }
    else
    {
        return redirect()->back();
    }


    });

    Route::get('/getfacilities/{agentid}','DealerController@getfacilities');

    Route::get('/editdealerdetails/{agentid}','DealerController@getdealerdata');
    Route::post('/adddealer','DealerController@storedealer');

    Route::any('checkdealer/{agentname}/{contactNo}','DealerController@checkdealer');

    Route::post('/updatedealer','DealerController@updatedealer');


    Route::post('canceldealer', function()
        {
            return Redirect::to('/mythriop/dealermaster');
        });


            // for all dealer names.
    Route::get('dealernames', function()
    {
        $dealer = Input::get('dealer');
        if($dealer == null)
        {
            return Response::json([]);
        }
        $query = "select distinct de_agentname_vc from dealer_master where  lower(de_agentname_vc) LIKE lower('$dealer%')";
        // return $query;
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->de_agentname_vc);
        }
        return $items;
    });

    Route::get('dealernameshosp', function()
    {
        $dealer = Input::get('dealer');
        if($dealer == null)
        {
            return Response::json([]);
        }
        $query = "select distinct de_agentname_vc from dealer_master where  lower(de_agentname_vc) LIKE lower('$dealer%')";
        // return $query;
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->de_agentname_vc);
        }
        return $items;
    });


   Route::get('dealernamesbusi', function()
    {
        $dealer = Input::get('dealer');
        if($dealer == null)
        {
            return Response::json([]);
        }
        $query = "select de_agentname_vc from dealer_master where de_agenttype_vc ='Business' and lower(de_agentname_vc) LIKE lower('$dealer%')";
        // return $query;
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->de_agentname_vc);
        }
        return $items;
    });

    Route::get('dealersarea', function()
    {
        $areaname = Input::get('areaname');
        if($areaname == null)
        {
            return Response::json([]);
        }
        $query = "select de_area_vc from dealer_master where  lower(de_area_vc) LIKE lower('$areaname%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->de_area_vc);
        }
        return $items;

    });



//--associate tariff


Route::get('/associatetariffmaster',function()
{
    $servicetype = DB::select("select distinct stm_servicetype_vc from servicetype_master where stm_status_vc= 'Active' order by stm_servicetype_vc");
    $name = Session::get('username');
    $data = AssociateTariffs::orderby('at_agentname_vc','asc')->get();
    if($name == 'admin' )
    {
    return view('associatetariffmaster',['title' => 'Dealer Master','servicetypes'=>$servicetype,'data'=>$data]);
    }
    else
    {
        return redirect()->back();
    }
    // return $data;

});


Route::get('/associatebusinessstariffmaster',function()
{
    $servicetype = DB::select("select distinct stm_servicetype_vc from servicetype_master where stm_status_vc= 'Active' order by stm_servicetype_vc");
    $name = Session::get('username');
    $data = AssociateTariffs::orderby('at_agentname_vc','asc')->get();
    if($name == 'admin' )
    {
    return view('associatebusinessstariffmaster',['title' => 'Dealer Master','servicetypes'=>$servicetype,'data'=>$data]);
    }
    else
    {
        return redirect()->back();
    }
    // return $data;

});


    Route::get('/editassociatetariff/{agentid}/{type}','AssociateController@getdata');

    Route::post('/addassociatetariff','AssociateController@store');

    Route::post('/updateassociatetariff/{agentid}/{type}','AssociateController@update');

    Route::get('/deleteassociatetariff/{agentid}/{type}','AssociateController@delete');

    Route::post('cancelassociatetariff', function()
        {
            return Redirect::to('/mythriop/associatetariffmaster');
        });


    Route::post('cancelassociatetariffbusi', function()
        {
            return Redirect::to('/mythriop/associatebusinessstariffmaster');
        });
    Route::post('exitassociatetariff', function()
        {
            return Redirect::to('/mythriop/admin');
        });

    Route::get('/getagentid/{name}/{area}','AssociateController@getagentid');
    Route::get('/getagentname/{id}','AssociateController@getagentname');



    // Route::get('/getagentid/{name}','AssociateController@getagentid');



    // agent registration


    Route::get('/agentregistration',function()
        {
            $data =AgentCardIssueDetails::orderby('ci_agentname_vc','asc')->get();
            $servicetype = DB::select("select distinct stm_servicetype_vc from servicetype_master");
            $name = Session::get('username');
            if($name =='admin')
            {
            return view('agentregistration',['title' => 'Dealer Master','data'=>$servicetype,'agents'=>$data]);
            }
            else
            {
                return redirect()->back();
            }
        });

    Route::get('getlastcard','AgentController@lastcardno');

    Route::post('addcardissuedata','AgentController@issuecards');

    Route::get('editagentdetails/{agentid}/{servicetype}/{date}','AgentController@getdetails');

    Route::post('updatagent/{agentid}/{servicetype}/{date}','AgentController@updateagent');

    Route::get('deleteagent/{agentid}/{servicetype}/{date}','AgentController@deleteagent');

    Route::post('agentregcancel', function()
        {
            return Redirect::to('/mythriop/agentregistration');
        });

    Route::post('regcancelexit', function()
        {
            return Redirect::to('/mythriop/admin');
        });

    Route::get('getopcard/{cardno}',function($cardno)
    {
        $query = DB::select("select * from card_details where cd_cardno_vc='$cardno'");
        return $query;
    });

    Route::get('getallservicetypecards/{agentid}/',function($agentid)
    {
        $query = DB::select("select distinct at_servicetype_vc from associate_tariffs where at_agentid_vc = '$agentid'");
        return $query;
    });


    Route::get('getdataforoneagent/{agentname}',function($agentname)
    {
        $query = DB::select("select * from associate_tariffs where at_agentname_vc = '$agentname'");
        return $query;

    });

    Route::get('getallagentdata/{agentname}',function($agentname)
    {
        $query = DB::select("select * from agent_cardissue where ci_agentname_vc = '$agentname'");
        return $query;

    });

    // Route::get('getallcards',function()
    // {
    //     $query = DB::select("select distinct stm_servicetype_vc from servicetype_master ");
    //     return $query;

    // });
    // for guests to search near by hospitals.

    Route::get('categorynames', function()
    {

        $dept = Input::get('name');
        if($dept == null)
        {
            return Response::json([]);
        }
        $query = "select distinct ctm_categoryname_vc from category_master where lower(ctm_categoryname_vc) LIKE lower('$dept%')";
        $results = DB::select($query);
        // return $query;
        $items = [];
        foreach($results as $res){
            array_push($items,$res->ctm_categoryname_vc);
        }
        return $items;

    });

    Route::get('getdatabyassociatetype/{name}',function($name)
    {
        // $data = DB::select("Select * from dealer_master where de_agenttype_vc = '$name'");
    $data = DB::select($name);
        return $data;
    });




    Route::get('searchhospitals',function()
    {

        $dept =DepartmentFacilities::orderBy('dfm_deptname_vc','asc')->get();

        $servicetypedata = DB::select("select distinct stm_servicetype_vc from servicetype_master ");
        $data = [];
        $categories = DB::select("select distinct ctm_categoryname_vc from category_master where ctm_categorystatus_vc='Active'  order by ctm_categoryname_vc  ");
        $district =DB::select("select distinct district_name from district order by district_name");

        // $associatename =
        $state =$city =$area= $dist=$department=$servicetype=$category='';

        return view ('searchpage',['title' => 'Search Hospitals','data'=>$data,'state' => $state,'city' => $city,'area'=>$area,'district'=>$district,'dist'=>$dist,'dept'=>$dept,'department'=>$department,'servicetype'=>$servicetype,'servicetypedata'=>$servicetypedata,'category'=>$category,'categoriesdata'=>$categories]);
    });
    Route::post('searchcancel',function()
    {
        return Redirect::to('/mythriop/searchhospitals');
    });

  Route::post('searchhosp','GuestController@getallhospdata');


//-- feedback form'

    Route::get('feedback',function()
    {
        return view ('feedback',['title'=>'FeedBack Form']);
    });

    Route::post('feedbackcancel',function(){
        return Redirect::to('/mythriop/feedback');
    });

    Route::post('sendfeedback','GuestController@sendfeedback');

    Route::get('gethospinfeedback', function()
    {

        $dept = Input::get('name');
        if($dept == null)
        {
            return Response::json([]);
        }
        $query = "select distinct de_agentname_vc from dealer_master where de_agenttype_vc ='Hospital' and  lower(de_agentname_vc) LIKE lower('$dept%')";
        $results = DB::select($query);
        // return $query;
        $items = [];
        foreach($results as $res){
            array_push($items,$res->de_agentname_vc);
        }
        return $items;

    });

    route::get('getcategories/{hospname}',function($hospname){
        $data = DB::select("Select distinct de_hospitalcategory_vc from dealer_master where de_agentname_vc = '$hospname'");
        return $data;
    });

    route::get('getallcategories',function(){
        $data = DB::select("Select distinct ctm_categoryname_vc  from category_master ");
        return $data;
    });




    Route::get('/categorymaster',Function()
    {
        $name = Session::get('username');
    if($name == 'admin' )
    {
        $data = CategoryMaster::orderBy('ctm_categorystatus_vc','asc')->orderBy('ctm_categoryname_vc','asc')->get();
        return view ('categorymaster',['title'=>'Category Master','data'=>$data]);
    }
    else
    {
        return redirect()->back();
    }

    });

    Route::get('editcategory/{code}','GuestController@getcategory');

    Route::post('/addcategory','GuestController@storecategory');

    Route::post('updatecategory/{code}','GuestController@updatecategory');

    Route::get('/deletecategory/{code}','GuestController@deletecategory');

    Route::post('/categorycancel',function(){
        return redirect('/mythriop/categorymaster');
    });


// query list page

    Route::get('/querypage',function()
    {
        $name = Session::get('username');
    if($name == 'admin' )
    {
        $data = DB::select("Select * from queries_tables where qt_status_in='0'");
        return view('querylist',['title'=>'Query List','data'=>$data]);
    }
    else
    {
        return redirect()->back();
    }


    });

    Route::post('querycancel',function(){
        return Redirect::to('/mythriop/querypage');
    });


    Route::post('/replyquery/{id}',function($id)
    {
        $data = DB::select("select * from queries_tables where id='$id'");
        return view('replyquery',['title'=>'Reply Page','value'=>$data]);
    });

    Route::post('sendreply','GuestController@sendqueryreply');




    //marketing staff master auto remeber.
    Route::get('marketingpeople', function()
    {
        $name  = Session::get('username');
        $query = DB::select("select * from user_master where um_username_vc = '$name'");
        if($query[0]->um_associate_vc =='')
        {
             $associatename = 'admin';
        }
        else{
        $associatename = $query[0]->um_associate_vc;
    }
        $staff = Input::get('staff');
        if($staff == null)
        {
            return Response::json([]);
        }
        $query = "select sm_name_fl from staff_master where sm_associatename_vc = '$associatename' and lower(sm_name_fl) LIKE lower('$staff%')";
        // return $query;
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->sm_name_fl);
        }
        return $items;

    });

    Route::get('staffarea', function()
    {
        $staffareaname = Input::get('staffareaname');
        if($staffareaname == null)
        {
            return Response::json([]);
        }
        $query = "select sm_area_vc from staff_master where  lower(sm_area_vc) LIKE lower('$staffareaname%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->sm_area_vc);
        }
        return $items;

    });

    Route::get('staffcity', function()
    {
        $staffcityname = Input::get('staffcityname');
        if($staffcityname == null)
        {
            return Response::json([]);
        }
        $query = "select sm_city_vc from staff_master where  lower(sm_city_vc) LIKE lower('$staffcityname%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->sm_city_vc);
        }
        return $items;

    });



    // view for online op card
Route::get('/onlineopcard',function ()
    {
        $servicetype = DB::select("select distinct stm_servicetype_vc from servicetype_master where stm_status_vc= 'Active' order by stm_servicetype_vc");
        $associatename = '';
        $district =DB::select("select distinct district_name from district order by district_name");

        return view('onlineopcard',['title' => 'OP REGISTRATION','servicetypes'=>$servicetype,'district'=>$district,'associate'=>$associatename]);
    });

// get promo discount()



Route::post('/addonlineopcard','OnlineController@store');

Route::post('/opcancel',function ()
    {
        return Redirect::to('/mythriop/onlineopcard');
    });



// view for promocode master
Route::get('/promocode',function ()
    {
        $data = PromoCodeMaster::orderBy('pcd_promocode_vc','asc')->get();
        return view('promocodemaster',['title' => 'Promo Code Master','data'=>$data]);
    });


Route::post('/addpromocode','OnlineController@addpromocode');

    Route::any('editpromocode/{code}',function($code){
        $query = DB::select("SELECT * from promocode_data where pcd_promocode_vc = '$code'");
        return view('editpromocode',['title' => 'Edit Promo Code Master','data'=>$query]);

    });


    Route::post('updatepromocode','OnlineController@updatepromocode');

    Route::get('/deletepromocode/{code}','OnlineController@deletepromocode');

    Route::post('/promocodecancel',function(){
        return redirect('/mythriop/promocode');
    });

    Route::get('promocodes', function()
    {
        $code = Input::get('code');
        if($code == null)
        {
            return Response::json([]);
        }
        $query = "select pcd_promocode_vc from promocode_data where  lower(pcd_promocode_vc) LIKE lower('$code%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->pcd_promocode_vc);
        }
        return $items;

    });
    //dept facilities
Route::get('/departmentfacilities',function ()
    {
        $data = DepartmentFacilities::orderBy('dfm_status_vc','asc')->orderBy('dfm_deptname_vc','asc')->get();

        return view('departmentfacilities',['title' => 'Department Facility Master','data'=>$data]);
    });

    Route::get('deptfacname', function()
    {
        $dept = Input::get('dept');
        if($dept == null)
        {
            return Response::json([]);
        }
        $query = "select dfm_deptname_vc from department_facilities where  lower(dfm_deptname_vc) LIKE lower('$dept%')";
        $results = DB::select($query);
        $items = [];
        foreach($results as $res){
            array_push($items,$res->dfm_deptname_vc);
        }
        return $items;

    });



Route::post('adddeptfacities','DeptFacilitiesController@adddeptfacities');


    Route::get('editdeptfacility/{deptname}','DeptFacilitiesController@getdeptfacility');


    Route::post('updatedeptfacility','DeptFacilitiesController@updatedeptfacility');


    Route::get('/deletedeptfacility/{deptname}','DeptFacilitiesController@deletedeptfacility');

    Route::post('/deptfacilitycancel',function(){
        return redirect('/mythriop/departmentfacilities');
    });
    Route::get('sample',function(){
         // $id = DealerMaster::orderBy('created_at', 'desc')->first();
         return $id;
    });



Route::any('/printcards',function ()
    {
        $data=DB::select("select * from op_registerdata");
        // return $data;
        return view('printcards',['title' => 'For Print Cards.','data'=>$data]);
    });
Route::post('/thiscancel',function ()
    {
        return Redirect::to('/mythriop/printcards');
    });
Route::any('/printcard/{cardno}/{name}/{age}','HomeController@printcard');


Route::any('locateon',function(){
    return view('locateon',['title' => 'For Locate on Map.']);
});


Route::any('/assocaiteverfication',function ()
    {
        $data=DB::select("select * from dealer_master where de_status_vc = '1'");
        // return $data;
        return view('assocaiteverfication',['title' => 'Verfication','data'=>$data]);
    });



    Route::get('/getareas/{name}',function($name)
        {
            $query = DB::select("select de_area_vc  from dealer_master where de_agentname_vc = '$name'");
                return $query;
        });

// special offers

    // view for promocode master
Route::get('/specialoffers',function ()
    {
        // $data = PromoCodeMaster::orderBy('pcd_promocode_vc','asc')->get();
        return view('specialoffers',['title' => 'Special Offers']);
    });

    Route::post('addspecialoffers','OnlineController@addspecialoffers');

    Route::any('pendingoffers','OnlineController@pendingoffers');

    Route::any('getofferdata/{offerid}','OnlineController@getofferdata');

    Route::any('acceptofferdata/{offerid}','OnlineController@updateofferdata');

});
