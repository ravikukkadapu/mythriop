<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoginRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => array('required', 'regex:/^([^\s]+[a-z\s.])+$/','min:3', 'max:10'),
            'Password' =>'required'
        ];
    }
    public function messages(){

        return [
            'username.required' => "Enter Username",
            'Password.required' =>"Enter Password",
        ];
    }
}
