<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FeedBackRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'cardno' => 'required',
                'mobileno' => 'required',
                'hospname' => 'required',
                'category' => 'required',


        ];
    }

    public function messages()
    {
        return [
                'hospname.required'=>'Enter Hospital / Clinic Name',
                'category.required'=>'Select any Hospital Type',
                'mobileno.required'=>'Enter Phone No',
                'cardno.required'=>'Enter Cardno',
        ];
    }
}
