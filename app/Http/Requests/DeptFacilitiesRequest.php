<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DeptFacilitiesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'deptname' =>'required',
            'deptstatus' =>'required',
        ];
    }
    public function messages()
    {
        return [
                'deptname.required' => 'Enter Department Facility Name',
            'deptstatus.required' => 'Select Status',
        ];
    }
}
