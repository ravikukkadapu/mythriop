<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DealerMasterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
                // 'agenttype' => 'required',
                'agentname' => 'required',
                // 'category' => $value1,
                 'contactname' => 'required',
                'contactNo1' => 'required|regex:/^([0-9]{10})$/',
                'enrolldt' =>'required',
                'state' => 'required',
                'district' => 'required',
                'city' => 'required',
                'area' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',

                // 'color' => 'required',

        ];
    }

    public function messages()
    {
        return [
            // 'agenttype.required' =>'Enter Agent Type ',
            'agentname.required' => "Enter Agent/ Associate Name",
            'contactname.required' => "Enter Name with Title (Mr/Mrs..) ",
            'contactNo1.required' => 'Enter Contact No 1',
            'contactNo1.regex' => 'Enter 10 digit Mobile No',
                'state.required' => "Select State ",
                'district.required' => "Select District ",
            'city.required' =>"Enter City/Town/Village",
            'area.required' =>"Enter Area",
            'enrolldt.required' => 'Select Enrolled Date',
            'latitude.required' =>"Select Latitude",
            'longitude.required' => 'Select Longitude',
            'color.required' => 'Select atleast one Out Patient facility',
            // 'documentproof.required' => 'Select Mou & facility Document'
        ];
    }
}
