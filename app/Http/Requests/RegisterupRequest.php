<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'servicetype' =>'required',
                 'issuedate' =>'required',
                 // 'validupto' =>'required',
                'age' =>'required|numeric',
                'sex' =>'required',
                 'Name'=>'required|max:30',
                 'contactNo' =>'required|numeric|min:10',
                 // 'photoid' =>'required',
                 'city' =>'required'

        ];
    }
        public function messages()
    {
        return [
            'servicetype.required' =>"Select Required Service Type",
            'issuedate.required' =>"Enter Card Issue Date",
            'age.required' => 'Enter Age',
            'age.numeric' => "Enter digits only",
            'sex.required' =>"Select Gender",
            'Name.required' => "Enter Name with Title (Mr/Mrs..) ",
            'contactNo.required' => 'Enter Mobile No',
            'contactNo.min' => 'Enter 10 digit Mobile No',
            // 'photoid.required' =>"Select Photo",
            'city.required' =>"Enter City/Town/Village"

        ];
    }
}
