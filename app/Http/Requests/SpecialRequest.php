<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SpecialRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' =>'required',



        ];
    }
            public function messages()
    {
        return [
            'title.required' => 'Enter Title / Subject',
            'description.required' => 'Enter Description',


        ];
    }
}
