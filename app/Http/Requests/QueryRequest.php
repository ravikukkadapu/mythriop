<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QueryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'mobile' => 'required',
            'email' => 'required',

        ];
    }

    public function messages()
    {

        return [
            'name.required' => 'Enter Name',
            'mobile.required' => 'Enter Mobile No',
            'email.required' => 'Enter Emaild',

        ];

    }
}
