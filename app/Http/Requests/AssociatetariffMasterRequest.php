<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AssociatetariffMasterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
                'agentname' => 'required',
                'servicetype' => 'required',
                'discount' => 'required|numeric',
                'cost' => 'required|numeric',
                'regcharges' => 'required|numeric',

        ];
    }

    public function messages()
    {
        return [
                'agentname.required' => 'Enter Agent Name',
                'servicetype.required' => 'Select Service Type',
                'cost.required' => 'Enter Cost',
                'cost.numeric' => 'Enter Cost in digits only',
                'regcharges.required' => 'Enter Registration Charges',
                'regcharges.numeric' => 'Enter Registration Charges in digits only',
                'discount.required' => 'Enter Discount '
                'discount.numeric' => 'Enter Discount in digits only'
        ];
    }
}
