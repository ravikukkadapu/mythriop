<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServiceTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serviceType' => 'required',
            'acost' => 'required | numeric',
            'chcost' => 'required | numeric',
            'opFees' =>'required | numeric'
        ];
    }
    public function messages()
    {
        return [
            'serviceType.required' => 'Enter service Type',
            'acost.required' => 'Enter Adult Cost',
            'chcost.required' => 'Enter Child Cost',
            'opFees.required' =>'Enter Op Fees'
        ];
    }
}
