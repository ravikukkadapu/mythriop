<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
                'agentname' =>'required',
                'agentid' =>'required',
                'servicetype1' =>'required',
                'issuedate1' =>'required',
                'noofcards1' =>'required',

        ];
    }
    public function messages()
    {
        return [
                'agentname.required' =>'Enter Associate Name',
                'agentid.required' =>'Please Register Associate then Issue Cards',
                'servicetype1.required' => 'Select service type for Issuing Card',
                'issuedate1.required' => 'Select Issue Date for Issuing Card',
                'noofcards1.required' => 'Enter No of Cards for Issuing Card',
        ];
    }
}
