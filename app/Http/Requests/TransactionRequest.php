<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cardno' => 'required',
            'phoneno' => 'required',
            //'servicetype' => 'required',
            //'opFees' => 'required',
            //'department' => 'required',
            //'consultingdoctor' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Enter Name (or)',
            'cardno.required' => 'Enter Card No (or)',
            'phoneno.required' => 'Enter Mobile No',
            'servicetype.required' => 'Enter Service Type',
            'opFees.required' => 'Enter Op Fees',
            'department.required' => 'Select Department',
            'consultingdoctor.required' => 'Select Consulting Doctor',
        ];
    }
}
