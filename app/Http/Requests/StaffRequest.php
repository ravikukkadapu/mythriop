<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StaffRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'mobileno' =>'required|regex:/^([0-9]{10})$/',
            'emailid' => 'required|email',
            'altmobileno' =>'regex:/^([0-9]{10})$/',
            'country' =>'required',
            'state' => 'required',
            'district' =>'required',
            'city' =>'required',
            'area' => 'required',
             'photoid' => 'required',
             'addressproof' => 'required',


        ];
    }
            public function messages()
    {
        return [
            'name.required' => 'Enter Name',
            'emailid.required' => 'Enter Email Id',
            'mobileno.required' => 'Enter  Mobile No',
            'mobileno.regex' => 'Enter 10 digit Mobile No',
            'altmobileno.regex' => 'Enter 10 digit Alt Mobile No',
            'emailid.email' =>"Enter Valid Email Id",
            'country.required' => 'Select Country Name',
            'state.required' => 'Select State Name',
            'district.required' => 'Select District Name',
            'city.required' => 'Enter City/Town/Village Name',
            'area.required' => 'Enter Area Name',
            'photoid.required' => 'Select Photo ID',
            'addressproof.required' => 'Select Address Proof',

        ];
    }
}
