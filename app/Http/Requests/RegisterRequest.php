<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        // 'cardno' =>'required',
                'servicetype' =>'required',
                 'issuedate' =>'required',
                 // 'validupto' =>'required',
                'age' =>'required|numeric',
                'sex' =>'required',
                 'Name'=>'required|max:30',
                 'contactNo' =>'required|regex:/^([0-9]{10})$/',
                 // 'photoid' =>'required',
                 'city' =>'required',
                 'area' => 'required',
// 'mobile' => array('required' , 'regex:/^([0-9]{10})$/'),
                 'aggrement' => 'required'
        ];
    }
        public function messages()
    {
        return [
        // 'cardno.required' =>'Enter Card No',
            'servicetype.required' =>"Select Required Service Type",
            'issuedate.required' =>"Enter Card Issue Date",
            'age.required' => 'Enter Age',
            'age.numeric' => "Enter digits only",
            'sex.required' =>"Select Gender",
            'Name.required' => "Enter Name with Title (Mr/Mrs..) ",
            'contactNo.required' => 'Enter Mobile No',
            'contactNo.regex' => 'Enter 10 digit Mobile No',
            // 'photoid.required' =>"Select Photo",
            'city.required' =>"Enter City/Town/Village",
            'area.required' =>"Enter Area",
                 'aggrement.required' => 'Select Terms & conditions'
        ];
    }
}
