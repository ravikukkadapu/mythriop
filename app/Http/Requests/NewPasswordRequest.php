<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewPasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currentpass' => 'required',
            'newpass' => 'required',
            'confirmpass' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'currentpass.required' =>"Enter Current Password",
            'newpass.required' =>"Enter New Password",
            'confirmpass.required' =>"Enter Confirm Password"
        ];
    }
}
