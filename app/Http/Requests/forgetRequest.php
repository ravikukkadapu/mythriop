<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class forgetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => array('required')

        ];
    }
    public function messages(){

        return [
            'username.required' => "Enter Username For Resending Password to Mail id"
        ];
    }
}
