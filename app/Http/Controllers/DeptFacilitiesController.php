<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DeptFacilitiesRequest;
use App\Http\Controllers\Controller;
use App\DepartmentFacilities;
use DB;
use Redirect;

class DeptFacilitiesController extends Controller
{
    public function adddeptfacities(DeptFacilitiesRequest $request)
    {


        $deptstatus = $request->input('deptstatus');
        $deptname = $request->input('deptname');
        $remarks = $request->input('remarks');

    try{
        $deptnames = DB::select("select count(*) as count from department_facilities where lower(dfm_deptname_vc) = lower('$deptname')");
        $count = $deptnames[0]->count;

        if($count == '1')
            {
                return redirect()->back()->withInput()->withErrors(array('message' => 'Department Facility  Already Exists!'));
            }

        else
            {
                $facility = new DepartmentFacilities;
                $facility->dfm_deptname_vc = strtoupper($deptname);
                $facility->dfm_remarks_vc = $remarks;
                $facility->dfm_status_vc = $deptstatus;
                $facility->save();
                return redirect()->back()->with('message', 'Department Facility Data Added Succesfully.');
            }
        }

    catch(\Exception $e)
        {
            // return $e;
            return redirect()->back()->withInput()->withErrors(array('message' => 'Error in Adding Department Facility Data! '));
        }
    }





public function getdeptfacility($deptname)
    {
        $query = DB::select("SELECT * from department_facilities where dfm_deptname_vc = '$deptname'");
        return view('editdeptfacility',['data'=>$query,'title'=>'Edit Department Facilities']);
    }




public function updatedeptfacility(DeptFacilitiesRequest $request)
{
        $deptname = $request->input('deptname');
        $deptstatus = $request->input('deptstatus');
        $remarks = $request->input('remarks');
       try
        {
            $userupdatequery = DB::select("UPDATE department_facilities SET
            dfm_status_vc='$deptstatus',
            dfm_remarks_vc = '$remarks'
            WHERE dfm_deptname_vc = '$deptname'");

            return redirect()->to('mythriop/departmentfacilities')->with('message', 'Department Facility Details updated successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update Department Facility Details'));
        }
}

    public function deletedeptfacility($deptname)
    {
        try
        {
            // $deleteuser = DB::select("DELETE from department_facilities where dfm_deptname_vc = '$deptname'");
            $userupdatequery = DB::select("UPDATE department_facilities SET
            dfm_status_vc='Inactive'
            WHERE dfm_deptname_vc = '$deptname'");
            // return redirect()->back()->with('message', 'Department Facility deleted successfully');
            return redirect()->back()->with('message', 'Department Facility is successfully Changed to Inactive');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete Department Facility'));
        }
    }



}
