<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\forgetRequest;
use App\Http\Requests\ChangeRequest;
use App\Http\Requests\NewPasswordRequest;
use App\UserMaster;
use Session;
use Redirect;
use DB;
use App;
use PDF;
use Mail;


class LoginController extends Controller
{
    public function dologin(LoginRequest $request)
    {
        // return 123;
        try
        {
            $username = $request->input('username');
            $password = ($request->input('Password'));
            $type = $request->input('usertype');
            $login = UserMaster::where('um_username_vc', $username)
                            ->where('um_password_vc', $password)
                            ->where('um_usertype_in',$type)
                            ->first();
        if ($login)
        {
            Session::put('username', $username);
            Session::put('usertype', $login->um_usertype_in);
            Session::put('type', $login->um_associatetype_vc);
            Session::put('id', $login->um_associate_vc);
            if ($login->um_usertype_in == 0)
            {
                    Session::put('username', $username);
                    Session::put('usertype', $login->um_usertype_in);
                    return Redirect::to('mythriop/admin');
            }
            else if ($login->um_usertype_in == 1)
            {
                // return Session::get('type');
                return Redirect::to('mythriop/user');

            }

        }
        else
        {
            return Redirect::back()
                            ->withInput()
                            ->withErrors('Incorrect Username/password');
        }
        }

        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => 'Username or Password not matched'));
        }
    }





    public function adminlogout()
    {
        Session::flush();
        return Redirect::to('mythriop/adminlogin');
    }


    public function logout()
    {
        Session::flush();
        return Redirect::to('mythriop/login');
    }

    // adminlogout

    public function forgot(forgetRequest $request)
    {
        $username = $request->input('username');
        $user = UserMaster::where('um_username_vc',$username)
                        ->first();
        $email = $user->um_mailid_vc;
        // return $email;
        try{
        if($user)
        {
            Mail::send('partials.passwords', ['user' => $user], function ($m) use ($email)
            {
                $m->to($email)->subject('confirmation mail!');
            });
            return Redirect::to('/mythriop/home')->with('message', 'Password has sent to your  mail id!');
        }
        else
        {
            return redirect()->back()->withInput()
                    ->withErrors(array('message' => 'Sorry ! You are not an  user. Please check your Username .'));
        }
    }
        catch(\Exception $e)
        {
            // return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => 'Error in sending Password.'));
        }
    }




    public function changepassword(ChangeRequest $request)
    {
        $username =  $request->input('username');
        return view ('changepassword',['title'=>'Change Password','username'=>$username]);
    }

    public function savechangepassword(Request $request)
    {
        $username = $request->input('username');
        $currentpassword =  $request->input('currentpass');
        $newpassword =  $request->input('newpass');
        $confirmpassword =  $request->input('confirmpass');

        $user = UserMaster::where('um_username_vc',$username)
                            ->where('um_password_vc',$currentpassword)
                        ->first();

        // return ($newpassword."fg".$confirmpassword);
        try
        {
        if($user)
        {
            $email = $user->um_mailid_vc;
            if($newpassword == $confirmpassword)
            {
                $update = DB::select("UPDATE user_master set um_password_vc = '$newpassword' where um_username_vc = '$username'");

            Mail::send('partials.changepassword', ['username' => $username,'password'=>$newpassword], function ($m) use ($email)
            {
                $m->to($email)->subject('Password Changed Succesfully!');
            });
            return Redirect::to('/mythriop/login')->with('message', 'Password Changed Succesfully !');
            }
            else
            {
            return Redirect::to('/mythriop/login')->withInput()->withErrors(array('message' => 'Password Mismatch.'));
            }
        }
        else
        {
            return Redirect::to('/mythriop/login')->withInput()->withErrors(array('message' => 'Entered Current  Password is wrong.'));
        }
    }
        catch(\Exception $e)
        {
            return $e;
            return Redirect::to('/mythriop/login')->withInput()->withErrors(array('message' => 'Error in Changing Password.'));
        }
    }

}
