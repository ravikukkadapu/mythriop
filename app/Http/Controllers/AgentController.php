<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AgentRequest;
use App\Http\Controllers\Controller;
use DB;
use App\AgentCardIssueDetails;
use App\CardDetails;
use Redirect;

class AgentController extends Controller
{

    Public function issuecards(AgentRequest $request)
    {
        $agentname =$request->input('agentname');
        $agentid =$request->input('agentid');
        $marketedBy =$request->input('marketedBy');

        $servicetype1 =$request->input('servicetype1');
        $charge1 =$request->input('charge1');
        $noofcards1 =$request->input('noofcards1');
        $fromcard1 =$request->input('fromcard1');
        $tocard1 =$request->input('tocard1');
        $issuedate1 =$request->input('issuedate1');
        $totalamount1 =$request->input('totalamount1');
try{
        if($servicetype1!='' )
        {
        $type = new AgentCardIssueDetails;
        $type->ci_agentname_vc = $agentname;
        $type->ci_agentid_vc = $agentid;
        $type->ci_marketedby_vc = $marketedBy;
        $type->ci_servicetype_vc = $servicetype1;
        $type->ci_charge_fl = $charge1;
        $type->ci_noofcards_in = $noofcards1;
        $type->ci_fromcard_in = $fromcard1;
        $type->ci_tocard_in = $tocard1;
        $type->ci_issuedate_dt = $issuedate1;
        $type->ci_totalamount_fl = $totalamount1;
        $type->save();

        for($i=$fromcard1;$i<=$tocard1;$i++)
        {
            $card = new CardDetails;
            $card->cd_cardno_vc = 'EHOPC'.$i;
            $card->cd_cardid_in = $i;
            $card->cd_agentname_vc = $agentname;
            $card->cd_agentid_vc = $agentid;
            $card->cd_servicetype_vc = $servicetype1;
            $card->cd_issuedate_dt = $issuedate1;
            $card->cd_status_vc = 'Not Sold';
            $card->cd_sellingprice_fl = $charge1;
            $card->save();

        }
        $tocardno = $tocard1+1;
        $query = DB::select("alter sequence card_no restart with ".$tocardno);

        }

            return redirect()->back()->with('message', 'Succesfully issued Cards to '.$agentname .' from EHOPC-'.$fromcard1 .' to EHOPC-'.$tocard1 );
        }
        catch(\Exception $e)
        {
            return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to add Issue Cards'));
        }
    }

    public function lastcardno()
    {
        $card_no = DB::select('SELECT last_value FROM card_no');
        // $cardno = "MHOPC-".$card_no[0]->card_no;
        return $card_no;
    }

    public function getdetails($agentid,$servicetype,$date)
    {
        $query = DB::select("select * from agent_cardissue where ci_agentid_vc ='$agentid' and ci_servicetype_vc ='$servicetype' and ci_issuedate_dt ='$date'");
        return $query;
    }

    public function updateagent(AgentRequest $request,$agentid,$servicetype,$date)
    {
        $agentname =$request->input('agentname');
        $marketedBy =$request->input('marketedBy');
        $charge1 =$request->input('charge1');
        $noofcards1 =$request->input('noofcards1');
        $fromcard1 =$request->input('fromcard1');
        $tocard1 =$request->input('tocard1');
        $totalamount1 =$request->input('totalamount1');

        $query=DB::select("Update agent_cardissue set
            ci_agentname_vc = '$agentname',
            ci_marketedby_vc = '$marketedBy',
            ci_charge_fl = '$charge1',
            ci_noofcards_in = '$noofcards1',
            ci_fromcard_in = '$fromcard1',
            ci_tocard_in = '$tocard1',
            ci_totalamount_fl = '$totalamount1'
            where ci_agentid_vc ='$agentid' and ci_servicetype_vc ='$servicetype' and ci_issuedate_dt ='$date'");

        return redirect()->back()->with('message', $agentname.'Card Issued details updated succesfully.');
    }

    public function deleteagent($agentid,$servicetype,$date)
    {
        $query = DB::select("delete from agent_cardissue where ci_agentid_vc ='$agentid' and ci_servicetype_vc ='$servicetype' and ci_issuedate_dt ='$date'");
        $delete = DB::select("delete from card_details where cd_agentid_vc ='$agentid' and cd_servicetype_vc ='$servicetype' and cd_issuedate_dt ='$date'");
        return redirect()->back()->with('message', 'Card Issued details Deleted  succesfully.');
    }
}
