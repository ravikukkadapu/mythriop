<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use PDF;
use Session;

class AssociateReportsController extends Controller
{
    public function getdata(Request $request)
    {
        $searchtype = $request->input('searchtype');
        $name = $request->input('name');
        // $fromdate = date("d-m-Y", strtotime($request->input('fromdate')));
        // $todate = date("d-m-Y", strtotime($request->input('todate')));
        $fromdate =$request->input('fromdate');
        $todate = $request->input('todate');
        $servicetype = $request->input('servicetype');
        $status =$request->input('status');
        $query = "SELECT * FROM patient_data";
        $query_length = strlen($query);
        $duration = $request->input('duration');

        $cardexpiry = "Select * from op_registerdata";
        $cardexpiry_length = strlen($cardexpiry);

        if($duration !='')
        {
            $str = date('Y-m-d', strtotime('+1 years'));
            $getdate = date('Y-m-d',(strtotime ( '-'.$duration.' day' , strtotime ( $str) ) ));
            if($searchtype =='Cardexpiry')
            {
                if(strlen($query) > $query_length)
                {
                $cardexpiry .= " AND op_validupto_dt between '$getdate' and '$str' ";
                }
                else
                {
                $cardexpiry .= " WHERE op_validupto_dt between '$getdate' and '$str' ";
                }

            }
        }
        $cardexpiry .=" order by op_cardno_vc";
        $cardexpiryresult = DB::select($cardexpiry);
// return $cardexpiryresult;

        if($name !='')
        {
            if($searchtype =='Doctorwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_consultingdoctor_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE td_consultingdoctor_vc = '$name' ";
                }
            }

            if($searchtype =='OpCardwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_cardno_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE td_cardno_vc = '$name' ";
                }
            }


            if($searchtype =='Patientwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND op_patientname_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE op_patientname_vc = '$name' ";
                }
            }

            if($searchtype =='Departmentwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_department_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE td_department_vc = '$name' ";
                }
            }

            if($fromdate != '' and $todate != '')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_opdate_dt between '$fromdate' AND '$todate'";
                }
                else
                {
                $query .= " WHERE td_opdate_dt between '$fromdate' AND '$todate'";
                }
            }

        if($servicetype !='')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND op_schmetype_vc ='$servicetype'";
            }
            else
            {
                $query .= " WHERE op_schmetype_vc= '$servicetype'";
            }
        }
        }

    if($name =='' and $fromdate == '' and $todate == '' and $servicetype !='')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND op_schmetype_vc ='$servicetype'";
            }
            else
            {
                $query .= " WHERE op_schmetype_vc= '$servicetype'";
            }
        }

    if($name =='' and $fromdate != '' and $todate != '' and $servicetype !='')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND op_schmetype_vc ='$servicetype' AND td_opdate_dt between '$fromdate' AND '$todate'";
            }
            else
            {
                $query .= " WHERE op_schmetype_vc= '$servicetype' AND td_opdate_dt between '$fromdate' AND '$todate'";
            }
        }

    if($name =='' and $fromdate != '' and $todate != '' and $servicetype == '')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND td_opdate_dt between '$fromdate' AND '$todate'";
            }
            else
            {
                $query .= " WHERE td_opdate_dt between '$fromdate' AND '$todate'";
            }
        }
        // for  marketing person.

        $sample = " select * from op_registerdata ";
        $sample_length = strlen($sample);

        if($searchtype =='Marketingwise')
            {
                if($name !='')
                {
                    if(strlen($sample) > $sample_length)
                    {
                    $sample .= " AND op_marketedby_vc = '$name' ";
                    }
                    else
                    {
                    $sample .=" WHERE op_marketedby_vc='$name' ";
                    }
                }

                if($servicetype !='')
                {
                    if(strlen($sample) > $sample_length)
                    {
                    $sample .=" AND op_schmetype_vc='$servicetype' ";
                    }
                    else
                    {
                    $sample .=" WHERE op_schmetype_vc='$servicetype' ";
                    }
                }

                if($fromdate != '' and $todate != '')
                {
                    if(strlen($sample) > $sample_length)
                    {
                        $sample .= " AND td_opdate_dt between '$fromdate' AND '$todate'";
                    }
                    else
                    {
                        $sample .= " WHERE td_opdate_dt between '$fromdate' AND '$todate'";
                    }
                }
            }

        $deal = " select * from card_details a left join op_registerdata b  on  a.cd_cardno_vc = b.op_cardno_vc ";
        $deal_length = strlen($deal);

        if($searchtype =='Dealerwise')
            {
                if($name !='')
                {
                    if(strlen($deal) > $deal_length)
                    {
                    $deal .= " AND cd_agentname_vc = '$name' ";
                    }
                    else
                    {
                    $deal .=" where cd_agentname_vc='$name' ";
                    }
                }

                if($servicetype !='')
                {
                    if(strlen($deal) > $deal_length)
                    {
                    $deal .=" AND cd_servicetype_vc='$servicetype' ";
                    }
                    else
                    {
                    $deal .=" where cd_servicetype_vc='$servicetype' ";
                    }
                }

                if($fromdate != '' and $todate != '')
                {
                    if(strlen($deal) > $deal_length)
                    {
                        $deal .= " AND cd_issuedate_dt between '$fromdate' AND '$todate'";
                    }
                    else
                    {
                        $deal .= " where cd_issuedate_dt between '$fromdate' AND '$todate'";
                    }
                }
                if($status !='')
                {
                    if(strlen($deal) > $deal_length)
                    {
                        $deal .= " AND cd_status_vc = '$status'";
                    }
                    else
                    {
                        $deal .= " where cd_status_vc = '$status'";
                    }
                }
            }

        $deal .= " order by cd_cardno_vc ";


        // return $deal;

        $sampledealer = DB::select($deal);
        $resultopdata = DB::select($sample);
        // return $resultopdata;


        $query .= " order by td_department_vc , td_consultingdoctor_vc";
                // return $query;
        $result = DB::select($query);
        // return $query;
        $service=DB::select("select distinct stm_servicetype_vc from servicetype_master");
        // return $servicetype;
        return view('associatereports', ['title' => 'Reports','data'=>$result,'regdata'=>$resultopdata,'searchtype1'=>$searchtype,'name1'=>$name,'fromdate1'=>$fromdate,'todate1'=>$todate,'type'=>$service,'servicetype1'=>$servicetype,'sampledealer'=>$sampledealer,'status1'=>$status,'cardexpirydata'=>$cardexpiryresult,'duration1'=>$duration]);
    }


    public function printreport(Request $request)
    {
        $searchtype = $request->input('searchtype1');
        $servicetype = $request->input('servicetype1');
        $name = $request->input('name1');
        $fromdate = $request->input('fromdate1');
        $todate = $request->input('todate1');
        $query = "SELECT * FROM patient_data";
        $query_length = strlen($query);
        $status =$request->input('status1');
        $duration = $request->input('duration1');

        $cardexpiry = "Select * from op_registerdata";
        $cardexpiry_length = strlen($cardexpiry);

        if($duration !='')
        {
            $str = date('Y-m-d', strtotime('+1 years'));
            $getdate = date('Y-m-d',(strtotime ( '-'.$duration.' day' , strtotime ( $str) ) ));
            if($searchtype =='Cardexpiry')
            {
                if(strlen($query) > $query_length)
                {
                $cardexpiry .= " AND op_validupto_dt between '$getdate' and '$str' ";
                }
                else
                {
                $cardexpiry .= " WHERE op_validupto_dt between '$getdate' and '$str' ";
                }

            }
        }
        $cardexpiry .=" order by op_cardno_vc";
        $cardexpiryresult = DB::select($cardexpiry);

// return $cardexpiryresult;


        if($name !='')
        {
            if($searchtype =='Doctorwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_consultingdoctor_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE td_consultingdoctor_vc = '$name' ";
                }
            }
            if($searchtype =='OpCardwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_cardno_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE td_cardno_vc = '$name' ";
                }
            }

            if($searchtype =='Patientwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND op_patientname_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE op_patientname_vc = '$name' ";
                }
            }

            if($searchtype =='Departmentwise')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_department_vc = '$name' ";
                }
                else
                {
                $query .= " WHERE td_department_vc = '$name' ";
                }
            }

            if($fromdate != '' and $todate != '')
            {
                if(strlen($query) > $query_length)
                {
                $query .= " AND td_opdate_dt between '$fromdate' AND '$todate'";
                }
                else
                {
                $query .= " WHERE td_opdate_dt between '$fromdate' AND '$todate'";
                }
            }
         if($servicetype !='')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND op_schmetype_vc ='$servicetype'";
            }
            else
            {
                $query .= " WHERE op_schmetype_vc= '$servicetype'";
            }
        }
        }

         if($name =='' and $fromdate == '' and $todate == '' and $servicetype !='')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND op_schmetype_vc ='$servicetype'";
            }
            else
            {
                $query .= " WHERE op_schmetype_vc= '$servicetype'";
            }
        }
        if($name =='' and $fromdate != '' and $todate != '' and $servicetype !='')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND op_schmetype_vc ='$servicetype' AND td_opdate_dt between '$fromdate' AND '$todate'";
            }
            else
            {
                $query .= " WHERE op_schmetype_vc= '$servicetype' AND td_opdate_dt between '$fromdate' AND '$todate'";
            }
        }

         if($name =='' and $fromdate != '' and $todate != '' and $servicetype == '')
        {
            if(strlen($query) > $query_length)
            {
                $query .= " AND td_opdate_dt between '$fromdate' AND '$todate'";
            }
            else
            {
                $query .= " WHERE td_opdate_dt between '$fromdate' AND '$todate'";
            }
        }
        $query .= " order by td_opdate_dt";
            // return $query;
        $result = DB::select($query);
        $count = count($result);


        // for dealer and marketing person.

        $sample = " select * from op_registerdata ";
        $sample_length = strlen($sample);

        if($searchtype =='Marketingwise')
            {
                if($name !='')
                {
                    if(strlen($sample) > $sample_length)
                    {
                    $sample .= " AND op_marketedby_vc = '$name' ";
                    }
                    else
                    {
                    $sample .=" WHERE op_marketedby_vc='$name' ";
                    }
                }

                if($servicetype !='')
                {
                    if(strlen($sample) > $sample_length)
                    {
                    $sample .=" AND op_schmetype_vc='$servicetype' ";
                    }
                    else
                    {
                    $sample .=" WHERE op_schmetype_vc='$servicetype' ";
                    }
                }

                if($fromdate != '' and $todate != '')
                {
                    if(strlen($sample) > $sample_length)
                    {
                        $sample .= " AND td_opdate_dt between '$fromdate' AND '$todate'";
                    }
                    else
                    {
                        $sample .= " WHERE td_opdate_dt between '$fromdate' AND '$todate'";
                    }
                }
            }
        $sample .='order by op_cardno_vc';
        $resultopdata = DB::select($sample);
        $opdatacount = count($resultopdata);



        $deal = " select * from card_details a left join op_registerdata b  on  a.cd_cardno_vc = b.op_cardno_vc ";
        $deal_length = strlen($deal);

        if($searchtype =='Dealerwise')
            {
                if($name !='')
                {
                    if(strlen($deal) > $deal_length)
                    {
                    $deal .= " AND cd_agentname_vc = '$name' ";
                    }
                    else
                    {
                    $deal .=" where cd_agentname_vc='$name' ";
                    }
                }

                if($servicetype !='')
                {
                    if(strlen($deal) > $deal_length)
                    {
                    $deal .=" AND cd_servicetype_vc='$servicetype' ";
                    }
                    else
                    {
                    $deal .=" where cd_servicetype_vc='$servicetype' ";
                    }
                }

                if($fromdate != '' and $todate != '')
                {
                    if(strlen($deal) > $deal_length)
                    {
                        $deal .= " AND cd_issuedate_dt between '$fromdate' AND '$todate'";
                    }
                    else
                    {
                        $deal .= " where cd_issuedate_dt between '$fromdate' AND '$todate'";
                    }
                }
                if($status !='')
                {
                    if(strlen($deal) > $deal_length)
                    {
                        $deal .= " AND cd_status_vc = '$status'";
                    }
                    else
                    {
                        $deal .= " where cd_status_vc = '$status'";
                    }
                }
            }

        $deal .= " order by cd_cardno_vc ";
        $sampledealer = DB::select($deal);
        $dealcount = count($sampledealer);

// return $sample;
        if($fromdate != '' or $todate != '')
        {
        $fromdate = date("d-m-Y", strtotime($request->input('fromdate1')));
        $todate = date("d-m-Y", strtotime($request->input('todate1')));
            $pdf = PDF::loadview('associateprint',['data'=>$result,'regdata'=>$resultopdata,'reportname'=>$searchtype,'name'=>$name,'fromdate'=>$fromdate,'todate'=>$todate,'count'=>$count,'opdatacount'=>$opdatacount,'dealcount'=>$dealcount,'servicetype'=>$servicetype,'sampledealer'=>$sampledealer,'status'=>$status,'cardexpirydata'=>$cardexpiryresult,'duration'=>$duration])->setPaper('a4')->setOrientation('portrait')->setWarnings(false);
        return $pdf->stream();
        }
        else
        {
        $fromdate =$request->input('fromdate1');
        $todate =$request->input('todate1');
            $pdf = PDF::loadview('associateprint',['data'=>$result,'regdata'=>$resultopdata,'reportname'=>$searchtype,'name'=>$name,'fromdate'=>$fromdate,'todate'=>$todate,'count'=>$count,'opdatacount'=>$opdatacount,'dealcount'=>$dealcount,'servicetype'=>$servicetype,'sampledealer'=>$sampledealer,'status'=>$status,'cardexpirydata'=>$cardexpiryresult,'duration'=>$duration])->setPaper('a4')->setOrientation('portrait')->setWarnings(false);
        return $pdf->stream();
        }
    }


}
