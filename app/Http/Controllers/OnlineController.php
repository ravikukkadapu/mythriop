<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\PromoRequest;
use App\Http\Requests\SpecialRequest;
use App\Http\Controllers\Controller;
use App\CardDetails;
use App\OpRegisterdata;
use App\PromoCodeMaster;
use App\SpecialOffers;
use DB;
use PDF;
use Session;
use Redirect;


class OnlineController extends Controller
{
public function upload($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        $url = explode('/public/', $output_file);
        return getenv('SERVER_URL') . $url[1];
    }
    public function store(RegisterRequest $request)
    {
        // $cardno = strtoupper($request->input('cardno'));
        try{
        $headcardno = $request->input('headcardno');
        $headname = $request->input('headname');
        $opfees = $request->input('opfees');
        $sufix = $request->input('sufix');
        $Name = $request->input('Name');
        $photoid = $request->file('photoid');
        $age = $request->input('age');
        $sex = $request->input('sex');
        $mailid = $request->input('mailid');
        $contactNo = $request->input('contactNo');
        $issuedate = $request->input('issuedate');
        $validdate = $request->input('validupto');
        $validupto = date("Y-m-d", strtotime($validdate));
        $amount = $request->input('amount');
        $issuedby = $request->input('issuedby');
        $servicetype = $request->input('servicetype');
        $remarks = $request->input('remarks');
        $country = $request->input('country');
        $state = $request->input('state');
        $district = $request->input('district');
        $city = strtoupper($request->input('city'));
        $area = $request->input('area');
        $street = $request->input('street');
        $houseNo = $request->input('houseNo');
        $pincode = $request->input('pincode');
        $landMark = $request->input('landMark');
        $marketedBy = $request->input('marketedBy');

        $card_no = DB::select('select card_no from vw_cardno');
        $cardno = "EHOPC".$card_no[0]->card_no;

            $card = new CardDetails;
            $card->cd_cardno_vc = $cardno;
            $card->cd_cardid_in = $card_no[0]->card_no;
            $card->cd_agentname_vc = $issuedby;
            // $card->cd_agentid_vc = $agentid;
            $card->cd_servicetype_vc = $servicetype;
            $card->cd_issuedate_dt = $issuedate;
            $card->cd_status_vc = 'Not Sold';
            $card->cd_sellingprice_fl = $amount;
            $card->save();

        $deptnames = DB::select("select count(*) as count from op_registerdata where lower(op_cardno_vc) = lower('$cardno')");
        $count = $deptnames[0]->count;

        if($count == '1')
        {
            return redirect()->back()->withInput()->withErrors(array('message' => 'Card No  is already Issued'));
        }

        else
        {
            $opdata = new OpRegisterdata;
            if($photoid !='')
            {
                $photoid_img = file_get_contents($photoid);
            // return $photoid_img;
                $photoid_card = base64_encode($photoid_img);
                $photoidoutput_file = getenv('document_path').'opcard'.$cardno.'.jpg';
                $photoidimage_url = $this->upload($photoid_card, $photoidoutput_file);
                $photoid_url = "http://".$photoidimage_url;
            }
            else
            {
                $photoid_url = "";
            }

            $opdata->op_cardno_vc = $cardno;
            $opdata->op_opfees_fl = $opfees;
            $opdata->op_nameprefix_vc = $sufix;
            $opdata->op_patientname_vc = $Name;
            $opdata->op_age_in = $age;
            $opdata->op_sex_vc = $sex;
            $opdata->op_date_dt = $issuedate;
            $opdata->op_contactno_vc = $contactNo;
            $opdata->op_mailid_vc = $mailid;
            $opdata->op_photoid_vc = $photoid_url;
            $opdata->op_validupto_dt = $validupto;
            $opdata->op_schmetype_vc = $servicetype;
            $opdata->op_amount_fl = $amount;
            $opdata->op_issuedby_vc = $issuedby;
            $opdata->op_remarks_vc = $remarks;
            $opdata->op_country_vc = $country;
            $opdata->op_state_vc = $state;
            $opdata->op_district_vc = $district;
            $opdata->op_city_vc = $city;
            $opdata->op_area_vc = $area;
            $opdata->op_street_vc = $street;
            $opdata->op_landmark_vc = $landMark;
            $opdata->op_houseno_vc = $houseNo;
            $opdata->op_pincode_vc = $pincode;
            $opdata->op_marketedby_vc = $marketedBy;
            $opdata->save();


        $status = DB::select("UPDATE card_details set cd_status_vc='Sold' where cd_cardno_vc = '$cardno'");
        $tocardno = $cardno+1;
        $query = DB::select("alter sequence card_no restart with ".$tocardno);

        // $pdf = PDF::loadview('opcardreceipt',['opfees'=> $opfees,'sufix'=> $sufix,'Name'=> $Name,'amount'=> $amount,'issuedby'=> $issuedby,'cardno'=>$cardno,'servicetype'=>$servicetype,'opfees'=>$opfees])->setPaper('a4')->setOrientation('portrait')->setWarnings(false);
        // return $pdf->stream();
            return redirect()->back()->with('message', 'Op Registered successfully for '.$cardno);
    }
}
catch(\Exception $e)
{
    return $e;
        return redirect()->back()->withInput()->withErrors(array('message' => 'Error in Registration! '));
    }
}





// promo functions



    public function addpromocode(PromoRequest $request)
    {
        // return 123;
        $promocode = $request->input('promocode');
        $discount = $request->input('discount');
    try{
        $deptnames = DB::select("select count(*) as count from promocode_data where lower(pcd_promocode_vc) = lower('$promocode')");
        $count = $deptnames[0]->count;

        if($count == '1')
            {
                return redirect()->back()->withInput()->withErrors(array('message' => 'Promo Code Already Exists!'));
            }

        else
            {
                $promo = new PromoCodeMaster;
                $promo->pcd_promocode_vc = $promocode;
                $promo->pcd_discount_fl = $discount;
                $promo->save();
                return redirect()->back()->with('message', 'Promo Code Data Added Succesfully.');
            }
        }

    catch(\Exception $e)
        {
            // return $e;
            return redirect()->back()->withInput()->withErrors(array('message' => 'Error in Adding Promocode Data! '));
        }
    }






public function updatepromocode(PromoRequest $request)
{
       try
        {
            $code = $request->input('promocode');
            $discount = $request->input('discount');
            $userupdatequery = DB::select("UPDATE promocode_data SET
            pcd_discount_fl='$discount'
            WHERE pcd_promocode_vc = '$code'");

            return redirect()->to('mythriop/promocode')->with('message', 'Promocode Details updated successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update Promocode Details'));
        }
}

    public function deletepromocode($code)
    {
        try
        {
            $deleteuser = DB::select("DELETE from promocode_data where pcd_promocode_vc = '$code'");
            return redirect()->back()->with('message', 'promoCode deleted successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete promoCode'));
        }
    }

// for special offers



public function addspecialoffers(SpecialRequest $request)
{
        $agentid = Session::get('id');
// return $agentid;
        if($agentid == 'admin'){
            $agentname = $agentid;
        }
        else
        {
        $query = DB::Select("SELECT * from dealer_master where de_agentid_vc='$agentid'");
        $agentname =  $query[0]->de_agentname_vc .' - '. $query[0]->de_area_vc;
        }

        $subject = $request->input('title');
        $description = $request->input('description');
        $poster1 = $request->input('poster1');
        $poster2 = $request->input('poster2');

    try
    {
        $special = new SpecialOffers;
        $special->so_agentid_vc = $agentid;
        $special->so_agentname_vc = $agentname;
        $special->so_subject_vc = $subject;
        $special->so_desc_vc = $description;
        $special->so_poster1_tx = $poster1;
        $special->so_poster2_tx = $poster2;
        $special->so_status_vc = '0';
        $special->save();

        return redirect()->back()->with('message', 'Special Offer Added successfully');

    }
    catch (\Exception $e)
    {
        return $e;
        return Redirect::back()->withInput()->withErrors(array('message' => '!Error in Adding Special Offer/'));

    }

}

    public function pendingoffers()
    {
        $query = DB::select("SELECT * from specialoffers where so_status_vc = '0'");

        return view ('pendingspecialoffers',['title'=>'Pending Special Offers','data'=>$query]);
    }

    public function getofferdata($offerid)
    {
        $query = DB::select("SELECT * from specialoffers where so_specialid_in = '$offerid'");
        return $query;
        // return Response::json([
        //     'data'=>$query,
        //         ]);
    }


    public function updateofferdata($offerid)
    {
        $query = DB::select("update  specialoffers set so_status_vc ='1' where so_specialid_in = '$offerid'");

        return Response::json([
            'message'=>'Mail Sent to All OP Card Holders.',
            ]);
    }

}
