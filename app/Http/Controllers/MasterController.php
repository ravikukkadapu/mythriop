<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ServiceTypeRequest;
use App\Http\Requests\StaffRequest;
use App\Http\Requests\QualificationRequest;
use App\Http\Requests\UserMasterRequest;
use App\Http\Controllers\Controller;
use App\ServiceTypeMaster;
use App\StaffMaster;
use App\QualificationMaster;
use App\UserMaster;
use DB;
use Redirect;
use Session;
use Mail;

class MasterController extends Controller
{
    public function storeservicetype(ServiceTypeRequest $request)
    {
        $servicetype = $request->input('serviceType');
        $cost = $request->input('cost');
        $opfees = $request->input('opFees');
        $noofpersons = $request->input('noofpersons');
        $status = $request->input('status');

        $deptnames = DB::select("select count(*) as count from servicetype_master where lower(stm_servicetype_vc) = lower('$servicetype')");
        $count = $deptnames[0]->count;

        if($count == '1')
        {
            return redirect()->back()->withInput()->withErrors(array('message' => 'Service Type '.$servicetype.' is already Exists'));
        }
        else
        {
            try{
            $service = new ServiceTypeMaster;
            $serviceid = DB::select('select servicetype_code from vw_servicetype');
            $id = "EOPST-".$serviceid[0]->servicetype_code;
            // return $id;
            $service->stm_code_vc = $id;
            $service->stm_servicetype_vc = $servicetype;
            $service->stm_cost_fl = $cost;
            $service->stm_opfees_fl = $opfees;
            $service->stm_noofpersons_vc = $noofpersons;
            $service->stm_status_vc = $status;
            $service->save();
            return redirect()->back()->with('message', 'Service Type Details are saved successfully');
        }

        catch(\Exception $e)
        {
            // return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to add Service Type Details'));
        }
}
    }

    public function gettype($type)
    {
        $query = "SELECT * from servicetype_master where stm_code_vc = '$type'";
        $result = DB::select($query);
        return view('editservicetype',['data'=>$result,'title'=>'Edit Service Type']);
    }

    public function updatetype(ServiceTypeRequest $request,$code)
    {
        try
        {
            $servicetype = $request->input('serviceType');
            $cost = $request->input('cost');
            $opfees = $request->input('opFees');
            $noofpersons = $request->input('noofpersons');
        $status = $request->input('status');

            $userupdatequery = "UPDATE servicetype_master SET
            stm_servicetype_vc='$servicetype',
            stm_cost_fl='$cost',
            stm_opfees_fl='$opfees',
            stm_noofpersons_vc='$noofpersons',
            stm_status_vc = '$status'
            WHERE stm_code_vc = '$code'";
            //return $userupdatequery;
            $result = DB::select($userupdatequery);
            return redirect()->to('/mythriop/servicetype')->with('message', $code.' Service Type Details updated successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update Service Type Details'));
        }
    }

    public function deletetype($code)
    {
        //
        try
        {
            $deleteuser = "DELETE from servicetype_master where stm_code_vc = '$code'";
            $result = DB::select($deleteuser);
            return redirect()->back()->with('message', 'Service Type Details deleted successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete Service Type Details'));
        }
    }
    public function storestaff(StaffRequest $request)
    {

        $photoid= $request->file('photoid');
        $addressproof= $request->file('addressproof');
        $sufix = $request->input('sufix');
        $name = $request->input('name');
        $mobileno = $request->input('mobileno');
        $emailid = $request->input('emailid');
        $altmobileno = $request->input('altmobileno');
        $remarks = $request->input('remarks');
        $country = $request->input('country');
        $state = $request->input('state');
        $district = $request->input('district');
        $city = $request->input('city');
        $area = $request->input('area');
        $street = $request->input('street');
        $houseNo = $request->input('houseNo');
        $pincode = $request->input('pincode');
        $landMark = $request->input('landMark');

        $nameid  = Session::get('id');
        $uname = Session::get('username');

        if($nameid =='')
        {
             $associatename = 'admin';
        }
        else
        {
                $associatename = $nameid;
        }

        $deptnames = DB::select("select count(*) as count from staff_master where lower(sm_name_fl) = lower('$name') and sm_associatename_vc = '$associatename'");
        $count = $deptnames[0]->count;

        if($count == '1')
        {
             return Redirect::back()->withInput()->withErrors(array('message' =>' Marketing Staff Details already exists '));
        }
        else
        {
            $staff = new StaffMaster;
            $serviceid = DB::select('select staff_id from vw_staffid');
            $id = "Emp-".$serviceid[0]->staff_id;
            // return $id;
         if($photoid == '')
            {
                $idcard_url = "";
            }
            elseif($photoid != '')
            {
                $id_img = file_get_contents($photoid);
                $id_proof = base64_encode($id_img);
                $idoutput_file = getenv('document_path').$id.'photoid.jpg';
                $idimage_url = $this->upload($id_proof, $idoutput_file);
                $idcard_url = "http://".$idimage_url;
            }

         if($addressproof == '')
            {
                $proof_url = "";
            }
            elseif($addressproof != '')
            {
                $proof_img = file_get_contents($addressproof);
                $addr_proof = base64_encode($proof_img);
                $idoutput_file = getenv('document_path').$id.'photoid.jpg';
                $proofimage_url = $this->upload($addr_proof, $idoutput_file);
                $proof_url = "http://".$proofimage_url;
            }
            $staff->sm_staffid_vc = $id;
            $staff->sm_sufix_vc = $sufix;
            $staff->sm_name_fl = $name;
            $staff->sm_mobileno_vc = $mobileno;
            $staff->sm_emailid_vc = $emailid;
            $staff->sm_altmobileno_vc = $altmobileno;
            $staff->sm_associatename_vc = $associatename;
            $staff->sm_remarks_vc = $remarks;
            $staff->sm_country_vc = $country;
            $staff->sm_state_vc = $state;
            $staff->sm_district_vc = $district;
            $staff->sm_city_vc = strtoupper($city);
            $staff->sm_area_vc = $area;
            $staff->sm_street_vc = $street;
            $staff->sm_houseno_vc = $houseNo;
            $staff->sm_pincode_vc = $pincode;
            $staff->sm_landmark_vc = $landMark;
            $staff->sm_photoid_vc = $idcard_url;
            $staff->sm_addressproof_vc = $proof_url;
            $staff->save();
            return redirect()->back()->with('message', 'Marketing Staff Details are saved successfully');
        }

    }

    public function getstaff($staffid)
    {
        $query = "SELECT * from staff_master where sm_staffid_vc = '$staffid'";
        $result = DB::select($query);
        // return $result;
        $district =DB::select("select distinct district_name from district order by district_name");
        return view('editmarketingstaff',['data'=>$result,'district'=>$district,'title'=>'Edit Marketing Staff']);
    }

    public function updatestaff(StaffRequest $request,$code)
    {
        $sufix = $request->input('sufix');
        $name = $request->input('name');
        $mobileno = $request->input('mobileno');
        $emailid = $request->input('emailid');
        $altmobileno = $request->input('altmobileno');
        $remarks = $request->input('remarks');
        $country = $request->input('country');
        $state = $request->input('state');
        $district = $request->input('district');
        $city = $request->input('city');
        $area = $request->input('area');
        $street = $request->input('street');
        $houseNo = $request->input('houseNo');
        $pincode = $request->input('pincode');
        $landMark = $request->input('landMark');
        try
        {


            $userupdatequery = "UPDATE staff_master SET
            sm_sufix_vc='$sufix',
            sm_name_fl='$name',
            sm_mobileno_vc='$mobileno',
            sm_emailid_vc='$emailid',
            sm_altmobileno_vc = '$altmobileno',
            sm_country_vc = '$country',
            sm_state_vc = '$state',
            sm_district_vc = '$district',
            sm_city_vc = '$city',
            sm_area_vc = '$area',
            sm_street_vc = '$street',
            sm_houseno_vc = '$houseNo',
            sm_pincode_vc = '$pincode',
            sm_landmark_vc = '$landMark',
            sm_remarks_vc = '$remarks'
            WHERE sm_staffid_vc = '$code'";
            // return $userupdatequery;
            $result = DB::select($userupdatequery);
            return redirect()->to('mythriop/marketingstaffmaster')->with('message', 'Marketing Staff Details updated successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update Marketing Staff Details'));
        }
    }

    public function deletestaff($code)
    {
        //
        try
        {
            $deleteuser = "DELETE from staff_master where sm_staffid_vc = '$code'";
            $result = DB::select($deleteuser);
            return redirect()->back()->with('message', 'Marketing Staff deleted successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete Marketing Staff'));
        }
    }

    public function storequalification(QualificationRequest $request)
    {
        $qualification = $request->input('qualification');
        $status = $request->input('status');
        $names = DB::select("select count(*) as count from qualificaton_master where lower(qm_qualificaton_vc) = lower('$qualification')");
        $count = $names[0]->count;

        if($count =='1')
        {
            return redirect()->back()->withInput()->withErrors(array('message' => 'Qualification already exists '));
        }

        else
        {
            $qualitype = new QualificationMaster;
            $serviceid = DB::select('select qualification_id from vw_qualificationid');
            $id = "Qua-".$serviceid[0]->qualification_id;
            // return $id;
            $qualitype->qm_qualificatoncode_vc = $id;
            $qualitype->qm_qualificaton_vc = $qualification;
            $qualitype->qm_status_vc = $status;

            $qualitype->save();
            return redirect()->back()->with('message', 'Qualification Details are saved successfully');
        }

    }

    public function getqualification($code)
    {
        $query = DB::select("SELECT * from qualificaton_master where qm_qualificatoncode_vc = '$code'");
        return view('editquaflica',['data'=>$query,'title'=>'Edit Qualification']);
    }


public function updatequalification(QualificationRequest $request)
{
       try
        {
            $qualification = $request->input('qualification');
            $code = $request->input('qualificationcode');
        $status = $request->input('status');
            $userupdatequery = "UPDATE qualificaton_master SET
            qm_qualificaton_vc='$qualification',
            qm_status_vc='$status'
            WHERE qm_qualificatoncode_vc = '$code'";
            // return $userupdatequery;
            $result = DB::select($userupdatequery);
            return redirect()->to('/mythriop/qualificationmaster')->with('message', 'Qualification Details updated successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update Qualification Details'));
        }
}

    public function deletequalification($code)
    {
        try
        {
            $deleteuser = "DELETE from qualificaton_master where qm_qualificatoncode_vc = '$code'";
            $result = DB::select($deleteuser);
            return redirect()->back()->with('message', 'Qualification deleted successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete Qualification'));
        }
    }
    //usermaster




    PUBLIC function store(UserMasterRequest $request)
    {
        // return $request;
        try
        {
            $username = $request->input('userName');
            $password = $request->input('userName');
            $usertype = $request->input('userType');
            $associatename = $request->input('agentid');
            $emailid = $request->input('emailid');
            $associatetype = $request->input('associatetype');
            $status = $request->input('status');

            $names = DB::select("select count(*) as count from user_master where lower(um_username_vc) = lower('$username')");
            $count = $names[0]->count;

            if($count =='1')
            {
            return redirect()->back()->withInput()->withErrors(array('message' => 'Username already exists '));
            }

            else
            {
            $user = new UserMaster;
            $user->um_username_vc = $username;
            $user->um_password_vc = ($username);
            $user->um_usertype_in = $usertype;
            $user->um_associate_vc = $associatename;
            $user->um_mailid_vc = $emailid;
            $user->um_associatetype_vc = $associatetype;
            $user->um_status_vc = $status;
            $user->save();

            if($emailid !='')
            {
                Mail::send('partials.usermails', ['username' => $username,'password'=>$password,'associate'=>$associatename,'associatetype'=>$associatetype], function ($m) use ($emailid)
                {
                    $m->to($emailid)->subject('Login Details!');
                });
            }
            return redirect()->back()->with('message', 'Username is successfully created');
           }
        }

        catch(\Exception $e)
        {
            // return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => 'Error in Adding Data...'));
        }
    }

    public function getdetails($usertype, $username)
    {
        $query = "SELECT * FROM user_master WHERE um_usertype_in=$usertype and um_username_vc='$username'";
        //return $query;
        $result = DB::select($query);
        $associates = DB::select("select distinct de_agentname_vc from dealer_master order by de_agentname_vc ");

        return view('editusermaster', [ 'data' => $result,'associate'=>$associates,'title'=>'Edit User ']);
    }

    public function update(UserMasterRequest $request)
    {
            $username = $request->input('userName');
            $usertype = $request->input('userType');
        $associatename = $request->input('agentid');
        $emailid = $request->input('emailid');

        try
        {
            $userupdatequery = "UPDATE user_master SET um_mailid_vc = '$emailid' WHERE um_username_vc = '$username'and um_usertype_in='$usertype'";
            //return $userupdatequery;
            $result = DB::select($userupdatequery);
            return redirect()->to('mythriop/usermaster')->with('message', 'Username updated successfully');
        }
        catch(\Exception $e)
        {
            return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update username'));
        }
    }

    public function destroy($usertype, $username)
    {
        //
        try
        {
            $deleteuser = "DELETE from user_master where um_username_vc = '$username' and um_usertype_in='$usertype'";
            $result = DB::select($deleteuser);
            return redirect()->back()->with('message', 'Username deleted successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete username'));
        }
    }


    public function upload($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        $url = explode('/public/', $output_file);
        return getenv('SERVER_URL') . $url[1];
    }
}
