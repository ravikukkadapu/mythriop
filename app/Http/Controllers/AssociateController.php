<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AssociatetariffMasterRequest;
use App\Http\Controllers\Controller;
use DB;
use App\AssociateTariffs;
use App\ServiceTypeMaster;

class AssociateController extends Controller
{
    public function store(AssociatetariffMasterRequest $request)
    {
        $agentid = $request->input('agentid');
        $agentname = $request->input('agentname');
        $servicetype = $request->input('servicetype');
        $amount = $request->input('amount');
        $cost = $request->input('cost');
        $regcharges = $request->input('regcharges');
        $remarks = $request->input('remarks');
        $discount = $request->input('discount');

        $deptnames = DB::select("select count(*) as count from associate_tariffs where lower(at_agentname_vc) = lower('$agentname') and at_servicetype_vc ='$servicetype' ");
        $count = $deptnames[0]->count;

        if($count == '1')
        {
            return redirect()->back()->withInput()->withErrors(array('message' => 'Associate Tariff is already Exists'));
        }
        else
        {
            $amounts = ServiceTypeMaster::where('stm_servicetype_vc',$servicetype)->first();
            $amount = $amounts->stm_cost_fl;


            if($cost>$amount)
            {
                return redirect()->back()->withInput()->withErrors(array('message' => 'Cost is less than Service Charge for hospital.'));
            }
            else
            {
                $tariffs = new AssociateTariffs;
                $tariffs->at_agentid_vc = $agentid;
                $tariffs->at_agentname_vc = $agentname;
                $tariffs->at_servicetype_vc = $servicetype;
                $tariffs->at_serviceamount_fl = $amount;
                $tariffs->at_discount_fl = $discount;
                $tariffs->at_cost_fl = $cost;
                $tariffs->at_regcharges_fl = $regcharges;
                $tariffs->at_remarks_vc = $remarks;
                $tariffs->save();
                return redirect()->back()->with('message', 'Associate Tariff Added successfully');
            }
        }
    }

    public function getagentid($name,$area)
    {
        $query = DB::select("SELECT * from dealer_master where de_agentname_vc = '$name' and de_area_vc='$area'");
        return $query;
    }

    public function getagentname($id)
    {
        $query = DB::select("SELECT * from dealer_master where de_agentid_vc = '$id'");
        return $query;
    }

    public function getdata($id,$type)
    {
        $query = DB::select("SELECT * from associate_tariffs where at_servicetype_vc ='$type' and at_agentid_vc='$id' ");
        return $query;
    }

    public function update(AssociatetariffMasterRequest $request,$id,$type)
    {
        $agentname = $request->input('agentname');
        $cost = $request->input('cost');
        $regcharges = $request->input('regcharges');
        $remarks = $request->input('remarks');
        $amount = $request->input('amount');
        $discount = $request->input('discount');

        $amounts = ServiceTypeMaster::where('stm_servicetype_vc',$type)->first();
        $amount = $amounts->stm_cost_fl;
        if($cost>$amount)
        {
            return redirect()->back()->withInput()->withErrors(array('message' => 'Cost is less than Service Charge for hospital.'));
        }
        else
        {
            $query = "UPDATE associate_tariffs set
                    at_cost_fl = $cost,
                    at_regcharges_fl = $regcharges,
                    at_serviceamount_fl = $amount,
                    at_discount_fl = $discount,
                    at_remarks_vc = '$remarks' where at_servicetype_vc ='$type' and at_agentid_vc='$id' ";
            $updatequery=DB::select($query);

            return redirect()->back()->with('message', 'Associate Tariff Details is successfully updated');
        }
    }

    public function delete($id,$type)
    {
        $query = DB::select("DELETE from associate_tariffs where at_servicetype_vc ='$type' and at_agentid_vc='$id' ");
        return redirect()->back()->with('message', 'Associate Tariff Details is successfully updated');
    }
}
