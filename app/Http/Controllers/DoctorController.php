<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\DoctorRequest;
use App\Http\Controllers\Controller;
use App\DoctorDetails;
use DB;
use Session;
use Redirect;

class DoctorController extends Controller
{
    public function storedoctordetails(DoctorRequest $request)
    {

        $sufix =$request->input('sufix');
        $doctorname = $request->input('doctorName');
        $departmentname = $request->input('deptName');
        $registrationno = $request->input('regNo');
        $doctordesignation = $request->input('designation');
        $doctorspecialization = $request->input('specialization');
        $doctorvisittype = $request->input('visitType');
        $doctorstatus = $request->input('status');
        $credentials = $request->input('credentials');
        $cert = $request->file('regcert');
        $name = Session::get('username');


        $associatename = $request->input('dealerid');

        $deptnames = DB::select("select count(*) as count from doctor_details where dd_drname_vc ='$doctorname' and dd_department_vc ='$departmentname' and dd_associatename_vc = '$associatename'");
        $count = $deptnames[0]->count;
    // return $count;
        if($count == '1')
        {
        return redirect()->back()->withInput()->withErrors(array('message' => 'Doctor Name is already Exists in this Department.'));
        }
        else
        {
            $doctordetails = new DoctorDetails;
            $sq_doctorid = DB::select('select sq_doctorid from vw_sqdoctorid');
            $drid = "doctor-".$sq_doctorid[0]->sq_doctorid;
            $doctordetails->dd_drid_vc = $drid;
             $doctordetails->dd_prefix_vc = $sufix;
if($cert == '')
            {
                $idcard_url = "";
            }
            elseif($cert != '')
            {
                $id_img = file_get_contents($cert);
                $id_proof = base64_encode($id_img);
                $idoutput_file = getenv('document_path').$registrationno.'.jpg';
                $idimage_url = $this->upload($id_proof, $idoutput_file);
                $idcard_url = "http://".$idimage_url;
            }
            $doctordetails->dd_drname_vc = strtoupper($doctorname);
            $doctordetails->dd_department_vc = $departmentname;
            $doctordetails->dd_designation_vc = $doctordesignation;
            $doctordetails->dd_specialization_vc = $doctorspecialization;
            $doctordetails->dd_regno_vc = $registrationno;
            $doctordetails->dd_visittype_vc = $doctorvisittype;
            $doctordetails->dd_status_vc = $doctorstatus;
            $doctordetails->dd_associatename_vc = $associatename;
            $doctordetails->dd_credentials_vc = $credentials;
            $doctordetails->dd_certificate_vc = $idcard_url;
            $doctordetails->save();
            return redirect()->back()->with('message', 'Doctor Name is saved successfully');
        }
    }

    public function updatedoctordetails(DoctorRequest $request)
    {
           $doctorid = $request->input('doctorid');
            $sufix =$request->input('sufix');
            // return $sufix;
           $doctorname = $request->input('doctorName');
            $departmentname = $request->input('deptName');
            $registrationno = $request->input('regNo');
            $doctordesignation = $request->input('designation');
            $doctorspecialization = $request->input('specialization');
            $doctorvisittype = $request->input('visitType');
            $doctorstatus = $request->input('status');
        $credentials = $request->input('credentials');

        $name = Session::get('username');

        $query = DB::select("select * from user_master where um_username_vc = '$name'");
        $associatename = $query[0]->um_associate_vc;
        try{
            $doctordetails = "UPDATE doctor_details SET
            dd_prefix_vc='$sufix',
            dd_designation_vc='$doctordesignation',
            dd_specialization_vc='$doctorspecialization',
            dd_regno_vc='$registrationno',
            dd_visittype_vc='$doctorvisittype',
            dd_status_vc='$doctorstatus',
            dd_credentials_vc = '$credentials'
            WHERE dd_drid_vc='$doctorid'";
            // return $doctordetails;
            $result = DB::select($doctordetails);
            return redirect()->to('/mythriop/doctormaster')->with('message', 'Doctor Details updated successfully');
        }
        catch(\Exception $e)
        {
            // return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update Doctor Details'));
        }
    }

    public function getdoctordetails($doctorid)
    {
        $name = Session::get('id');

        $deptnames = DB::select("select distinct dm_deptname_vc from department_master where dm_associatename_vc ='$name' order by dm_deptname_vc");

        $qualifiationdata = DB::select("select distinct qm_qualificaton_vc from qualificaton_master order by qm_qualificaton_vc");

        $query = DB::select("SELECT * FROM doctor_details WHERE dd_drid_vc='$doctorid'");

        return view('editdoctor',['data'=>$query,'department'=>$deptnames,'qualifiationdata'=>$qualifiationdata,'title'=>'Edit Doctor']);
    }

    public function deletedoctordetails($doctorid)
    {
        try
        {
            $doctordetails = "delete from doctor_details WHERE dd_drid_vc='$doctorid'";
            $result = DB::select($doctordetails);
            // $deletetr = "DELETE  FROM transaction_data where td_department_vc = '$doctorid'";
            // $resulttr = DB::select($deletetr);
            return redirect()->back()->with('message', 'Doctor Details deleted successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete Doctor Details'));
        }
    }


    public function upload($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        $url = explode('/public/', $output_file);
        return getenv('SERVER_URL') . $url[1];
    }
}
