<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DealerMasterRequest;
use App\Http\Controllers\Controller;
use App\DealerMaster;
use DB;
use Redirect;
use App\DepartmentFacilities;
use App\OutPatientFacilities;

class DealerController extends Controller
{
public function storedealer(DealerMasterRequest $request)
{

    $agentname = $request->input("agentname");
    $gstno = $request->input("gstno");
    $cstno = $request->input("cstno");
    $prefix = $request->input("prefix");
    $contactname = $request->input("contactname");
    $contactNo = $request->input("contactNo1");
    $mailid = $request->input("mailid");
    $country = $request->input("country");
    $state = $request->input("state");
    $district = $request->input("district");
    $city = STRTOUPPER($request->input("city"));
    $area = $request->input("area");
    $street = $request->input("street");
    $houseNo = $request->input("houseNo");
    $pincode = $request->input("pincode");
    $landMark = $request->input("landMark");
    $marketedBy = $request->input('marketedBy');
    $agenttype = $request->input('agenttype');
    $category = $request->input('category');
    $enrolldt = $request->input('enrolldt');
    $contactNo2 = $request->input('contactNo2');
    $emergency = $request->input('emergency');
    $facilities = $request->input('drregno');
    $documentproof = $request->file('documentproof');




try{
    $deptnames = DB::select("select count(*) as count from dealer_master where lower(de_agentname_vc) = lower('$agentname') and de_contactno_vc='$contactNo'");
    $count = $deptnames[0]->count;
    // return $query;

    if($count == '1')
    {
        return redirect()->back()->withInput()->withErrors(array('message' => 'Dealer Name is already Exists'));
    }
    else
    {


    $dealerid = DB::select('select dealer_id from vw_dealer_id');
    // return $dealerid;
    $agentid = 'Dealer-'.$dealerid[0]->dealer_id;
            if($documentproof == '')
            {
                $idcard_url = "";
            }
            elseif($documentproof != '')
            {
                $id_img = file_get_contents($documentproof);
                $id_proof = base64_encode($id_img);
                $idoutput_file = getenv('document_path').$agentid.'document.jpg';
                $idimage_url = $this->upload($id_proof, $idoutput_file);
                $idcard_url = "http://".$idimage_url;
            }
    $dealer = new DealerMaster;
    $dealer->de_agentid_vc = $agentid;
    $dealer->de_agentname_vc = $agentname;
    $dealer->de_gstno_vc = $gstno;
    $dealer->de_cstno_vc = $cstno;
    $dealer->de_nameprefix_vc = $prefix;
    $dealer->de_contactname_vc = $contactname;
    $dealer->de_contactno_vc = $contactNo;
    $dealer->de_mailid_vc = $mailid;
    $dealer->de_country_vc = $country;
    $dealer->de_state_vc = $state;
    $dealer->de_district_vc = $district;
    $dealer->de_city_vc = $city;
    $dealer->de_area_vc = $area;
    $dealer->de_street_vc = $street;
    $dealer->de_houseno_vc = $houseNo;
    $dealer->de_pincode_vc = $pincode;
    $dealer->de_landmark_vc = $landMark;
    $dealer->de_marketingby_vc = $marketedBy;
    $dealer->de_agenttype_vc = $agenttype;
    $dealer->de_hospitalcategory_vc = $category;
    $dealer->de_enrolldate_dt = $enrolldt;
    $dealer->de_contactno2_vc = $contactNo2;
    $dealer->de_emergency_vc = $emergency;
    $dealer->de_document_vc = $idcard_url;
    $dealer->de_drregno_vc = $facilities;
    $dealer->save();

    $id = DealerMaster::all() -> last();
    $fac = $request->input('color');
    // return $fac;
    $count = count($fac);
    for($i = 0;$i<$count;$i++)
     {
    $opf = new OutPatientFacilities;

    $opf->de_agentid_vc = $id->de_agentid_vc;
    $opf->dfm_deptname_vc= $fac[$i];
    $opf->save();
     }

    return redirect()->back()->with('message', 'Dealer Name Registered successfully with code : '.$agentid);
    }
}
    catch (\Exception $e)
    {
        // return $e;
        return redirect()->back()->withInput()->withErrors(array('message' => 'Dealer Name is Already Exists'));
    }
}


public function checkdealer($agentname,$contactNo)
{
    $deptnames = DB::select("select count(*) as count from dealer_master where lower(de_agentname_vc) = lower('$agentname') and de_contactno_vc='$contactNo'");

    $count = $deptnames[0]->count;
     return $count;

}

 public function getdealerdata($agentid)
 {
    $data = DB::select("Select * from dealer_master where de_agentid_vc = '$agentid'");
        $district =DB::select("select distinct district_name from district  order by district_name  ");
        $deptfacilities =DepartmentFacilities::where('dfm_status_vc','Active')->orderBy('dfm_deptname_vc','asc')->get();
        $categories = DB::select("select distinct ctm_categoryname_vc from category_master where ctm_categorystatus_vc='Active' order by ctm_categoryname_vc  ");
        // return $data;
        return view('editdealer',['title' => 'Dealer Master','district'=>$district,'data'=>$data,'categories'=>$categories,'deptfacilities'=>$deptfacilities,'data'=>$data]);
 }

    public function updatedealer(DealerMasterRequest $request)
    {
        // return 123;

    $agentid = $request->input('agentid');
    $facilities = $request->input('drregno');
        $agentname = $request->input("agentname");
        $gstno = $request->input("gstno");
        $cstno = $request->input("cstno");
        $prefix = $request->input("prefix");
        $contactname = $request->input("contactname");
        $contactNo = $request->input("contactNo1");
        $mailid = $request->input("mailid");
        $country = $request->input("country");
        $state = $request->input("state");
        $district = $request->input("district");
        $city = STRTOUPPER($request->input("city"));
        $area = $request->input("area");
        $street = $request->input("street");
        $houseNo = $request->input("houseNo");
        $pincode = $request->input("pincode");
        $landMark = $request->input("landMark");
    $marketedBy = $request->input('marketedBy');
    $agenttype = $request->input('agenttype');
    $category = $request->input('category');
    $enrolldt = $request->input('enrolldt');
    $contactNo2 = $request->input('contactNo2');
    $emergency = $request->input('emergency');
    $de_drregno_vc = $request->input('facilities');
    $documentproof = $request->file('documentproof');
    // return $documentproof;

            if($documentproof == '')
            {
            $query = DB::select("SELECT de_document_vc from dealer_master where de_agentid_vc = '$agentid'");
            $idcard_url = $query[0]->de_document_vc;
            }
            elseif($documentproof != '')
            {
                $id_img = file_get_contents($documentproof);
                $id_proof = base64_encode($id_img);
                $idoutput_file = getenv('document_path').$agentid.'document.jpg';
                $idimage_url = $this->upload($id_proof, $idoutput_file);
                $idcard_url = "http://".$idimage_url;
            }
try{
        $query = DB::select("DELETE from outpatient_facilities where de_agentid_vc = '$agentid'");
        $query = "UPDATE dealer_master set
        de_agentname_vc = '$agentname',
        de_gstno_vc = '$gstno',
        de_cstno_vc = '$cstno',
        de_nameprefix_vc = '$prefix',
        de_contactname_vc = '$contactname',
        de_contactno_vc = '$contactNo',
        de_mailid_vc = '$mailid',
        de_country_vc = '$country',
        de_state_vc = '$state',
        de_district_vc = '$district',
        de_city_vc = '$city',
        de_area_vc = '$area',
        de_street_vc = '$street',
        de_houseno_vc = '$houseNo',
        de_pincode_vc = '$pincode',
        de_landmark_vc = '$landMark',
        de_marketingby_vc = '$marketedBy',
        de_agenttype_vc = '$agenttype',
        de_hospitalcategory_vc ='$category',
        de_enrolldate_dt ='$enrolldt',
        de_contactno2_vc ='$contactNo2',
        de_emergency_vc ='$emergency',
        de_document_vc = '$idcard_url'
         where de_agentid_vc = '$agentid' ";
        // return $query;
        $updatequery=DB::select($query);


    $fac = $request->input('color');
    // return $fac;
    $count = count($fac);
    for($i = 0;$i<$count;$i++)
     {
    $opf = new OutPatientFacilities;

    $opf->de_agentid_vc = $agentid;
    $opf->dfm_deptname_vc= $fac[$i];
    $opf->save();
     }

        return redirect()->back()->with('message', 'Dealer Details is successfully updated '.$agentid);
    }
    catch (\Exception $e)
    {
        return $e;
        return redirect()->back()->withInput()->withErrors(array('message' => 'Dealer Name is not Updated'));
    }
    }

    public function deletedealer($agentid)
    {
        try{
        $query = DB::select("DELETE from dealer_master where de_agentid_vc = '$agentid'");
        return redirect()->back()->with('message', 'Dealer Details is successfully Deleted '.$agentid);
    }
    catch (\Exception $e)
    {
        return redirect()->back()->withInput()->withErrors(array('message' => 'Dealer Name is not Deleted'));
    }
    }

public function upload($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        $url = explode('/public/', $output_file);
        return getenv('SERVER_URL') . $url[1];
    }
public function getfacilities($agentid)
{
    $data = DB::select("Select * from outpatient_facilities where de_agentid_vc = '$agentid'");
    return $data;
}
}
