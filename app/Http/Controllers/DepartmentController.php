<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Departmentrequest;
use DB;
use Redirect;
use App\DepartmentDetails;
use Validator;
use Input;
use Session;


class DepartmentController extends Controller
{
    public function store(Departmentrequest $request)
    {

    $departmentname=$request->input('DepartmentName');
    $phoneno=$request->input('PhoneNo');
    $departmentheadname=$request->input('DeptHeadname');
    $noofdoctors=$request->input('NoOfDoctors');
    $mobileno=$request->input('MobileNo');
    $location=$request->input('location');
    $noofnurses=$request->input('NoOfNurses');
    $noofsuppstaff=$request->input('NoOfSuppStaff');
    $roomno=$request->input('RoomNo');
    $remarks=$request->input('Remarks');
    $assname = Session::get('username');
    $associatename =$request->input('name');

    $deptnames = DB::select("select count(*) as count from department_master where dm_deptname_vc = '$departmentname' and dm_associatename_vc = '$associatename'");
    $count = $deptnames[0]->count;

    if($count == '1')
    {
        return redirect()->back()->withInput()->withErrors(array('message' => 'Department Name is already Exists'));
    }
    else
    {

   $deptdetails= new DepartmentDetails;
            $department_code = DB::select('select department_code from vw_department_code');
            $dept = "dept-".$department_code[0]->department_code;
   $deptdetails->dm_departmentcode_vc = $dept;
   $deptdetails->dm_deptname_vc =strtoupper($departmentname);
   $deptdetails->dm_deptheadname_vc =$departmentheadname;
   $deptdetails->dm_location_vc=$location;
   $deptdetails->dm_mobileno_vc=$mobileno;
   $deptdetails->dm_phoneno_vc=$phoneno;
   $deptdetails->dm_noofdoctors_vc=$noofdoctors;
   $deptdetails->dm_noofnurses_vc=$noofnurses;
   $deptdetails->dm_noofsuppstaff_vc=$noofsuppstaff;
   $deptdetails->dm_roomno_vc=$roomno;
   $deptdetails->dm_remarks_vc=$remarks;
   $deptdetails->dm_associatename_vc = $associatename;
    $deptdetails->dm_username_vc = $assname;
   // $deptdetails->id='0';
   $deptdetails->save();

   // return 123;
    return redirect()->back()->with('message','Department Name Added Successfully');
    }

    }

    public function edit($departmentcode)
    {
        $query = "SELECT * from department_master where dm_departmentcode_vc = '$departmentcode'";
        $result = DB::select($query);
        $name = Session::get('username');
        $query = DB::select("select * from user_master where um_username_vc = '$name'");
        $val = $query[0]->um_associate_vc;
        return view('editdept',['data'=>$result,'val'=>$val,'title'=>'Edit Department']);
    }

    public function update(Departmentrequest $request)
    {
       try
        {
          $departmentcode=$request->input('DepartmentCode');
          $departmentname=$request->input('DepartmentName');
          $departmentheadname=$request->input('DeptHeadname');
          $mobileno=$request->input('MobileNo');
           $roomno=$request->input('RoomNo');
           $location=$request->input('location');
          $query = "UPDATE department_master set
            dm_deptname_vc = UPPER('$departmentname'),
            dm_deptheadname_vc = '$departmentheadname',
            dm_mobileno_vc = '$mobileno',dm_roomno_vc = '$roomno',
            dm_location_vc = '$location'
            where dm_departmentcode_vc = '$departmentcode'";
            // return $query;
            $result = DB::select($query);
            return redirect()->to('/mythriop/departmentmaster')->with('message', 'Department Name is successfully updated');
          }
          catch(\Exception $e)
        {
          // return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => 'Department Name is not updated'));
        }


    }


    public function delete($department)
    {

    $deptnames = DB::select("select count(*) as count from doctor_details where dd_department_vc = '$department'");
    $count = $deptnames[0]->count;
    // return $count;
    if($count >= '0')
    {
            $deletedr = "DELETE  FROM doctor_details where dd_department_vc = '$department'";
            $resultdr = DB::select($deletedr);
    }
            $query = "DELETE  FROM department_master where dm_deptname_vc = '$department'";
            $result = DB::select($query);

            return redirect()->back()->with('message', 'Department Name is successfully deleted after '.$count .' Doctors are Deleted from Doctor Master.');
    }

    public function cancel()
    {
        return redirect("/mythriop/departmentmaster");
    }
}
