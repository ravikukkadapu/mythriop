<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use PDF;
use App\OpRegisterdata;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\RegisterupRequest;
use Session;
use Redirect;
use App\ServiceTypeMaster;
use DateTime;

class HomeController extends Controller
{
public function upload($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        $url = explode('/public/', $output_file);
        return getenv('SERVER_URL') . $url[1];
    }
//--opcard functions

// RegisterRequest
    public function storeopdata(RegisterRequest $request)
    {
         // return Session::get('username');

        $cardno = strtoupper($request->input('cardno'));
        $headcardno = $request->input('headcardno');
        $headname = $request->input('headname');
        $opfees = $request->input('opfees');
        $sufix = $request->input('sufix');
        $Name = $request->input('Name');

        $photoid = $request->file('photoid');

        $age = $request->input('age');
        $sex = $request->input('sex');
        $mailid = $request->input('mailid');
        $contactNo = $request->input('contactNo');
        $issuedate = $request->input('issuedate');
        $validupto = $request->input('validupto');

$date = new DateTime($validupto);
$valid =  $date->format('Y-m-d');
        $amount = $request->input('amount');
        $issuedby = $request->input('issuedby');
        $servicetype = $request->input('servicetype');
        $remarks = $request->input('remarks');
        $country = $request->input('country');
        $state = $request->input('state');
        $district = $request->input('district');
        $city = strtoupper($request->input('city'));
        $area = $request->input('area');
        $street = $request->input('street');
        $houseNo = $request->input('houseNo');
        $pincode = $request->input('pincode');
        $landMark = $request->input('landMark');
        $marketedBy = $request->input('marketedBy');

try{
// return $photoid;
    $deptnames = DB::select("select count(*) as count from op_registerdata where lower(op_cardno_vc) = lower('$cardno')");
    $count = $deptnames[0]->count;
    $data = ServiceTypeMaster::where('stm_servicetype_vc',$servicetype)->first();
    $persons = $data->stm_noofpersons_vc;
    // return $persons;
    if($count >= $persons)
    {
        return redirect()->back()->withInput()->withErrors(array('message' => 'All '.$persons .'Cards   are already Issued'));
    }
    else
    {
            $opdata = new OpRegisterdata;
            $opdata->op_cardno_vc = $cardno;

            if($photoid !='')
            {
                $photoid_img = file_get_contents($photoid);
            // return $photoid_img;
                $photoid_card = base64_encode($photoid_img);
                $photoidoutput_file = getenv('document_path').'opcard'.$cardno.'.jpg';
                $photoidimage_url = $this->upload($photoid_card, $photoidoutput_file);
                $photoid_url = "http://".$photoidimage_url;
            }
            else
            {
                $photoid_url = "";
            }

            $opdata->op_opfees_fl = $opfees;
            $opdata->op_nameprefix_vc = $sufix;
            $opdata->op_patientname_vc = $Name;
            $opdata->op_age_in = $age;
            $opdata->op_sex_vc = $sex;
            $opdata->op_date_dt = $issuedate;
            $opdata->op_contactno_vc = $contactNo;
            $opdata->op_mailid_vc = $mailid;
            $opdata->op_photoid_vc = $photoid_url;
            $opdata->op_validupto_dt = $valid;
            $opdata->op_schmetype_vc = $servicetype;
            $opdata->op_amount_fl = $amount;
            $opdata->op_issuedby_vc = $issuedby;
            $opdata->op_remarks_vc = $remarks;
            $opdata->op_country_vc = $country;
            $opdata->op_state_vc = $state;
            $opdata->op_district_vc = $district;
            $opdata->op_city_vc = $city;
            $opdata->op_area_vc = $area;
            $opdata->op_street_vc = $street;
            $opdata->op_landmark_vc = $landMark;
            $opdata->op_houseno_vc = $houseNo;
            $opdata->op_pincode_vc = $pincode;
            $opdata->op_marketedby_vc = $marketedBy;

        if($count <= $persons && $count >0)
        {
                $opdata->save();

            $val = $persons-$count;
            if($val <= 0)
            {
                // $opdata->save();
            $status = DB::select("UPDATE card_details set cd_status_vc='Sold' where cd_cardno_vc = '$cardno'");
            return redirect()->back()->with('message', 'All Op Cards  Registered successfully for '.$cardno);
            }
            else
            {
                return redirect()->back()->with('message', 'Op Registered successfully for '.$cardno. ' You have remaining '.$val.'  Cards');
            }
        }
    }
}
        catch(\Exception $e)
        {
            // return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to Register Op card Data'));
        }
    }




    public function edit($cardno)
    {
        $query = "SELECT * from op_registerdata where UPPER(op_cardno_vc) =UPPER('$cardno')";
        $result = DB::select($query);
        return $result;
    }

    public function update(RegisterupRequest $request,$cardno)
    {
        try
        {
        $opfees = $request->input('opfees');
        $sufix = $request->input('sufix');
        $Name = $request->input('Name');
        $photoid = $request->file('photoid');
        $age = $request->input('age');
        $sex = $request->input('sex');
        $mailid = $request->input('mailid');
        $contactNo = $request->input('contactNo');
        $issuedate = $request->input('issuedate');
        $validupto = $request->input('validupto');
        $amount = $request->input('amount');
        $issuedby = $request->input('issuedby');
        $servicetype = $request->input('servicetype');
        $remarks = $request->input('remarks');
        $country = $request->input('country');
        $state = $request->input('state');
        $district = $request->input('district');
        $city = strtoupper($request->input('city'));
        $area = $request->input('area');
        $street = $request->input('street');
        $houseNo = $request->input('houseNo');
        $pincode = $request->input('pincode');
        $landMark = $request->input('landMark');
        $marketedBy = $request->input('marketedBy');
// return $photoid;
        if($photoid !='')
        {
            $photoid_img = file_get_contents($photoid);
            // return $photoid_img;
            $photoid_card = base64_encode($photoid_img);
            $photoidoutput_file = getenv('document_path').'opcard'.$cardno.'.jpg';
            $photoidimage_url = $this->upload($photoid_card, $photoidoutput_file);
            $photoid_url = "http://".$photoidimage_url;
            // return $photoid_url;
        }
        else
        {
            $query = DB::select("SELECT op_photoid_vc from op_registerdata where op_cardno_vc = '$cardno'");
            $photoid_url = $query[0]->op_photoid_vc;
        }

            $query = DB::select("Update op_registerdata set
            op_opfees_fl = '$opfees',
            op_nameprefix_vc='$sufix',
            op_patientname_vc = '$Name',
            op_age_in = '$age',
            op_sex_vc = '$sex',
            op_date_dt = '$issuedate',
            op_contactno_vc = '$contactNo',
            op_mailid_vc = '$mailid',
            op_photoid_vc = '$photoid_url',
            op_validupto_dt = '$validupto',
            op_schmetype_vc = '$servicetype',
            op_amount_fl = '$amount',
            op_issuedby_vc = '$issuedby',
            op_remarks_vc = '$remarks',
            op_country_vc = '$country',
            op_state_vc = '$state',
            op_district_vc = '$district',
            op_city_vc = '$city',
            op_area_vc = '$area',
            op_street_vc = '$street',
            op_landmark_vc = '$landMark',
            op_houseno_vc = '$houseNo',
            op_pincode_vc = '$pincode',
             op_marketedby_vc ='$marketedBy' where UPPER(op_cardno_vc) =UPPER('$cardno')
                ");
            return redirect()->back()->with('message', 'Op Card Data Updated successfully for '.$cardno);
        }
        catch(\Exception $e)
        {
            // return $e;
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to Update Data'));
        }
    }

    public function delete($cardno)
    {
        try{
            $deleteuser = "DELETE from op_registerdata where op_cardno_vc = '$cardno'";
            $result = DB::select($deleteuser);
            return redirect()->back()->with('message', 'Op Card Data deleted successfully for '.$cardno);
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete'));
        }
    }


    public function opcard($cardno)
    {
    $data = OpRegisterdata::where('op_cardno_vc',$cardno)->first();
    // return $cardno;
    // return view ('opcard',['data'=>$data]);

    $pdf = PDF::loadview('opcard',['data'=>$data])->setPaper('a4')->setOrientation('portrait')->setWarnings(false);
    return $pdf->stream();
    }


    public function printcard($cardno,$name,$age)
    {
    $data = OpRegisterdata::where('op_cardno_vc',$cardno)->where('op_patientname_vc',$name)->where('op_age_in',$age)->first();
    // return $cardno;
    // return view ('opcard',['data'=>$data]);

    $pdf = PDF::loadview('opcard',['data'=>$data])->setPaper('a4')->setOrientation('portrait')->setWarnings(false);
    return $pdf->stream();
    }



    public function serviceopamount($type,$agentname)
    {
        $query = "SELECT * from servicetype_master where stm_servicetype_vc = '$type' ";
        $result = DB::select($query);
        return $result;
    }

    public function serviceamount($type,$agentname)
    {
       $query = "SELECT * from associate_tariffs where at_servicetype_vc = '$type' and at_agentid_vc = '$agentname'";
        $result = DB::select($query);
        return $result;
    }

    public function getheadname($headcardno)
    {
        $query = "SELECT * from op_registerdata where op_cardno_vc = '$headcardno'";
        $result = DB::select($query);
        return $result;
    }

    public function getheadcardno($headname)
    {
        $query = "SELECT * from op_registerdata where lower(op_patientname_vc) = lower('$headname')";
        $result = DB::select($query);
        return $result;
    }
}
