<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\QueryRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\FeedBackRequest;
use App\Http\Requests\ReplyRequest;
use App\Http\Controllers\Controller;
use DB;
use App\FeedbackData;
use App\CategoryMaster;
use App\QueryData;
use Redirect;
use Mail;
use App\DepartmentFacilities;

class GuestController extends Controller
{

    public function getallhospdata(Request $request)
    {
        // $associatename = $request->input('associatename');
        $state = $request->input('state');
        $city = $request->input('city');
        $area = $request->input('area');
        $dist = $request->input('district');
        $department = $request->input('department');
        $servicetype = $request->input('servicetype');
        $category = $request->input('category');
        $query = " select * from vw_sampledata where de_agenttype_vc = 'Hospital' ";
        $quer_len = strlen($query);


        if($department !='')
        {

            $query .= " AND dfm_deptname_vc =  '$department'";
        }

        if($category !='')
        {
            $query .= " AND de_hospitalcategory_vc =  '$category'";
        }

        // if($associatename !='')
        // {
        //     $query .= " AND de_agentname_vc =  '$associatename'";
        // }

        if($state !='')
        {
            $query .= " AND de_state_vc =  '$state'";
        }

        if($dist !='')
        {
            $query .= " AND de_district_vc =  '$dist'";
        }

        if($city !='')
        {
            $query .= " AND de_city_vc =  '$city'";
        }

        if($area !='')
        {
            $query .= " AND de_area_vc =  '$area'";
        }

        // if($servicetype !='')
        // {
        //     $query .= " AND ci_servicetype_vc =  '$servicetype'";
        // }

        $query .= " order by de_agentname_vc";
        // return $query;
        $result  = DB::select($query);
// return $result;
        // $associates = DB::select("select distinct de_agentname_vc from dealer_master where de_agenttype_vc = 'Hospital' order by de_agentname_vc ");

        $district =DB::select("select distinct district_name from district order by district_name");

        $dept =DepartmentFacilities::orderBy('dfm_deptname_vc','asc')->get();

        $servicetypedata = DB::select("select distinct stm_servicetype_vc from servicetype_master ");
        $categories = DB::select("select distinct ctm_categoryname_vc from category_master  order by ctm_categoryname_vc  ");
        return view ('searchpage',['title' => 'Search Hospitals','data'=>$result,'state' => $state,'city' => $city,'area'=>$area,'district'=>$district,'dist'=>$dist,'dept'=>$dept,'department'=>$department,'servicetype'=>$servicetype,'servicetypedata'=>$servicetypedata,'category'=>$category,'categoriesdata'=>$categories]);

    }


    public function sendfeedback(Request $request)
    {
        $complaint = $request->input('complaint');
        $treatedby = $request->input('treatedby');
        $complainttype = $request->input('complainttype');
        $cardno = $request->input('cardno');
        $phoneno = $request->input('phoneno');
        $patientname = $request->input('patientname');
        $visitid = $request->input('visitid');

        try{
        $feedback = new FeedbackData;
        $feedback->fd_complaint_vc = $complaint;
        $feedback->fd_treatedby_vc = $treatedby;
        $feedback->fd_complainttype_vc = $complainttype;
        $feedback->op_patientname_vc = $patientname;
        $feedback->op_cardno_vc = $cardno;
        $feedback->op_mobileno_vc = $phoneno;
        $feedback->fd_visitid_i = $visitid;
        $feedback->save();

        $adminmailid  = 'ravik10892@gmail.com';
        $ass = DB::select("SELECT de_mailid_vc from dealer_master where de_agentname_vc = '$hospname' and de_hospitalcategory_vc = '$agenttype'");
        $associatemailid =  $ass[0]->de_mailid_vc;
        Mail::send('partials.feedbackcomplaints',['complaintval' => $complaint,'cardno'=>$cardno,'patientname'=>$patientname], function ($m) use ($adminmailid,$associatemailid)
            {
                $m->to($adminmailid,'complaint')->subject('Feedback Complaints!');
                $m->to($associatemailid,'complaint')->subject('Feedback Complaints!');
            });
        return redirect()->back()->with('message', 'Feedback Data sent successfully');
        }

    catch (\Exception $e)
    {
        // return $e;
        return redirect()->back()->withInput()->withErrors(array('message' => 'Error In Sending Feedback Data!! '));
    }
    }

    public function sendquery(QueryRequest $request)
    {
        $name = $request->input('name');
        $mobile = $request->input('mobile');
        $email = $request->input('email');
        $message = $request->input('message');
        // return $name;

        $query = new QueryData;
        $query->qt_name_vc = $name;
        $query->qt_contactno_vc = $mobile;
        $query->qt_emailid_vc = $email;
        $query->qt_message_vc = $message;
        $query->qt_senddate_vc = date('Y-m-d',time());
        $query->qt_status_in= '0';
        $query->save();

        $adminmailid  = 'ravik10892@gmail.com';

        Mail::send('partials.contactqueries', ['querymessage' => $message], function ($m) use ($adminmailid,$email,$name)
            {
                $m->from($email, $name);
                $m->to($adminmailid,'querymessage')->subject('Query Message!');
            });
        return redirect()->back()->with('message', 'Query sent successfully');
    }

    Public function sendqueryreply(Request $request)
    {
        $replymessage = $request->input('replymessage');
        $dt = date('Y-m-d',time());
        $id =$request->input('Id');
        $data = DB::select("Update queries_tables set qt_replymessage_vc='$replymessage', qt_status_in = '1' , qt_replydate_vc ='$dt' where id='$id'" );
            return Redirect::to('/mythriop/querypage')->with('message', 'Reply Sent To the Customer');
    }




    public function storecategory(CategoryRequest $request)
    {
        $category = $request->input('category');
        $status = $request->input('status');

        $names = DB::select("select count(*) as count from category_master where lower(ctm_categoryname_vc) = lower('$category')");
        $count = $names[0]->count;

        if($count =='1')
        {
            return redirect()->back()->withInput()->withErrors(array('message' => 'Category already exists '));
        }

        else
        {
            $caty = new CategoryMaster;
            $serviceid = DB::select('select category_id from vw_categoryid');
            $id = "Caty-".$serviceid[0]->category_id;
            // return $id;
            $caty->ctm_categorycode_vc = $id;
            $caty->ctm_categoryname_vc = $category;
            $caty->ctm_categorystatus_vc = $status;
            $caty->save();
            return redirect()->back()->with('message', 'Category Details are saved successfully');
        }

    }

    public function getcategory($code)
    {
        $query = DB::select("SELECT * from category_master where ctm_categorycode_vc = '$code'");
        return view('editcategory',['data'=>$query,'title'=>'Edit Category']);
    }


public function updatecategory(CategoryRequest $request, $code)
{
       try
        {
        $category = $request->input('category');
        $status = $request->input('status');

            $userupdatequery = "UPDATE category_master SET
                    ctm_categoryname_vc='$category',
                    ctm_categorystatus_vc = '$status'
                    WHERE ctm_categorycode_vc = '$code'";
            // return $userupdatequery;
            $result = DB::select($userupdatequery);
            return redirect()->to('/mythriop/categorymaster')->with('message', 'Category Details updated successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to update Category Details'));
        }
}

    public function deletecategory($code)
    {
        try
        {
            // $deleteuser = "DELETE from category_master where ctm_categorycode_vc = '$code'";

            $deleteuser = "UPDATE category_master SET
                    ctm_categorystatus_vc = 'Inactive'
                    WHERE ctm_categorycode_vc = '$code'";
            $result = DB::select($deleteuser);
            return redirect()->back()->with('message', 'Category Maked Inactive successfully');
        }
        catch(\Exception $e)
        {
            return Redirect::back()->withInput()->withErrors(array('message' => '!Failed to delete Category'));
        }
    }

    PUBLIC function checkmarketing(Request $request)
    {
        $mobileno = $request->input('mobileno');
        $emailid = $request->input('emailid');

        $mobquery = DB::select("select * from staff_master where sm_mobileno_vc='$mobileno'");
        $emailquery = DB::select("select * from staff_master where sm_emailid_vc='$emailid'");

        if(count($mobquery)!=0 && count($emailquery)!=0)
        {
        return Response::json([
            'message'=>" Both exists",
                ]);
        }

        if(count($mobquery)!=0 && count($emailquery)==0)
        {
        return Response::json([
            'message'=>" Mobileno already exists",
                ]);
        }

        if(count($mobquery)==0 && count($emailquery)!=0)
        {
        return Response::json([
            'message'=>" Email already exists",
                ]);
        }
    }








}
