<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionRequest;
use App\OpRegisterdata;
use App\OpTransaction;
use Redirect;
use DB;
use PDF;

class TransactionController extends Controller
{

    public function store(TransactionRequest $request)

    {
        // return 123;

        $cardno = strtoupper($request->input('cardno'));
        $department =$request->input('department');
        $consultingdoctor = $request->input('consultingdoctor');
        $date = $request->input('opdate');
        $time = $request->input('optime');
        $reason = $request->input('reason');
        $remarks = $request->input('remarks');
        $val = Session::get('id');

        try{
        $query = OpRegisterdata::where('op_cardno_vc',$cardno)->first();
            // return $query;
        $validupto = $query->op_validupto_dt;
        $today_time = strtotime($date);
        $expire_time = strtotime($validupto);
        if($expire_time < $today_time)
        {
            return redirect()->back()->withInput()->withErrors(array('message' => '!This Card is Expired.'));
        }
            $transaction = new OpTransaction;
            $transaction->td_cardno_vc =$cardno;
            // $transaction->td_hospitalopno_vc =$patientid;
            $transaction->td_consultingdoctor_vc =$consultingdoctor;
            $transaction->td_department_vc =$department;
            $transaction->td_opdate_dt  =$date;
            $transaction->td_optime_ti =$time;
            $transaction->td_reason_vc =$reason;
            $transaction->td_remarks_vc =$remarks;
            $transaction->td_agentname_vc = $val;
            $transaction->save();

            $query=DB::select("select * from patient_data where td_cardno_vc = '$cardno' and td_opdate_dt = '$date' and td_optime_ti = '$time' ");
        // return view('receipt',['data'=>$query]);
            $pdf = PDF::loadview('receipt',['data'=>$query])->setPaper('a4')->setOrientation('portrait')->setWarnings(false);
            return $pdf->stream();
            // return redirect()->back()->with('message', 'Op Card Transaction Added successfully ');
    }
    catch (\Exception $e)
    {
        // return $e;
            return redirect()->back()->withInput()->withErrors(array('message' => '!This Entry of transaction for patient already done.'));
    }
}


    Public function getcardinfo($cardno)
    {
        $data = OpTransaction::where('td_cardno_vc',$cardno)->get();
        return view('optransactions', ['title' => 'OP Card Transactions','data'=>$data]);
        // return Redirect::to('/mythriop/optransactions',['data'=>$data]);
    }



    Public function getbycardno($cardno)
    {
        $data = DB::select("select * from op_registerdata where lower(op_cardno_vc) = lower('$cardno')");
        return $data;

    }

    Public function getbyphoneno($phoneno)
    {
        $data = DB::select("select * from op_registerdata where op_contactno_vc = '$phoneno'");
        return $data;

    }

    Public function getbyname($name)
    {
        $data = DB::select("select * from op_registerdata where op_patientname_vc = '$name'");
        return $data;

    }

    public function edit($cardno, $opdate,$optime)
    {
        $query=DB::select("select * from transaction_data where td_cardno_vc = '$cardno' and td_opdate_dt = '$opdate' and td_optime_ti = '$optime'");
        return $query;
    }

    public function update(Request $request, $cardno, $opdate,$optime)
    {
        $department =$request->input('department');
        $patientid = $request->input('patientid');
        $consultingdoctor = $request->input('consultingdoctor');
        $reason = $request->input('reason');
        $remarks = $request->input('remarks');
            $query = DB::select("Update transaction_data set
            td_hospitalopno_vc = '$patientid',
            td_consultingdoctor_vc = '$consultingdoctor',
            td_reason_vc = '$reason',
            td_department_vc = '$department',
            td_remarks_vc = '$remarks' where td_cardno_vc = '$cardno' and td_opdate_dt = '$opdate' and td_optime_ti = '$optime'
                ");
            return redirect::to('/mythriop/optransactions')->with('message', 'Op Card Data Updated successfully ');
    }

    public function delete($cardno, $opdate,$optime)
    {
        $query = DB::select("Delete from transaction_data where td_cardno_vc = '$cardno' and td_opdate_dt = '$opdate' and td_optime_ti = '$optime'");
        return redirect::to('/mythriop/optransactions')->with('message', 'Op Card Data Delete successfully ');
    }

    public function getreceipt(Request $request)
    // public function getreceipt($cardno, $opdate,$optime)
    {
        $cardno = $request->input('cardno');
        $opdate = $request->input('opdate');
        $optime = $request->input('optime');
        if($cardno == "")
        {
        return redirect()->back()->withInput()->withErrors(array('message' => ' please select cardno from below table For Print.'));
        }
        else
        {
        $query=DB::select("select * from patient_data where td_cardno_vc = '$cardno' and td_opdate_dt = '$opdate' and td_optime_ti = '$optime' ");
        // return view('receipt',['data'=>$query]);
            $pdf = PDF::loadview('receipt',['data'=>$query])->setPaper('a4')->setOrientation('portrait')->setWarnings(false);
            return $pdf->stream();
        }

    }
}
