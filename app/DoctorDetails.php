<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorDetails extends Model
{
    protected $table = 'doctor_details';

}
