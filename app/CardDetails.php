<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardDetails extends Model
{
    protected $table = 'card_details';
}
