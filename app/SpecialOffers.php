<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialOffers extends Model
{
    protected $table = 'specialoffers';
}
