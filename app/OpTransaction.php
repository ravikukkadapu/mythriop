<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpTransaction extends Model
{
    protected $table = 'transaction_data';
}
