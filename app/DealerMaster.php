<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealerMaster extends Model
{
    protected $table = 'dealer_master';
}
