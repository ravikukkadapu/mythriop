<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociateTariffs extends Model
{
    protected $table = 'associate_tariffs';
}
