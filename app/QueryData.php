<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueryData extends Model
{
    protected $table = 'queries_tables';
}
