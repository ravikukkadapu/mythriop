<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutPatientFacilities extends Model
{
    protected $table = 'outpatient_facilities';
    public $timestamps = false;


}
