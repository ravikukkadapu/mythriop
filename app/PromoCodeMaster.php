<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCodeMaster extends Model
{
    protected $table = 'promocode_data';
    public $timestamps = false;
}
