<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackData extends Model
{
    protected $table = 'feedback_data';
}
