<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffMaster extends Model
{
    protected $table = 'staff_master';
}
