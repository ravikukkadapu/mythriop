<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTypeMaster extends Model
{
    protected $table = 'servicetype_master';
}
