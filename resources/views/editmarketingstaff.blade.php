@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
<!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Business Development Associate  <small>(Edit)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#"> Edit Business Development Associate</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>EDIT BUSINESS DEVELOPMENT ASSOCIATE
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
@foreach($data as $data)
                    <form name="marketingstaffform" id='marketingstaffform' method="post" action="{{ URL::to('/mythriop/updatestaff') . '/' . $data->sm_staffid_vc }}" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-body">
            <h3 class="form-section">Personal Details</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Name</label>
                            <div class="col-md-3">
                               <select class="form-control input-sm" name="sufix" id="sufix" >
                               <option value="{{$data->sm_sufix_vc}}">{{$data->sm_sufix_vc}}</option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Dr">Dr</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control input-sm" id='name' name='name' value="{{$data->sm_name_fl}}">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Mobile No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm"  name="mobileno" id="mobileno" value="{{$data->sm_mobileno_vc}}"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Alt Mobile No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="altmobileno" id="altmobileno" value="{{$data->sm_altmobileno_vc}}">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Email Id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id='emailid' name='emailid' value="{{$data->sm_emailid_vc}}"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="remarks" id="remarks" value="{{$data->sm_emailid_vc}}">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <h3 class="form-section">Address</h3>
                                                <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Country</label>
                                <div class="col-md-9">
                                    <select class="form-control input-sm" name="country" id="country">
                                        <option value="{{$data->sm_country_vc}}">{{$data->sm_country_vc}}</option>
                                        <option value='India' selected>India</option>
                                    </select>
                                </div>
                            </div>
                        </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> State</label>
                    <div class="col-md-9">
                        <select class="form-control input-sm" name="state" id="state"  >
                            <option value='{{$data->sm_state_vc}}'>{{$data->sm_state_vc}}</option>
                            <option value='Andhra Pradesh'> Andhra Pradesh</option>
                            <option value='Telangana'> Telangana</option>
                            <option value='Karnataka'> Karnataka</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> District</label>
                    <div class="col-md-9">
                        <select class="form-control input-sm select2me" id="district" name="district" >
                            <option value="{{$data->sm_district_vc}}"> {{$data->sm_district_vc}}</option>
                            @foreach($district as $val)
                            <option value='{{$val->district_name}}'>{{$val->district_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
                                                    <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> City/Town/Village</label>
                    <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="city" id="city" value="{{$data->sm_city_vc}}">
                    </div>
                </div>
            </div>
                                                    <!--/span-->
        </div>
                                                <!--/row-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> Area</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name="area" id="area" value="{{$data->sm_area_vc}}">
                    </div>
                </div>
            </div>
                                                    <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Street</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name="street" id="street" value="{{$data->sm_street_vc}}"/>
                    </div>
                </div>
            </div>
                                                    <!--/span-->
        </div>
                                                <!--/row-->

            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Houseno</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name="houseNo" id="houseNo value="{{$data->sm_houseno_vc}}"">
                    </div>
                </div>
            </div>
                                                    <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Pincode</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name="pincode" id="pincode"  value="{{$data->sm_pincode_vc}}"/>
                    </div>
                </div>
            </div>
                                                    <!--/span-->
        </div>
                                                <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Landmark</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="landMark" id="landMark value="{{$data->sm_landmark_vc}}"">
                        </div>
                    </div>
                </div>
                                                    <!--/span-->
            </div>
                                                <!--/row-->
            <h3 class="form-section"></h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><span class='red'>*</span> Photo ID</label>
                        <div class="col-md-2">
                            <input type="file" id="browse1" name="photoid" style="display: none" onChange="Handlechange1();"/>
                            <input type="button" value="Select" onclick="HandleBrowseClick1();" class="form-control input-sm"/>
                        </div>
                        <div class="col-md-7">
                            <input type="text" id="filename1" readonly="true" class="form-control input-sm" value='{{$data->sm_photoid_vc}}'/>
                        </div>
                    </div>
                </div>
                                                    <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><span class='red'>*</span> Address Proof</label>
                        <div class="col-md-2">
                            <input type="file" id="browse2" name="addressproof" style="display: none" onChange="Handlechange2();"/>
                                <input type="button" value="Select" onclick="HandleBrowseClick2();" class="form-control input-sm "/>
                            </div>
                            <div class="col-md-7">
                                <input type="text" id="filename2" readonly="true" class="form-control input-sm" value='{{$data->sm_addressproof_vc}}'/>
                            </div>
                        </div>
                    </div>
                                                    <!--/span-->
                </div>
                                                <!--/row-->
                                                @endforeach
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default" onclick='cancel()'>Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->

{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/marketingpeople_autosuggest.js"></script>
<script src="/mythriop/style/js/staffarea_autosuggest.js"></script>
<script src="/mythriop/style/js/staffcity_autosuggest.js"></script>
<script>

function HandleBrowseClick1()
{
    var fileinput = document.getElementById("browse1");
    fileinput.click();
}
function Handlechange1()
{
var fileinput = document.getElementById("browse1").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename1");
textinput.value = res;
}


function HandleBrowseClick2()
{
    var fileinput = document.getElementById("browse2");
    fileinput.click();
}
function Handlechange2()
{
var fileinput = document.getElementById("browse2").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename2");
textinput.value = res;
}










var i=0;
    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }


function cancel()
{

    document.marketingstaffform.action = '/mythriop/staffcancel';
    document.marketingstaffform.submit();
}


</script>

@stop
