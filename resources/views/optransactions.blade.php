@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Visit Information  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Visit Information</a>
                    </li>

                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Visit Information
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form class="form-horizontal" name="optransactionform" method="post" action="/mythriop/addtransaction" enctype="multipart/form-data">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Card No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id='cardno' name='cardno' onchange ='getdatabycardno()'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6" >
                        <div class="form-group" id='abc'>
                            <label class="control-label col-md-3">Name</label>

                            <div class="col-md-2" >
                                <select name="sufix" id="sufix" value="{{ Input::old('sufix') }}" class="form-control input-sm">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr">Dr</option>
                                    <option value="Master">Master</option>
                                </select>
                            </div>
                            <div class="col-md-7" >
                                <input type="text" class="form-control input-sm" id='x' name='x' autocomplete=off>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id='phoneno' name='phoneno' onblur ='getdatabyphoneno()'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Visit No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="patientid" id="patientid"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Date</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control input-sm"  name="opdate" id="opdate">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Time</label>
                            <div class="col-md-9">
                                <input type="time" class="form-control input-sm" name="optime" id="optime"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Department</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="department" id="department" onchange='getdoctor()'>
                                <option value='{{Input::old('department')}}'>{{Input::old('department')}}</option>
                    @foreach($department as $deptname)
                      <option value="{{$deptname->dm_deptname_vc}}">{{$deptname->dm_deptname_vc}}</option>
                      @endforeach</select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Consulting Doctor</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="consultingdoctor" id="consultingdoctor">
                                    <option value='{{Input::old('consultingdoctor')}}'>{{Input::old('consultingdoctor')}}</option>
                                        @foreach($doctor as $doc)
                                            <option value="{{$doc->dd_drname_vc}}">{{$doc->dd_drname_vc}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Service Type</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="servicetype" id="servicetype">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Op Fees</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="opFees" id="opFees"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Complaint/Reason</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="reason" id="reason">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">

                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                        <button type="button" class="btn purple-plum" onclick='print()'>Receipt</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Item(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Card No</th>
                    <th>Patient Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Visit no</th>
                    <th>Department Name</th>
                    <th>Consulting Doctor</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $value)
                        <tr onClick="editdetails('{{$value->td_cardno_vc}}','{{$value->td_opdate_dt}}','{{$value->td_optime_ti}}')" class="clickable-row">
                            <td>{{$value->td_cardno_vc}}</td>
                            <td>{{$value->op_patientname_vc}}</td>
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td><?php echo $dateformat ?></td>
                            <td>{{$value->td_optime_ti}}</td>
                            <td>{{$value->td_hospitalopno_vc}}</td>
                            <td>{{$value->td_department_vc}}</td>
                            <td>{{$value->td_consultingdoctor_vc}}</td>
                        </tr>
                        @endforeach
            </tbody>
        </table>
    </div>
</div>
                        </div>
                    </div>

<script>
var i=0;
function edittransaction()
    {
        i = 1;
        document.getElementById('Delete').disabled = false;
    }

    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }

function getdatabycardno()
{
    var form = document.optransactionform;
    var cardno = document.getElementById('cardno').value.toUpperCase();
    console.log(cardno);
    var url = "/mythriop/getbycardno/" + cardno;
    console.log(url);
    var sample='<label class="control-label col-md-3">Name</label><div class="col-md-2" ><select name="sufix" id="sufix" value="{{ Input::old("sufix") }}" class="form-control input-sm"><option value="Mr">Mr</option><option value="Mrs">Mrs</option><option value="Ms">Ms</option><option value="Dr">Dr</option><option value="Master">Master</option></select></div><div class="col-md-7" >';
            downloadUrl(url, function(data)
            {
                console.log(data.length)
        if(data.length == 0)
            {
            alert("Entered Card No is Not Registered, Please register first.")
            }
        else if(data.length ==1)
        {
            document.getElementById('x').style.display='none';
                console.log('123');

            sample += '<input type="text" class="form-control input-sm" name="name" id="name"  value='+data[0].op_patientname_vc+'>';
        }

        else if(data.length >1)
        {
                document.getElementById('x').style.display='none';
                console.log(data);
                // form.name.value = data[0].op_patientname_vc;
             sample += '<select class="form-control input-sm" name="name" id="name" ><option value="">Select</option><option value="{{Input::old("name")}}"> {{Input::old("name")}}</option>';

             $.each(data, function(index,data)
            {
                sample += '<option value="'+data.op_patientname_vc+'">'+data.op_patientname_vc+'</option>';
            });
            sample +='</select>'
        }
        sample +='</div>'
            form.phoneno.value = data[0].op_contactno_vc;
            form.servicetype.value = data[0].op_schmetype_vc;
            form.opFees.value = data[0].op_opfees_fl;
            form.sufix.value = data[0].op_nameprefix_vc;
            console.log(sample)
            $('#abc').html(sample);

        // document.getElementById("name").readOnly = true;
        document.getElementById("phoneno").readOnly = true;
        document.getElementById("patientid").readOnly = true;
        document.getElementById("servicetype").readOnly = true;
        document.getElementById("opFees").readOnly = true;


        });
}


function getdatabyphoneno()
{
    var form = document.optransactionform;
    var phoneno = document.getElementById('phoneno').value;
    var url = "/mythriop/getbyphoneno/" + phoneno;
    console.log('1');
    var sample='<select name="sufix" id="sufix" value="{{ Input::old("sufix") }}" style="width:60px; height:25px"><option value="Mr">Mr</option><option value="Mrs">Mrs</option><option value="Ms">Ms</option><option value="Dr">Dr</option><option value="Master">Master</option></select>&nbsp;';
            downloadUrl(url, function(data)
            {

        if(data.length == 0)
            {
            alert("Entered Phone No is Not Registered, Please register first.")
            }

        else if(data.length ==1)
        {
            document.getElementById('x').style.display='none';
                console.log(data);

            sample += '<input type="text" style="width:270px ; height:25px" name="name" id="name"  value='+data[0].op_patientname_vc+'>';
        }
        else if(data.length >1)
        {
                document.getElementById('x').style.display='none';
                console.log(data);
                // form.name.value = data[0].op_patientname_vc;
             sample += '<select style="width:270px ; height:25px" name="name" id="name" ><option value="">Select</option><option value="{{Input::old("name")}}"> {{Input::old("name")}}</option>';

             $.each(data, function(index,data)
            {
                sample += '<option value="'+data.op_patientname_vc+'">'+data.op_patientname_vc+'</option>';
            });
            sample +='</select>'
        }
            $('#abc').html(sample);
                console.log(data[0].op_schmetype_vc);
                // form.name.value = data[0].op_patientname_vc;
                form.cardno.value = data[0].op_cardno_vc;
                form.phoneno.value = data[0].op_contactno_vc;
                form.servicetype.value = data[0].op_schmetype_vc;
                form.opFees.value = data[0].op_opfees_fl;
                form.sufix.value = data[0].op_nameprefix_vc;

            });
}


function editdetails(cardno, opdate, optime)
    {
        var form = document.optransactionform;
        if(form.edit.disabled==false && i==1)
        {
            var url = '/mythriop/edittransactionop/' + cardno +'/'+ opdate +'/'+ optime;
            console.log(url);
            downloadUrl(url, function(data)
            {
                console.log(data);
                form.cardno.value = data[0].td_cardno_vc;
                form.patientid.value= data[0].td_hospitalopno_vc;
                form.consultingdoctor.value= data[0].td_consultingdoctor_vc;
                form.opdate.value= data[0].td_opdate_dt;
                form.optime.value= data[0].td_optime_ti;
                form.reason.value= data[0].td_reason_vc;
                form.department.value = data[0].td_department_vc;
getdatabycardno();
// getdoctor();
            });

        }
        document.getElementById("cardno").readOnly = true;
        document.getElementById("opdate").readOnly = true;
        document.getElementById("optime").readOnly = true;

    }

function updateoptransaction()
{
    var form = document.optransactionform;
    if(i==1)
    {
        var cardno = document.getElementById('cardno').value;
        var opdate = document.getElementById('opdate').value;
        var optime = document.getElementById('optime').value;
        form.action = '/mythriop/updatetransactionop/' + cardno +'/'+ opdate +'/'+ optime ;
    }
}

function deleteoptransaction()
{
    var form = document.optransactionform;
    var cardno = document.getElementById('cardno').value;
    var opdate = document.getElementById('opdate').value;
    var optime = document.getElementById('optime').value;
    var url = '/mythriop/deletetransactionop/' + cardno +'/'+ opdate +'/'+ optime ;
    console.log(url);
    ok.href = url;
}

    function cancel()
    {
    document.optransactionform.action = "{{ URL::to('/mythriop/optransactioncancel')}}";
    document.optransactionform.submit();
    }



function getdoctor()
{
// if(i==0)
// {
var department = document.getElementById('department').value;
var url = '/mythriop/getdoctorname/' + department;
downloadUrl(url,function(data)
{
var dept = data;
var districtData = '<option value="">Select Any</option>';

$.each(dept, function(index,data)
{
districtData += '<option value="'+data.dd_drname_vc+'">'+data.dd_drname_vc+'</option>';
});

$('#consultingdoctor').html(districtData);
});
// }
}


function print()
{
    console.log('1');
    var form = document.optransactionform;
    var cardno = document.getElementById('cardno').value.toUpperCase();
    var optime = document.getElementById('optime').value;
    var opdate = document.getElementById('opdate').value;
    form.action = "/mythriop/receipt";
    // + cardno + "/" + opdate + "/" + optime;
    form.submit();
}
function exit()
{
    document.optransactionform.action = "{{ URL::to('/mythriop/exitamsd') }}";
    document.optransactionform.submit();
}


</script>
{!! HTML::style('mythriop/style/css/global.css') !!}
<script src="/mythriop/style/js/typeahead.js"></script>
{{-- <script src="/mythriop/style/js/patientname_autosuggest.js"></script> --}}
<script src="/mythriop/style/js/patientmobile_autosuggest.js"></script>
@stop
