@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Doctor  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Doctor</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form class="form-horizontal" name="doctor_details_form" id="doctor_details_form" method="post" action="adddoctordetails" enctype="multipart/form-data">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Department</label>
                            <div class="col-md-9">
                                <input type='hidden' name='doctorid' id='doctorid'>
                                <select name="deptName" id="deptName" class="form-control input-sm" onchange='selectdeptnames()'>
                                    <option value="{{Input::old('deptName')}}">{{Input::old('deptName')}}</option>
                                    @foreach($department as $deptname)
                                        <option value="{{$deptname->dm_deptname_vc}}">{{$deptname->dm_deptname_vc}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type='hidden' name='dealerid' id='dealerid' value='{{$dealerid}}'>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Doctor Name</label>
                            <div class="col-md-2">
                                <select name="sufix" id="sufix"  class="form-control input-sm">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Ms">Ms</option>
                                    <option value="Dr" selected>Dr</option>
                                    <option value="Master">Master</option>
                                </select>
                            </div>
                            <div class="col-md-7">
                                <input type="text" name="doctorName" id="doctorName" class="form-control input-sm" value="{{Input::old('doctorName')}}"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">

                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Qualification</label>
                            <div class="col-md-9">
                            <select  name="designation" id="designation" class="form-control input-sm">
                    <option value="{{Input::old('designation')}}">{{Input::old('designation')}}</option>
                    @foreach($qualifiationdata as $val)
                        <option value="{{$val->qm_qualificaton_vc}}">{{$val->qm_qualificaton_vc}}</option>
                    @endforeach
                    </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Specialization</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="specialization" id="specialization"  value="{{Input::old('specialization')}}">
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Visit Type</label>
                            <div class="col-md-9">
                                <select name="visitType" id="visitType" class="form-control input-sm">
                        <option value="{{Input::old('visitType')}}">{{Input::old('visitType')}}</option>
                      <option value="Part-time">Part-time</option>
                      <option value="Fill-time">Full-time</option>
                      </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select name="status" id="status" class="form-control input-sm">
                                    <option value="{{Input::old('status')}}">{{Input::old('status')}}</option>
                                    <option value="Avaliable" selected>Avaliable</option>
                                    <option value="NotAvaliable">NotAvaliable</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


<div class="row">
           <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> Reg State</label>

                    <div class="col-md-9">
                        <select class="form-control input-sm" name="state" id="state" onchange = 'getDistrict()' >
                            <option value='{{ Input::old('state') }}'>{{ Input::old('state') }}</option>
                            <option value='Andhra Pradesh'> Andhra Pradesh</option>
                            <option value='Telangana'> Telangana</option>
                            <option value='Karnataka'> Karnataka</option>
                        </select>
                    </div>
                </div>
            </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Reg No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="regNo" id="regNo" value="{{Input::old('regNo')}}">
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="row">

                <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Reg Certificate</label>
                            <div class="col-md-2">
                                <input type="file" id="browse1" name="regcert" style="display: none" onChange="Handlechange1();"/>
                                <input type="button" value="Select" onclick="HandleBrowseClick1();" class="form-control input-sm"/>
                            </div>
                            <div class="col-md-7">
                                <input type="text" id="filename1" readonly="true" class="form-control input-sm"/>
                            </div>
                    </div>
                    </div>
                    <!--/span-->

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Credentials</label>
                            <div class="col-md-9">
                                <textarea class="form-control input-sm" name="credentials" id="credentials" value="{{Input::old('credentials')}}"></textarea>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Doctor(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Doctor Name</th>
                    <th>Department Name</th>
                    <th>Qualification</th>
                    <th>Reg No</th>
                    <th>Specialization</th>
                    <th>Type</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            @foreach($doctordetail as $doctordetails)
                <tr >
                    <td><a href='editdoctordetails/{{ $doctordetails->dd_drid_vc }}'>{{ $doctordetails->dd_drname_vc }}</a></td>
                    <td>{{ $doctordetails->dd_department_vc }}</td>
                    <td>{{ $doctordetails->dd_designation_vc }}</td>
                    <td>{{ $doctordetails->dd_regno_vc }}</td>
                    <td>{{ $doctordetails->dd_specialization_vc }}</td>
                    <td>{{ $doctordetails->dd_visittype_vc }}</td>
                    <td>{{ $doctordetails->dd_status_vc }}</td>
                  </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
                        </div>
                    </div>
{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/doctorname_autosuggest.js"></script>
<script>
function exit()
{
    document.doctor_details_form.action = "{{ URL::to('/mythriop/exitamsd') }}";
    document.doctor_details_form.submit();
}
var i=0;
    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }
function HandleBrowseClick1()
{
    var fileinput = document.getElementById("browse1");
    fileinput.click();
}
function Handlechange1()
{
var fileinput = document.getElementById("browse1").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename1");
textinput.value = res;
}

//Function to enable doctordetails
function editdoctorEnable()
{
  //alert('entered into editcapacityenable function');
  i = 1;
    document.getElementById("Delete").disabled = false;
        document.getElementById("editlabel").innerHTML = 'Select the Doctor Name to be edited or deleted respectively.'
}

// Function to get doctor-details values into form
function editdoctordetails(doctorid)
{
// alert('1')
    var form = document.doctor_details_form;
    if(form.Edit.disabled==false && i==1)
    {
      var url = '/mythriop/displaydoctordetails/' + doctorid
      console.log(url)
     downloadUrl(url, function(data)
      {
        // alert('start');
        console.log(data);
        form.sufix.value = data[0].dd_prefix_vc;
        form.doctorid.value = data[0].dd_drid_vc;
        form.doctorName.value = data[0].dd_drname_vc;
        form.deptName.value = data[0].dd_department_vc;
        form.regNo.value = data[0].dd_regno_vc;
        form.designation.value = data[0].dd_designation_vc;
        form.specialization.value = data[0].dd_specialization_vc;
        form.visitType.value = data[0].dd_visittype_vc;
        form.status.value = data[0].dd_status_vc;
        form.credentials.value = data[0].dd_credentials_vc;
        // alert('end');
        //form.fileimage.value = data[0].dd_certificate_vc
      });
      // alert('123');
    }
}

//Function to update bank-details
function doctordetailsUpdate()
{
  var form = document.doctor_details_form;
    if(i==1)
    {
        // console.log(form.action);
        var doctorid = document.getElementById('doctorid').value;
        form.action = "/mythriop/updatedoctordetails/" + doctorid;
        //console.log(form.action);
    }
}

function deletedata()
{
  var form = document.doctor_details_form;
        var doctorid = document.getElementById('doctorid').value;
    var url = "/mythriop/deletedoctordetails/" + doctorid;
    console.log(url);
    ok.href = url;
}
    function cancel()
    {
    document.doctor_details_form.action = "{{ URL::to('/mythriop/canceldoctor')}}";
    document.doctor_details_form.submit();
    }


function selectdeptnames()
{

    var deptName = document.getElementById('deptName').value;
    if(deptName != '')
    {

        document.getElementById('doctortable').style.display='none';
        getdept(deptName);
    }
    else
    {
        document.getElementById('doctortable').style.display='';
    }
}

function getdept(deptName)
{
    // alert(deptName);
var deptName = document.getElementById('deptName').value;
var url = '/mythriop/getdoctorsfordept/' + deptName;
downloadUrl(url,function(data)
{
    len = data.length;
var district = data;
var districtData = '<table style="width:900px;" id="doctortable" border="1"><tr style="height:25px"><th style="text-align:center;">Doctor Name</th><th style="text-align:center;" >Department Name</th><th style="text-align:center">Qualification</th><th style=>Reg No</th><th style="text-align:center;" >Specialization</th><th style="text-align:center;">Type</th><th style="text-align:center;">Status</th></tr>';
if(len<=0)
{
districtData += '<tr style="text-align:center"><td colspan="7">No Doctors Avaliable in That Department</td></tr>';
}
else
{
$.each(district, function(index,data)
{
districtData += '<tr style="text-align:center"><td><a href="#" onclick=editdoctordetails("'+data.dd_drid_vc+'")>'+data.dd_drname_vc+'</a></td><td>'+data.dd_department_vc+'</td><td>'+data.dd_designation_vc+'</td><td>'+data.dd_regno_vc+'</td><td>'+data.dd_specialization_vc+'</td><td>'+data.dd_visittype_vc+'</td><td>'+data.dd_status_vc+'</td></tr>';
});

districtData +='</table>';
}
$('#attach').html(districtData);
});

}

</script>

@stop
