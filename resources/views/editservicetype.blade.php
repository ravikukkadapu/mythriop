@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
<!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Service Type  <small>(Edit)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Service Type</a>

                    </li>

                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Edit Service Type Master
        </div>

                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
    </div>

    <div class="portlet-body form">
    <!-- BEGIN FORM-->
       @foreach($data as $data)
        <form name="servicetypeform" id='servicetypeform' method="post" action="{{ URL::to('/mythriop/updatetypedetails') . '/' . $data->stm_code_vc }}"  class="form-horizontal">

            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Service Type</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id='serviceType' name='serviceType' value='{{$data->stm_servicetype_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Cost</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="cost" id="cost" value='{{$data->stm_cost_fl}}'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Op Fees</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="opFees" id="opFees" value='{{$data->stm_opfees_fl}}' >
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">No of Persons covered</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="noofpersons" id="noofpersons" value='{{$data->stm_noofpersons_vc}}'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select  class="form-control input-sm" id='status' name='status'  >
                                    <option value='{{$data->stm_status_vc}}'>{{$data->stm_status_vc}}</option>
                                    <option value='Active'>Active</option>
                                    <option value='Inactive'>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default" onclick='cancel()'>Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif





<!-- Data Table -->

<script>

function cancel()
{

    document.servicetypeform.action = '/mythriop/typecancel';
    document.servicetypeform.submit();
}

</script>
@stop
