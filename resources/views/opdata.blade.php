@extends('layout/headfoot')
@section('content')

            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Reports <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Reports</a>

                    </li>

                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-search"></i>Search Form
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
    <form name="searchform" id='searchform' method="post" action="/mythriop/search" enctype="multipart/form-data" class="form-horizontal">

            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Search Type</label>
                            <div class="col-md-9">
                               <select class="form-control" name='searchtype' id='searchtype' onclick='enableit()'>
                            <option value='{{$searchtype1}}'>{{$searchtype1}}</option>
{{--                             <option value='Doctorwise'>Doctorwise</option>
                            <option value='Patientwise'>Patientwise</option>
                            <option value='Departmentwise'>Departmentwise</option>
                            <option value='Dealerwise'>Dealerwise</option> --}}
                            <option value='Marketingwise'>Marketingwise</option>
                            <option value='Dealerwise'>Dealerwise</option>
                            <option value='Cardexpiry'>Card Expiry Wise</option>
                            <option value='Feedback'>Feedback</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-9">
                               <select class="form-control" name="name" id="name">
{{-- <option value=''></option> --}}
<option value='{{$name1}}'>{{$name1}}</option>
</select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Service Type</label>
                            <div class="col-md-9">
                                <select class="form-control" name='servicetype' id='servicetype'>
                                <option value='{{$servicetype1}}'>{{$servicetype1}}</option>
                                <option value=''>All</option>

                                @foreach($type as $val)
                                <option value='{{$val->stm_servicetype_vc}}'>{{$val->stm_servicetype_vc}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">From Date</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" id='fromdate' name='fromdate'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">To Date</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" name="todate" id="todate">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Card Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name='status' id='status' disabled>
                                <option value='{{$status1}}'>{{$status1}}</option>
                                <option value='Sold'>Sold</option>
                                <option value='Not Sold'>Not Sold</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->



                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Duration</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name='duration' id='duration'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type='hidden' id='name1' name='name1' value='{{$name1}}'>
                            <input type='hidden' id='fromdate1' name='fromdate1' value='{{$fromdate1}}'>
                            <input type='hidden' id='todate1' name='todate1' value='{{$todate1}}'>
                            <input type='hidden' id='searchtype1' name='searchtype1' value='{{$searchtype1}}'>
                            <input type='hidden' id='servicetype1' name='servicetype1' value='{{$servicetype1}}'>
                            <input type='hidden' id='status1' name='status1' value='{{$status1}}'>
                            <input type='hidden' id='duration1' name='duration1' value='{{$duration1}}'>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                    <!--/span-->
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default" >Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>


@if ($errors->any())
    <ul class="alert alert-danger" style='width:500px;text-align:center; margin-left:245px'>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif



<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Report(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">











@if($searchtype1=='Doctorwise' )
{{-- for doctor wise --}}
    <center>
    @if($searchtype1 !='')
        <b> {{$searchtype1}} Consulting Report</b><br> &nbsp;&nbsp;&nbsp;
    @endif
    @if($name1 !='')
        <b> For Doctor :</b> {{$name1}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype1 !='')
        <b> For Card Type :</b> {{$servicetype1}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate1 !='' )
        <b>Date :</b> {{$fromdate1}}  <b> to </b> {{$todate1}}
    @endif
    </center>



                    <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center;">Op Fees</th>
                            <th style="text-align:center;">Department</th>
                            @if($name1 =='')
                            <th style="text-align:center;">DoctorName</th>
                            @endif
                        </tr>
                        </thead>
            <tbody>
                                                @if($data !=[])
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$value->td_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td><?php echo $dateformat ?></td>
                            {{-- <td>{{$value->td_opdate_dt}}</td> --}}
                            <td>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                            <td style='text-align:left'>{{$value->td_department_vc}}</td>
                            @if($name1 =='')
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            @endif
                        </tr>
                        @endforeach
                        @else
                        <tr style='text-align:center'><td colspan=7>No Data Avaliable</td></tr>
                        @endif
                        </tbody>
                                            </table>

           @elseif($searchtype1=='Cardexpiry')
    <center>
    @if($searchtype1 !='')
        <b> {{$searchtype1}}  Report</b> &nbsp;&nbsp;&nbsp;
    @endif

    @if($duration1 !='')
        <b>
Cards Expiry Between
         <?php
        $str = date('d-m-Y', strtotime('+1 years'));
        $getdate = date('d-m-Y',(strtotime ( '-'.$duration1.' day' , strtotime ( $str) ) ));
        print($getdate);?> &nbsp;and&nbsp;&nbsp; <?php print($str);        ?></b><br> &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype1 !='')
        <b> For Card Type :</b> {{$servicetype1}} &nbsp;&nbsp;&nbsp;
    @endif

    </center>
                 <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Valid Upto</th>
                            <th style="text-align:center;">Enrolled By</th>
                            <th style="text-align:center;">Mobile No</th>
                            <th style="text-align:center; ">Email</th>
                        </tr>
                        </thead>
            <tbody>
                        @if($cardexpirydata !=[])

                        @foreach($cardexpirydata as $val)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$val->op_cardno_vc}}</td>
                            <td style='text-align:left'>{{$val->op_patientname_vc}}</td>
                            <?php
                            $opdate = $val->op_validupto_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td width='100px'><?php echo $dateformat ?></td>

                             @if($name1 == '')
                            <td style='text-align:left'>{{$val->op_issuedby_vc}}</td>
                            @endif
                            <td>{{$val->op_contactno_vc}}</td>
                            <td style='text-align:left'>{{$val->op_mailid_vc}}</td>
                        </tr>
                        @endforeach

                        @else
                        <tr style="text-align:center;"><td colspan=6>No Cards are Expired.</td></tr>
                        @endif
                        </tbody>
                </table>


            @elseif($searchtype1=='OpCardwise')
    <center>
    @if($searchtype1 !='')
        <b> {{$searchtype1}} Consulting Report</b><br> &nbsp;&nbsp;&nbsp;
    @endif
    @if($name1 !='')
        <b> For OpCard No :</b> {{$name1}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype1 !='')
        <b> For Card Type :</b> {{$servicetype1}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate1 !='' )
        <b>Date :</b> {{$fromdate1}}  <b> to </b> {{$todate1}}
    @endif
    </center>
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center;">Department</th>
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                </thead>
            <tbody>
                        @if($data !=[])
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            {{-- <td>{{$value->td_opdate_dt}}</td> --}}
                                                        <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td><?php echo $dateformat ?></td>
                            <td>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            <td style='text-align:left'>{{$value->td_department_vc}}</td>
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr style='text-align:center'><td colspan=7>No Data Avaliable</td></tr>
                        @endif
                </tbody>
                                            </table>

            @elseif($searchtype1=='Patientwise')
    <center>
    @if($searchtype1 !='')
        <b> {{$searchtype1}} Consulting Report</b><br> &nbsp;&nbsp;&nbsp;
    @endif
    @if($name1 !='')
        <b> For Patient :</b> {{$name1}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype1 !='')
        <b> For Card Type :</b> {{$servicetype1}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate1 !='' )
        <b>Date :</b> {{$fromdate1}}  <b> to </b> {{$todate1}}
    @endif
    </center>
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center; ">Date</th>
                            @if($name1 == '')
                            <th style="text-align:center;">Marketing</th>
                            @endif
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center;">Department</th>
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                        </thead>
            <tbody>
                        @if($data !=[])

                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$value->td_cardno_vc}}</td>
                            {{-- <td>{{$value->td_opdate_dt}}</td> --}}
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td><?php echo $dateformat ?></td>
                            @if($name1 == '')
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            @endif
                            <td style='text-align:left'>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:left'>{{$value->td_department_vc}}</td>
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr><td colspan=7>No Data Avaliable</td></tr>
                        @endif
                        </tbody>
                </table>
            @elseif($searchtype1=='Departmentwise')
    <center>
    @if($searchtype1 !='')
        <b> {{$searchtype1}} Consulting Report</b><br> &nbsp;&nbsp;&nbsp;
    @endif
    @if($name1 !='')
        <b> For Department :</b> {{$name1}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype1 !='')
        <b> For Card Type :</b> {{$servicetype1}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate1 !='' )
        <b>Date :</b> {{$fromdate1}}  <b> to </b> {{$todate1}}
    @endif
    </center>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                        </thead>
            <tbody>
                        @if($data !=[])
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$value->td_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            {{-- <td>{{$value->td_opdate_dt}}</td> --}}
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td><?php echo $dateformat ?></td>
                            <td style='text-align:left'>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr style='text-align:center'><td colspan=6>No Data Avaliable</td></tr>
                        @endif

                    </tbody>
                                            </table>




                                            {{-- @endif --}}

@elseif($searchtype1=='Marketingwise')
    <center>
        <b> {{$searchtype1}} Consultancy Report</b><br>
    @if($name1 !='')
        <b> For Marketing Person :</b> {{$name1}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype1 !='')
        <b> For Card Type :</b> {{$servicetype1}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate1 !='' )
        <b>Date :</b> {{$fromdate1}}  <b> to </b> {{$todate1}}
    @endif
    </center>
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center; ">Name</th>
                            <th style="text-align:center; ">Issue Date</th>
                            <th style="text-align:center; ">Vaild Upto</th>
                            <th style="text-align:center;">Enrolled By</th>
                            <th style="text-align:center; ">Scheme Type</th>
                            <th style="text-align:center;">Op Fees</th>
                            @if($name1 == '')
                            <th style="text-align:center;">Marketing</th>
                            @endif

                        </tr>
                        </thead>
            <tbody>
                        @if($regdata !=[])
                        @foreach($regdata as $val)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$val->op_cardno_vc}}</td>
                            <td style='text-align:left'>{{$val->op_patientname_vc}}</td>
                            <?php
                            $opdate = $val->op_date_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td width='100px'><?php echo $dateformat ?></td>
                            <?php
                            $opdate1 = $val->op_validupto_dt;
                            $dateformat1 = date("d-m-Y", strtotime($opdate1));
                            ?>
                            <td width='100px'><?php echo $dateformat1 ?></td>
                            <td style='text-align:left'>{{$val->op_issuedby_vc}}</td>
                            <td style='text-align:left'>{{$val->op_schmetype_vc}}</td>
                            <td style='text-align:right'>{{$val->op_opfees_fl}}</td>
                            @if($name1 == '')
                            <td style='text-align:left'>{{$val->op_marketedby_vc}}</td>
                            @endif
                        </tr>
                        @endforeach
                        @else
                        <tr style='text-align:center'><td colspan=8>No Data Avaliable</td></tr>
                        @endif
                    </tbody>
                                            </table>
           @elseif($searchtype1=='Dealerwise')
    <center>
    @if($searchtype1 !='')
        <b> {{$searchtype1}} Consulting Report</b><br> &nbsp;&nbsp;&nbsp;
    @endif
@if($name1 !='')
        <b> For Dealer :</b> {{$name1}} &nbsp;&nbsp;&nbsp;
            <?php
            $no = DB::select("select count(*) from card_details where cd_status_vc = 'Not Sold' and cd_agentname_vc = '$name1' ");
            $yes = DB::select("select count(*) from card_details where cd_status_vc = 'Sold' and cd_agentname_vc = '$name1' ");
            $notsoldcards = $no[0]->count;
             $soldcards = $yes[0]->count;

             $totalamount = DB::select("select sum(op_amount_fl) as total from op_registerdata where op_issuedby_vc = '$name1'");

             $total = $totalamount[0]->total;?>
        Total Cards : <?php
    print_r($soldcards+$notsoldcards);
    ?> &nbsp;&nbsp;&nbsp;
    No of Cards Not Sold:
    <?php
    print_r($notsoldcards);
    ?> &nbsp;&nbsp;&nbsp;
    No of Cards Sold:
    <?php
    print_r($soldcards);
    ?> &nbsp;&nbsp;&nbsp;
    Total Amount Collected :
    <?php
    if($total =='')
    {
        print_r(0);
    }
    else
        print_r($total);
    ?>

    @endif
    @if($servicetype1 !='')
        <b> For Card Type :</b> {{$servicetype1}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate1 !='' )
        <b>Date :</b> {{$fromdate1}}  <b> to </b> {{$todate1}}
    @endif
    @if($status1 !='')
        <b>Status :</b> {{$status1}}
    @endif
    </center>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center; ">Issue Date</th>
                            @if($name1 == '')
                            <th style="text-align:center;">Enrolled By</th>
                            @endif
                            <th style="text-align:center; ">Scheme Type</th>
                            <th style="text-align:center">Selling Price</th>
                            <th style="text-align:center;">Status</th>
                            <th style="text-align:center;">Patient Name</th>

                        </tr>
                        </thead>
            <tbody>
                        @if($sampledealer !=[])
                        @foreach($sampledealer as $val)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$val->cd_cardno_vc}}</td>

                            <?php
                            $opdate = $val->cd_issuedate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td width='100px'><?php echo $dateformat ?></td>

                             @if($name1 == '')
                            <td style='text-align:left'>{{$val->cd_agentname_vc}}</td>
                            @endif
                            <td style='text-align:left'>{{$val->cd_servicetype_vc}}</td>
            <?php
            $s1 = $val->cd_agentname_vc;
            $s2 = $val->cd_servicetype_vc;
            $atdata = DB::select("select at_cost_fl from associate_tariffs where at_agentname_vc = '$s1' and at_servicetype_vc = '$s2' ");
            if($atdata ==[])
            {
                $dataval ='';
            }
            else{
            $dataval =  $atdata[0]->at_cost_fl;
            }
            ?>
                            <td><?php echo $dataval ;?></td>
                            <td style='text-align:left'>{{$val->cd_status_vc}}</td>
                            <td style='text-align:left'>{{$val->op_patientname_vc}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr style='text-align:center'><td colspan=6>No Data Avaliable</td></tr>
                        @endif
                </tbody>
                </table>
@elseif($searchtype1=='Feedback')
    <center>
        <b> {{$searchtype1}} Consultancy Report</b><br>
@if($name1 !='')
        <b> For Hospital / Clinic Name :</b> {{$name1}} &nbsp;&nbsp;&nbsp;
        @endif
        @if($fromdate1 !='' )
        <b>Date :</b> <?php  echo date("d-m-Y", strtotime($fromdate1)) ?> <b> to </b>  <?php  echo date("d-m-Y", strtotime($todate1)) ?>
    @endif
    </center>
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                        <th style="text-align:center;">Date</th>
                            <th style="text-align:center;">Hospital Name</th>
                            <th style="text-align:center; ">Category</th>
                            {{-- <th style="text-align:center; ">Address</th> --}}
                            <th style="text-align:center; ">Compliant Description</th>
                        </tr>
                        @if($feedata !=[])
                        @foreach($feedata as $val)
                    </thead>
            <tbody>
                        <tr style="text-align:center;">
                        <?php
                            $opdate1 = $val->fd_date_dt;
                            $dateformat1 = date("d-m-Y", strtotime($opdate1));
                            ?>
                            <td width='100px'><?php echo $dateformat1 ?></td>

                            <td style='text-align:left'>{{$val->fd_hospitalname_vc}}</td>
                            <td style='text-align:left'>{{$val->fd_agenttype_vc}}</td>
                            {{-- <td style='text-align:left'>{{$val->op_cardno_vc}}</td> --}}
                            <td style='text-align:left'>{{$val->fd_complaint_vc}}</td>

                        </tr>
                        @endforeach
                        @else
                        <tr style='text-align:center'><td colspan=4>No Data Avaliable</td></tr>
                        @endif
                    </tbody>
                                            </table>

            @else
               <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Department</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                        </thead>
            <tbody>
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$value->td_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td><?php echo $dateformat ?></td>
                            <td style='text-align:left'>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            <td style='text-align:left'>{{$value->td_department_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                        {{-- <tr style='text-align:center'><td colspan=7>No Information is Avaliable for your Search</td></tr> --}}
                </tbody>
                </table>



@endif










    </div>
</div>
                        </div>
                    </div>





















<script>
function showDate()
{
var now = new Date();
thour=now.getHours();
tmin=now.getMinutes();
tsec=now.getSeconds();
a=now.getMonth();
//alert(a);
if (tmin<=9) { tmin="0"+tmin; }
if (tsec<=9) { tsec="0"+tsec; }
if (thour<10) { thour="0"+thour; }
timetext = thour + ":" + tmin;

document.getElementById("todate").value = new Date().toISOString().substring(0, 10);
}

    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }

function enableit()
{
    var searchtype = document.getElementById('searchtype').value;
    if(searchtype=='Dealerwise')
    {
        document.getElementById("status").disabled = false;
    }
    var url = '/mythriop/getname/' + searchtype;
    downloadUrl(url,function(data)
    {
        var district = data;
        var districtData = '<option value="">Select any</option>';

        $.each(district, function(index,data)
        {
            if(searchtype == 'Doctorwise')
            {
districtData += '<option value="'+data.dd_drname_vc+'">'+data.dd_drname_vc+'</option>';
            }

            if(searchtype == 'Patientwise')
            {
districtData += '<option value="'+data.op_patientname_vc+'">'+data.op_patientname_vc+'</option>';
            }

            if(searchtype == 'Departmentwise')
            {
districtData += '<option value="'+data.dm_deptname_vc+'">'+data.dm_deptname_vc+'</option>';
            }

            if(searchtype == 'OpCardwise')
            {
districtData += '<option value="'+data.op_cardno_vc+'">'+data.op_cardno_vc+'</option>';
            }

            if(searchtype == 'Marketingwise')
            {
districtData += '<option value="'+data.sm_name_fl+'">'+data.sm_name_fl+'</option>';
            }

            if(searchtype == 'Dealerwise')
            {
districtData += '<option value="'+data.de_agentname_vc+'">'+data.de_agentname_vc+'</option>';
            }

            if(searchtype == 'Feedback')
            {
districtData += '<option value="'+data.de_agentname_vc+'">'+data.de_agentname_vc+'</option>';
            }

});

$('#name').html(districtData);
});
}


function getprint()
{
    document.searchform.action = "/mythriop/print";
    document.searchform.submit();
}

function cancel()
{
    document.searchform.action = "/mythriop/reportcancel";
    document.searchform.submit();
}


</script>

@stop
