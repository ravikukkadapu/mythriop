@extends('layout/homefoot')
@section('content')

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>SEARCH HOSPITALS
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form name="searchform" id="searchform" method="post" action='searchhosp' class="form-horizontal">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Category</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm"  name="category" id="category">
                                     <option value="{{$category}}"> {{$category}}</option>
                                     @foreach($categoriesdata as $categoriesdata)
                                     <option value='{{$categoriesdata->ctm_categoryname_vc}}'>{{$categoriesdata->ctm_categoryname_vc}}</option>
                                     @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Department</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="department" id="department">
                                     <option value="{{$department}}"> {{$department}}</option>
                                    @foreach($dept as $dept)
                                    <option value='{{$dept->dfm_deptname_vc}}'>{{$dept->dfm_deptname_vc}}</option>
                                    @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">State</label>
                            <div class="col-md-9">
                               <select class="form-control input-sm" name="state" id="state" onchange = 'getDistrict()' >
                                    <option value='{{$state}}'>{{$state}}</option>
                                <option value='Andhra Pradesh'> Andhra Pradesh</option>
                                <option value='Telangana'> Telangana</option>
                                <option value='Karnataka'> Karnataka</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->

                <div class="row">
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" id="district" name="district" >
                                    <option value="{{$dist}}"> {{$dist}}</option>
                                @foreach($district as $val)
                                <option value='{{$val->district_name}}'>{{$val->district_name}}</option>
                                @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">City/Town/Village</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id="city" size="30" />
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Area</label>
                            <div class="col-md-9">
                                <input class="form-control input-sm" name="area" id="area" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default" onclick='searchcancel()'>Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Result(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Hospital Name</th>
                    <th>Category</th>
                    <th>Department</th>
                    <th>Enrolled Date</th>
                    <th>Contact No 1</th>
                    <th>Email</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
           @if($data !=[])
@foreach($data as $data)
<tr style="text-align:left">
    <td>{{$data->de_agentname_vc}}</td>
    <td>{{$data->de_hospitalcategory_vc}}</td>
    <td>{{$data->dfm_deptname_vc}}</td>
    <?php
    $date = $data->de_enrolldate_dt;
    $dt = date("d-m-Y", strtotime($date));
    ?>
    <td><?php echo $dt;?></td>
    <td>{{$data->de_contactno_vc}}</td>
    <td>{{$data->de_mailid_vc}}</td>
    <td>{{$data->de_city_vc}}, {{$data->de_district_vc}}, {{$data->de_state_vc}}</td>
    {{-- <td>{{$data->de_opfacilities_vc}}</td> --}}
</tr>
@endforeach
@else
<tr style='text-align:center'>
    <td colspan='7'>No Hospitals Avaliable</td>
</tr>
@endif
            </tbody>
        </table>
    </div>
</div>
                        </div>
                    </div>
{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/citytown_autosuggest.js"></script>
<script>
function searchcancel()
{
    document.searchform.action = "{{ URL::to('/mythriop/searchcancel')}}";
    document.searchform.submit();
}
    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }

function getDistrict()
{
var state = document.getElementById('state').value;
var url = '/mythriop/district/' + state;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '<option value="">Select Any</option>';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.district_name+'">'+data.district_name+'</option>';
});

$('#district').html(districtData);
});

}

    </script>


@stop
