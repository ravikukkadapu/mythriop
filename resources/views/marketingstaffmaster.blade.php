@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
 <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Marketing Staff   <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Marketing Staff</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->

                    <form name="marketingstaffform" id='marketingstaffform' method="post" action="/mythriop/addstaff" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-body">
            <h3 class="form-section">Personal Details</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Name</label>
                            <div class="col-md-3">
                               <select class="form-control input-sm" name="sufix" id="sufix" >
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Dr">Dr</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control input-sm" id='name' name='name'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Mobile No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm"  name="mobileno" id="mobileno"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Alt Mobile No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="altmobileno" id="altmobileno">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Email Id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id='emailid' name='emailid'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="remarks" id="remarks">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <h3 class="form-section">Address</h3>
                                                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Country</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="country" id="country">
                                    <option value="{{ Input::old('country') }}">{{ Input::old('country') }}</option>
                                    <option value='India' selected>India</option>
                                </select>
                            </div>
                        </div>
                    </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> State</label>
                    <div class="col-md-9">
                        <select class="form-control input-sm" name="state" id="state" onchange = 'getDistrict()' >
                            <option value='{{ Input::old('state') }}'>{{ Input::old('state') }}</option>
                            <option value='Andhra Pradesh'> Andhra Pradesh</option>
                            <option value='Telangana'> Telangana</option>
                            <option value='Karnataka'> Karnataka</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> District</label>
                    <div class="col-md-9">
                        <select class="form-control input-sm select2me" id="district" name="district" onchange = 'getdatabytype()'>
                            <option value="{{Input::old('district')}}"> {{Input::old('district')}}</option>
                            @foreach($district as $val)
                            <option value='{{$val->district_name}}'>{{$val->district_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
                                                    <!--/span-->
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span>City/Town/Village</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control input-sm" name="city" id="city">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><span class='red'>*</span>Area</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control input-sm" name="area" id="area">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Street</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control input-sm" name="street" id="street"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->

                                                 <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Houseno</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control input-sm" name="houseNo" id="houseNo">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Pincode</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control input-sm" name="pincode" id="pincode" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Landmark</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control input-sm" name="landMark" id="landMark">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                                <h3 class="form-section"></h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><span class='red'>*</span>Photo ID</label>
                                                            <div class="col-md-2">
                                                                <input type="file" id="browse1" name="photoid" style="display: none" onChange="Handlechange1();"/>
                                                                <input type="button" value="Select" onclick="HandleBrowseClick1();" class="form-control input-sm"/>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <input type="text" id="filename1" readonly="true" class="form-control input-sm"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><span class='red'>*</span>Address Proof</label>
                                                            <div class="col-md-2">
                                                                <input type="file" id="browse2" name="addressproof" style="display: none" onChange="Handlechange2();"/>
                                <input type="button" value="Select" onclick="HandleBrowseClick2();" class="form-control input-sm "/>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <input type="text" id="filename2" readonly="true" class="form-control input-sm"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Marketing Staff(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Mobile No</th>
                    <th>Email Id</th>
                    <th>State</th>
                    <th>District</th>
                    <th>City</th>
                    <th>Area</th>
                </tr>
            </thead>
            <tbody>
           @foreach($details as $value)
                        <tr >
                            <td ><a href='displaystaffdata/{{$value->sm_staffid_vc}}'>{{$value->sm_sufix_vc}} {{$value->sm_name_fl}}</a></td>
                            <td>{{$value->sm_mobileno_vc}}</td>
                            <td >{{$value->sm_emailid_vc}}</td>
                            <td >{{$value->sm_state_vc}}</td>
                            <td >{{$value->sm_district_vc}}</td>
                            <td >{{$value->sm_city_vc}}</td>
                            <td >{{$value->sm_area_vc}}
                            </td>

                        </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>
                        </div>
                    </div>
{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/marketingpeople_autosuggest.js"></script>
<script src="/mythriop/style/js/staffarea_autosuggest.js"></script>
<script src="/mythriop/style/js/staffcity_autosuggest.js"></script>
<script>

function HandleBrowseClick1()
{
    var fileinput = document.getElementById("browse1");
    fileinput.click();
}
function Handlechange1()
{
var fileinput = document.getElementById("browse1").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename1");
textinput.value = res;
}


function HandleBrowseClick2()
{
    var fileinput = document.getElementById("browse2");
    fileinput.click();
}
function Handlechange2()
{
var fileinput = document.getElementById("browse2").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename2");
textinput.value = res;
}



function cancel()
{

    document.marketingstaffform.action = '/mythriop/staffcancel';
    document.marketingstaffform.submit();
}

function exit()
{
    document.marketingstaffform.action = "{{ URL::to('/mythriop/exituser') }}";
    document.marketingstaffform.submit();
}


function getDistrict()
{
var state = document.getElementById('state').value;
var url = '/mythriop/district/' + state;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '<option value="">Select</option>';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.district_name+'">'+data.district_name+'</option>';
});

$('#district').html(districtData);
getstaffbyarea()
});

}
function getstaffbyarea()
{
var state = document.getElementById('state').value;
var district = document.getElementById('district').value;
var city = document.getElementById('city').value;
var area = document.getElementById('area').value;
var query = "SELECT * FROM staff_master";
var query_length = query.length;
    if(state !='')
    {
        if(query.length > query_length)
        {
            query += " AND lower(sm_state_vc) = lower('"+state+"')";
        }
         else
        {
            query += " WHERE lower(sm_state_vc) = lower('"+state+"')";
        }
    }


    if(district !='')
    {
        if(query.length > query_length)
        {
            query += " AND lower(sm_district_vc) = lower('"+district+"')";
        }
         else
        {
            query += " WHERE lower(sm_district_vc) = lower('"+district+"')";
        }
    }

    if(city !='')
    {
        if(query.length > query_length)
        {
            query += " AND lower(sm_city_vc) = lower('"+city+"')";
        }
         else
        {
            query += " WHERE lower(sm_city_vc) = lower('"+city+"')";
        }
    }

    if(area !='')
    {
        if(query.length > query_length)
        {
            query += " AND lower(sm_area_vc) = lower('"+area+"')";
        }
         else
        {
            query += " WHERE lower(sm_area_vc) = lower('"+area+"')";
        }
    }

    query +=" order by sm_state_vc,sm_district_vc,sm_city_vc,sm_area_vc";
    console.log(query)
    var url = '/mythriop/getstaffbyarea/' + query;
    console.log(url)
    document.getElementById('tablew').style.display= 'none';
    downloadUrl(url,function(data)
    {
        console.log(data);
        var district = data;
        var districtData = '<table style="width:900px;" align="center" border="1" id="tablew" ><tr style="height:25px;color:#800000"><th style="text-align:center;">Name</th><th style="text-align:center;">Mobile No</th><th style="text-align:center; ">Email Id</th><th style="text-align:center; ">State</th><th style="text-align:center; ">District</th><th style="text-align:center; ">City</th><th style="text-align:center; ">Area</th></tr>';
        if(data.length <=0)
        {
            districtData += '<tr style="text-align:center" ><td colspan=7>No Marketing Persons avaliable</td</tr>';
        }
        else{
        $.each(district, function(index,data)
        {
            districtData += '<tr style="text-align:left"  class="clickable-row" onclick=editdetails("'+data.sm_staffid_vc+'")><td >'+data.sm_sufix_vc+' '+data.sm_name_fl+'</td><td>'+data.sm_mobileno_vc+'</td><td>'+data.sm_emailid_vc+'</td><td>'+data.sm_state_vc+'</td><td>'+data.sm_district_vc+'</td><td>'+data.sm_city_vc+'</td><td>'+data.sm_area_vc+'</td</tr>';

        });
    }
        districtData +='</table>'

        $('#abc').html(districtData);

    });

    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }
}
</script>

@stop
