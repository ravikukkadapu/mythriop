@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
 <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Business Development Assocciate  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Tariffs</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Business Development Assocciate </a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">

    <div class="portlet-title">

        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>

                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->

        <form action="addassociatetariff" class="form-horizontal" name="associatetariffform" id="associatetariffform" method='post' enctype="multipart/form-data" >
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Agent Org Name</label>
                            <div class="col-md-9">
                                <input type="text" id='agentname' name='agentname' class="form-control input-sm"  autocomplete=off  value="{{Input::old('agentname')}}" onblur='getagentid()'>
                            </div>
                        </div>
                    </div>
                                                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Agent Id</label>
                            <div class="col-md-9">
                                <input type="text" name="agentid" id="agentid" class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                                                    <!--/span-->
                </div>

<div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3"><span class='red'>*</span> Service Type</label>
                            <div class="col-md-9">
                        <select class="form-control input-sm " name="servicetype" id="servicetype" onchange='getamount()'>
                            {{-- <option value='' >Select</option> --}}
                        <option value='{{Input::old('servicetype')}}' >{{Input::old('servicetype')}}</option>
                            @foreach($servicetypes as $val)
                                <option value='{{$val->stm_servicetype_vc}}' >{{$val->stm_servicetype_vc}}</option>
                            @endforeach
                        </select></div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Amount</label>
                        <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name='amount' id='amount' readonly></div>
                    </div>
                </div>


            </div>
        <!--/row-->
        <div class="row">


            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Discount(%)</label>
                    <div class="col-md-9">
                    <input type="text" class="form-control input-sm" name="discount" id="discount" autocomplete=off maxlength='3' onchange='getdiscount()'></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3"><span class='red'>*</span> Cost</label>
                    <div class="col-md-9">
                    <input type="text" class="form-control input-sm" name="cost" id="cost"  value="{{Input::old('cost')}}" autocomplete=off readonly></div>
                </div>
            </div>


        </div>
                                                <!--/row-->
                                                <!--/row-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Reg charge</label>
                    <div class="col-md-9">
                    <input type="text" class="form-control input-sm" name="regcharges" id="regcharges"  value="0" autocomplete=off ></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Remarks</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-sm"  id='remarks' name='remarks' autocomplete=off >
                    </div>
                </div>
            </div>
        </div>
        </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>

                </div>
            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif


                       <!-- data table start -->

<div class="row">
                <div class="col-lg-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box green-haze">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-list"></i>List Of Items(s)
                            </div>
                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>
                                     Agent name
                                </th>
                                <th>
                                     Agent Id
                                </th>
                                <th>
                                     Service Type
                                </th>
                                <th>
                                     Service Amount
                                </th>
                                <th>
                                     Discount(%)
                                </th>
                                <th>
                                     Cost
                                </th>
                                <th>
                                     Op Fees
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $val)
                    <tr onclick="editdetails('{{$val->at_agentid_vc}}','{{$val->at_servicetype_vc}}')">
                        <td><a href='editassociatetariff/{{$val->at_agentid_vc}}/{{$val->at_servicetype_vc}}'>{{$val->at_agentname_vc}}</td>
                        <td>{{$val->at_agentid_vc}}</td>
                        <td>{{$val->at_servicetype_vc}}</td>
                        <td>{{$val->at_serviceamount_fl}}</td>
                        <td>{{$val->at_discount_fl}}
                        <td>{{$val->at_cost_fl}}</td>
                        <td>{{$val->at_regcharges_fl}}</td>
                    </tr>
                    @endforeach
                        </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                       <!-- data table end -->


{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script src="/mythriop/style/js/dealername_busi_autosuggest.js"></script>

<script>

var i = 0;

function editEnable()
{
    // alert('entered into editenable function');
    i = 1;
    document.getElementById("Delete").disabled = false;
        document.getElementById("editlabel").innerHTML = 'Select the Agent Id to be edited or  deleted respectively.'
}

//Function to download data
function downloadUrl(url, callback)
    {
        jQuery.getJSON(url, function(data)
        {
            callback(data);
        });
    }

function getagentid()
{
    var form = document.associatetariffform;
    var agentname = document.getElementById('agentname').value;
    var url = '/mythriop/getagentid/' + agentname;

    // console.log(url);
        downloadUrl(url, function(data)
        {
            console.log(data[0].de_agentid_vc)
            form.agentid.value = data[0].de_agentid_vc;
        });
// searchdet()
}
function getamount()
{
    var servicetype = document.getElementById("servicetype").value;
    var assname = document.getElementById("agentname").value;
    var form = document.associatetariffform;
    var url = '/mythriop/serviceopamount/' + servicetype +'/'+ assname;

        downloadUrl(url, function(data)
            {
                console.log(data[0].stm_cost_fl)
                form.amount.value= data[0].stm_cost_fl;
            });
}
function getagentname()
{
    var form = document.associatetariffform;
    var agentid = document.getElementById('agentid').value;
    var url = '/mythriop/getagentname/' + agentid;

    console.log(url);
        downloadUrl(url, function(data)
        {
            form.agentname.value = data[0].de_agentname_vc;
        });
}


// Function to get values into form
function editdata(index)
{

    var form = document.associatetariffform;
    if(i==1)
    {
        var url = '/mythriop/editassociatetariff/' + tarifarray[index].at_agentid_vc + '/' + tarifarray[index].at_servicetype_vc;
        downloadUrl(url, function(data)
        {
            form.agentname.value = data[0].at_agentname_vc;
            form.agentid.value = data[0].at_agentid_vc;
            form.servicetype.value = data[0].at_servicetype_vc;
            form.cost.value = data[0].at_cost_fl;
            form.regcharges.value = data[0].at_regcharges_fl;
            form.remarks.value = data[0].at_remarks_vc;
            form.discount.value = data[0].at_discount_fl;
            getamount()
        });
    }
}

function editdetails(agentid,type)
{

    var form = document.associatetariffform;
    if(i==1)
    {
        var url = '/mythriop/editassociatetariff/' + agentid + '/' + type;
        downloadUrl(url, function(data)
        {
            form.agentname.value = data[0].at_agentname_vc;
            form.agentid.value = data[0].at_agentid_vc;
            form.servicetype.value = data[0].at_servicetype_vc;
            form.cost.value = data[0].at_cost_fl;
            form.regcharges.value = data[0].at_regcharges_fl;
            form.remarks.value = data[0].at_remarks_vc;
            form.discount.value = data[0].at_discount_fl;
            // form.remarks.value = data[0].at_remarks_vc;
            getamount()
        });
    }
}
// Function to update details
function activateUpdate()
{
    //alert("entered into activateUpadate function");
    var form = document.associatetariffform;
    if(i==1)
    {
        console.log(form.action);
        var id = document.getElementById('agentid').value;
        var type = document.getElementById('servicetype').value;
        form.action = "/mythriop/updateassociatetariff/" + id + '/' + type;
        //console.log(form.action);
    }
}

//Function to delete user-details
function deletedetails()
{
    //alert("entered into deletedetails function");
    var form = document.associatetariffform;
        var id = document.getElementById('agentid').value;
        var type = document.getElementById('servicetype').value;
    var url = "/mythriop/deleteassociatetariff/" + id + '/' + type;
    console.log(url);
    ok.href = url;
}

function cancel()
{

    document.associatetariffform.action = '/mythriop/cancelassociatetariff';
    document.associatetariffform.submit();
}
val=[]
function searchdet()
{
    var agentname = document.getElementById('agentname').value;
    var url = '/mythriop/getdataforoneagent/' + agentname;
    document.getElementById('alldatatab').style.display = 'none';
    document.getElementById('editlabel').style.display = 'none';
downloadUrl(url,function(data)
{
    console.log(data)
    len = data.length;
var district = data;
 districtData = '<center><label id="editlabel1">To Edit (or) Delete Associate Tariff Details Click on Edit Button.</label></center><table style="width:750px;" align="center" border="1" id="tablew"><tr style="height:25px;color:#800000"><th style="text-align:center;">Agent Name</th><th style="text-align:center;">Agent Id</th><th style="text-align:center;">Service Type</th><th style="text-align:center;">Service Amount</th><th style="text-align:center;">Discount(%)</th><th style="text-align:center; ">Cost</th><th style="text-align:center;">Op Fees</th></tr>';

if(len<=0)
{
districtData += '<tr style="text-align:center"><td colspan="7">No Tariffs Avaliable for that Agent</td></tr>';
}
else
{
    j=0;
    tarifarray=[];
    tarifarray=data;
$.each(district, function(index,data)
{

    val.push(data);
districtData += '<tr  class="clickable-row" onclick= editdata("'+j+'")><td style="text-align:left">'+data.at_agentname_vc+'</td><td style="text-align:left">'+data.at_agentid_vc+'</td><td style="text-align:left">'+data.at_servicetype_vc+'</td><td style="text-align:right">'+data.at_serviceamount_fl+'</td><td style="text-align:right">'+data.at_discount_fl+'</td><td style="text-align:right">'+data.at_cost_fl+'</td><td style="text-align:right">'+data.at_regcharges_fl+'</td></tr>'
j++;
});

districtData +='</table>';
}

$('#attach').html(districtData);

});

}


function exitdetails()
{
    document.associatetariffform.action = "{{ URL::to('/mythriop/exitassociatetariff') }}";
    document.associatetariffform.submit();
}
function getdiscount()
{
    var form = document.associatetariffform;
    var dis = document.getElementById('discount').value;
    var amount = document.getElementById('amount').value;
    if(dis == "" || isNaN(dis))
    {
        discount = 0;
    }
    else
    {
        discount = parseInt(dis)

    }
    if(amount == "" || isNaN(amount))
    {
        amt = 0;
    }
    else
    {
        amt = parseInt(amount)
    }
    tot =(amt - ((amt*discount)/100));
    document.getElementById('cost').value=tot;
}
</script>


@stop
