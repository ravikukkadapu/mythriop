@extends('layout/headfoot')
@section('content')

    <div id="content">
        <div class="container">
            <div class="row blog-page">
                @include('partials/loginmenu')

                <!-- Start -posts -->
                <div class="col-md-8 col-xs-6 col-sm-6 blog-box" style="padding-right: 2em; height:535px">
            @if(Session::has('message'))
                <div class="alert alert-success" style='width:500px; margin-left:245px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif

                    <div class="col-md-6">
                <div class="row" style='margin-top:10px;'>
                    <font style="font-size: medium; margin-left:245px"><b>Change Password</b></font>
                    <form name="changepassword" method="post" action="/mythriop/savenewpassword">
                        <fieldset class='fs' align='center' style='margin-top:5px; margin-left:235px; width:340px; height:auto;'>

                            <table  border="0" style='margin-top:10px'>
                                <tr>
                                <input type='hidden' id='username' name='username' value='{{$username}}'>
                                    <td ><label><font color='#800000'>Current Password</font></label></td>
                                    <td width="20">:</td>
                                    <td width="180"><input type="text" style="width:170px ; height:25px" name="currentpass" id="currentpass" size="30" value="{{Input::old('currentpass')}}" autocomplete='off'></td>
                                </tr>

                                <tr>
                                    <td ><label><font color='#800000'>New Password</font></label></td>
                                    <td width="20">:</td>
                                    <td width="180"><input type="password" style="width:170px ; height:25px" name="newpass" id="newpass" size="30" value="{{Input::old('newpass')}}" autocomplete='off'></td>
                                </tr>

                                <tr>
                                    <td ><label><font color='#800000'>Confirm Password</font></label></td>
                                    <td width="20">:</td>
                                    <td width="180"><input type="password" style="width:170px ; height:25px" name="confirmpass" id="confirmpass" size="30" value="{{Input::old('confirmpass')}}" autocomplete='off'></td>
                                </tr>


                            </table>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if ($errors->any())
                        <ul class="alert alert-danger" style='width:285px;text-align:center; '>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                        <table align="center" style="height:45px;">
                            <tr>

                                <td width="70px"><input type='submit' value='Submit' style="width:65px" ></td>
                                <td width="70px"><input type='button' value='Cancel' style="width:65px" onclick='exit()'></td>
                            </tr>
                        </table>
                        </fieldset>
                    </form>
                </div>

                    </div>
                </div>

                @include('partials/ads')

            </div>
        </div>
    </div>
<script>
    function exit()
    {
        document.changepassword.action = "{{ URL::to('/mythriop/loginexit') }}";
        document.changepassword.submit();
    }


</script>
@stop
