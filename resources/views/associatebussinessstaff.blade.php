@extends('layout/headfoot')
@section('content')
<style>
.data
{
    font-size:15px;
    color:black;
    background:#EADCAE;
    box-shadow: 2px 3px 5px #888888;
    width:80px;
    height:26px;
    padding-top:3px;
    border-radius: 3px;

}
</style>
    <div id="content">
        <div class="container">
            <div class="row blog-page">
                @include('partials/businessmenu')
              <div class="col-md-10 col-xs-10 col-sm-12 blog-boxx" style="padding-right:1em; height:545px; overflow-y:scroll;">
              {{-- <div class="col-md-10 col-xs-10 col-sm-12 blog-boxx" style="padding-right:1em; height:545px; overflow:auto; margin-left:120px; margin-top:10px"> --}}

            @if(Session::has('message'))
                <div class="alert alert-success" style='width:500px; margin-left:245px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <font style="font-size: medium;"><b><center>Marketing Staff Master</center></b></font>
                <fieldset class='fs' style='margin-top:0px;margin-left:0px;width:960px;height:auto'>
                    <form name="marketingstaffform" id='marketingstaffform' method="post" action="/mythriop/addstaff" enctype="multipart/form-data">

                        <table  border="0" style='margin-top:5px; margin-left:5px;width:930px'>
                            <tr>
                            <input type='hidden' name='staffcode' id='staffcode'>
                                <td width="90"><label><font color='#800000'>Name</font></label></td>
                                <td width="5"></td>
                                <td width="270">
                                <select name="sufix" id="sufix" value="{{ Input::old('sufix') }}" style="width:50px; height:25px">
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Dr">Dr</option>
                </select>&nbsp;<input type="text" style="width:200px ; height:25px" id='name' name='name' autocomplete=off  value="{{Input::old('name')}}" ></td>

                                <td width="5"></td>
                                <td width="90"><label><font color='#800000'>Mobile No</font></label></td>
                                <td width="5"></td>
                                <td width="100"><input type="text" style="width:170px ; height:25px" name="mobileno" id="mobileno"  value="{{Input::old('mobileno')}}" autocomplete=off maxlength='11'></td>
                                <td width="5"></td>
                                <td width="80"><label><font color='#0000B'>Alt Mobile No</font></label></td>
                                <td width="5"></td>
                                <td width="100"><input type="text" style="width:170px ; height:25px" name="altmobileno" id="altmobileno"  value="{{Input::old('altmobileno')}}" autocomplete=off maxlength='11'></td>
                            </tr>
                            <tr>
                                <td width="90"><label><font color='#800000'>Email Id</font></label></td>
                                <td width="5"></td>
                                <td><input type="text" style="width:254px ; height:25px" id='emailid' name='emailid' autocomplete=off  value="{{Input::old('emailid')}}" ></td>
                                <td></td>
                                <td width="90"><label><font color='#00008B'>Remarks</font></label></td>
                                <td width="5"></td>
                                <td colspan=5><input type="text" style="width:490px ; height:25px" id='remarks' name='remarks' autocomplete=off  value="{{Input::old('remarks')}}" ></td>
                            </tr>
                        </table>
            <font style="font-size: medium;"><b>Address</b></font>
                <fieldset class='fs' style='margin-top:-15px;margin-left:65px;width:860px;height:auto;margin-bottom:10px'>
                        <table>
                        <tr>
                                <td width="45"><label><font color='#800000'>Country</font></label></td>
                                <td width="5"></td>
                                <td width="125">
                                    <select style="width:200px; height:25px" name="country" id="country">
                                        <option value="{{ Input::old('country') }}">{{ Input::old('country') }}</option>
                                        <option value='India' selected>India</option>
                                    </select>
                                </td>
                                <td width="15"></td>
                                <td width="70"><label><font color='#800000'>State</font></label></td>
                                <td width="6"></td>
                                <td width="180">

                            <select style="width:200px; height:25px" name="state" id="state" onchange = 'getDistrict()' >
                                    <option value='{{ Input::old('state') }}'>{{ Input::old('state') }}</option>
                                <option value='Andhra Pradesh'> Andhra Pradesh</option>
                                <option value='Telangana' > Telangana</option>
                                <option value='Karnataka'> Karnataka</option>

                            </select>

                                </td>
                                <td width="10"></td>
                                <td width="63"><label><font color='#800000'>District</font></label></td>
                                <td width="2"></td>
                                <td width="175">
                                    <select style="width:200px; height:25px" id="district" name="district" >

                                    <option value="{{Input::old('district')}}"> {{Input::old('district')}}</option>
                                @foreach($district as $val)
                                <option value='{{$val->district_name}}'>{{$val->district_name}}</option>
                                @endforeach
                                    </select>
                                </td>
                                <TD>&nbsp;</TD>
                            </tr>

                            <tr>
                                <td width="90"><label><font color='#800000'>City/Town/Village</font></label></td>
                                <td width="2"></td>
                                <td width="205"><input type="text" style="width:200px" name="city" id="city" size="30" value="{{ Input::old('city') }}" autocomplete='off'></td>
                                <td width="15"></td>
                                <td width="45"><label><font color='#800000'>Area</font></label></td>
                                <td width="7"></td>
                                <td width="120"><input type="text" style="width:200px;" name="area" id="area" size="30" value="{{ Input::old('area') }}"  autocomplete='off'></td>
                                <td width="20"></td>
                                <td width="70"><label><font color='#00008B'>Street</font></label></td>
                                <td width="7"></td>
                                <td width="180"><input type="text" style="width:200px" name="street" id="street" size="30" value="{{ Input::old('street') }}" autocomplete='off'></td>
                                <td width="10"></td>

                            </tr>

                            <tr>
                                <td width="70"><label><font color='#00008B'>Houseno</font></label></td>
                                <td width="2"></td>
                                <td width="115"><input type="text" style="width:200px" name="houseNo" id="houseNo" size="30" value="{{ Input::old('houseNo') }}" autocomplete='off'></td>
                                <td width="15"></td>
                                <td width="45"><label><font color='#00008B'>Pincode</font></label></td>
                                <td width="6"></td>
                                <td width="120"><input type="text" style="width:200px;" name="pincode" id="pincode" maxlength="6" value="{{ Input::old('pincode') }}"  autocomplete='off'></td>
                                <td width="6"></td>
                                <td width="60"><label><font color='#00008B'>Landmark </font></label></td>
                                <td width="6"></td>
                                <td width="175"><input type="text" style="width:200px" name="landMark" id="landMark" size="30" value="{{ Input::old('landMark') }}" autocomplete='off'></td>
                                <td>&nbsp;</td>
                        </tr>
                    </table>

                </fieldset>
                <table border="0" style=' margin-left:-22px;width:930px'>
                <tr>
               <td width="80" style='text-align:right'><label><font color='#800000'>Photo ID</font></label></td>
                                <td></td>
                                <td ><input type="file" id="browse1" name="photoid" style="display: none" onChange="Handlechange1();"/>&nbsp;
                                <input type="button" value="Select" onclick="HandleBrowseClick1();" style='width:70px;height:26px'/>&nbsp;
                                <input type="text" id="filename1" readonly="true" style='width:250px'/></td>
<td width="160" style='text-align:right'><label><font color='#800000'>Address Proof</font></label></td>
                                <td width='10'></td>
                                <td ><input type="file" id="browse2" name="addressproof" style="display: none" onChange="Handlechange2();"/>
                                <input type="button" value="Select" onclick="HandleBrowseClick2();" style='width:70px;height:26px'/>&nbsp;
                                <input type="text" id="filename2" readonly="true" style='width:250px'/></td>
                            </tr>

                        </table>
                </table>
                    </fieldset>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
                         <table align="center" style="height:45px;">
                            <tr>
                                <td width="70px"><input type='submit' value='Save' style="width:65px" onclick='updatedata()'></td>
                                <td width="70px"><input type='button' value='Edit' style="width:65px" onclick='editdata()'></td>
                                <td width="70px"><input type="button" value="DELETE" id="Delete" style="width:65px" data-toggle="modal" data-target="#myModal" disabled></td>
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-sm">

                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                            <p>Are you sure to delete?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#" class="btn btn-default"  name="delete" onclick="deletedata()" id="ok">Ok</a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <td width="70px"><input type='button' value='Cancel' style="width:65px" onclick='cancel()'></td>
                                <td width="70px"><input type='button' value='Exit' style="width:65px" onclick='exit()'></td>

                            </tr>
                        </table>
                                            @if ($errors->any())
                        <ul class="alert alert-danger" style='width:400px;text-align:center; margin-left:285px'>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <center>
                    <label id='editlabel'>
                    To Edit (or) Delete Marketing Staff Details Click on Edit Button.</label>
                    </center>
                    <table style='width:900px;'align='center' border="1" id='tablew' >
                        <tr style='height:25px;color:#800000'>
                            <th style="text-align:center;">Name</th>
                            <th style="text-align:center;">Mobile No</th>
                            <th style="text-align:center; ">Email Id</th>
                            <th style="text-align:center; ">Address</th>
                        </tr>
                    @foreach($details as $value)
                        <tr style='text-align:center'  class="clickable-row" onclick="editdetails('{{$value->sm_staffid_vc}}')">
                            <td style='text-align:left'>{{$value->sm_sufix_vc}} {{$value->sm_name_fl}}</td>
                            <td>{{$value->sm_mobileno_vc}}</td>
                            <td style='text-align:left'>{{$value->sm_emailid_vc}}</td>
                            <td style='text-align:left;width:400px'>{{$value->sm_houseno_vc}},{{$value->sm_street_vc}},{{$value->sm_area_vc}},{{$value->sm_city_vc}}{{$value->sm_district_vc}},{{$value->sm_state_vc}},{{$value->sm_pincode_vc}}</td>
                        </tr>
                    @endforeach
                    </table>
                                            </form>
    </div>

                {{-- @include('partials/ads') --}}

            </div>
        </div>
    </div>

<script>
function HandleBrowseClick1()
{
    var fileinput = document.getElementById("browse1");
    fileinput.click();
}
function Handlechange1()
{
var fileinput = document.getElementById("browse1").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename1");
textinput.value = res;
}


function HandleBrowseClick2()
{
    var fileinput = document.getElementById("browse2");
    fileinput.click();
}
function Handlechange2()
{
var fileinput = document.getElementById("browse2").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename2");
textinput.value = res;
}

var i=0;
    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }

    function editdata()
    {
        i=1;
    document.getElementById("Delete").disabled = false;
    document.getElementById("editlabel").innerHTML = 'Select the Name to be edited or  deleted respectively.'
    }

    function editdetails(staffid)
    {

    var form = document.marketingstaffform;
    var url = '/mythriop/displaystaffdata/' + staffid ;
    console.log(url);
        downloadUrl(url, function(data)
            {
                console.log(data);
                form.staffcode.value= data[0].sm_staffid_vc;
                form.sufix.value= data[0].sm_sufix_vc;
                form.name.value=data[0].sm_name_fl;
                form.mobileno.value= data[0].sm_mobileno_vc;
                form.emailid.value = data[0].sm_emailid_vc;
                form.altmobileno.value = data[0].sm_altmobileno_vc;
                form.filename1.value = data[0].sm_photoid_vc;
                form.filename2.value = data[0].sm_addressproof_vc;
                form.country.value= data[0].sm_country_vc;
                form.state.value= data[0].sm_state_vc;
                form.district.value=data[0].sm_district_vc;
                form.city.value= data[0].sm_city_vc;
                form.area.value = data[0].sm_area_vc;
                form.street.value = data[0].sm_street_vc;
                form.houseNo.value= data[0].sm_houseno_vc;
                form.pincode.value = data[0].sm_pincode_vc;
                form.landMark.value = data[0].sm_landmark_vc;
                form.remarks.value = data[0].sm_remarks_vc;
            });
    }





function updatedata()
{
    var form = document.marketingstaffform;
    if(i==1)
    {
        var staffcode = document.getElementById('staffcode').value;
        form.action = "/mythriop/updatestaff/" + staffcode;
    }
}

function deletedata()
{
    var form = document.marketingstaffform;
    var staffcode = document.getElementById('staffcode').value;
    var url = "/mythriop/deletestaff/" + staffcode;
    console.log(url);
    ok.href = url;
}

function cancel()
{

    document.marketingstaffform.action = '/mythriop/staffcancel2';
    document.marketingstaffform.submit();
}

function exit()
{
    document.marketingstaffform.action = "{{ URL::to('/mythriop/exitamsdbus') }}";
    document.marketingstaffform.submit();
}


function getDistrict()
{
var state = document.getElementById('state').value;
var url = '/mythriop/district/' + state;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.district_name+'">'+data.district_name+'</option>';
});

$('#district').html(districtData);
});

}
</script>
{!! HTML::style('mythriop/style/css/global.css') !!}
<script src="/mythriop/style/js/typeahead.js"></script>
<script src="/mythriop/style/js/marketingpeople_autosuggest.js"></script>
<script src="/mythriop/style/js/staffarea_autosuggest.js"></script>
<script src="/mythriop/style/js/staffcity_autosuggest.js"></script>
@stop
