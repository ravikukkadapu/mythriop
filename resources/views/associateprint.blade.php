<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<style>
hr {
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: solid;
    border-width: 2px;
    border-color:black;

}
</style>

<body>
<br>
<Center>
@if($reportname=='Dealerwise' and $name !='' )
<font style='font-size:40px'><B>{{$name}}</B><br></font>
@else
<font style='font-size:40px'><B>Mythri Hospitals</B><br></font>
@endif
{{-- (A DIVISION OF SURYAPRAKASH HOSPITALS
& DIAGNOSTICS PVT LTD.)<br> --}}
{{--  E-mail:info@mythrihosiptals
 Phone no:040-66558877 --}}
</Center>

    <table style='width:100%;'  align='center'>
        <tr style='height:25px'>
            <td><hr></td>
        </tr>
    </table>

@if($reportname=='Cardexpiry')
    <center>
    @if($reportname !='')
        <b> {{$reportname}} Consulting Report</b> &nbsp;&nbsp;&nbsp;
    @endif


    @if($duration !='')
        <b>
Cards Expiry Between
         <?php
        $str = date('d-m-Y', strtotime('+1 years'));
        $getdate = date('d-m-Y',(strtotime ( '-'.$duration.' day' , strtotime ( $str) ) ));
        print($getdate);?> &nbsp;and&nbsp;&nbsp; <?php print($str);        ?></b><br> &nbsp;&nbsp;&nbsp;
    @endif




    @if($servicetype !='')
        <b> For Card Type :</b> {{$servicetype}} &nbsp;&nbsp;&nbsp;
    @endif

    </center>
                <table style='width:100%;'  align='center' frame='border' rules='all'>
                        <tr >
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center;width:80px ">Valid Upto</th>
                            <th style="text-align:center;">Enrolled By</th>
                            <th style="text-align:center;width:80px">Mobile No</th>
                            <th style="text-align:center; ">Email</th>
                        </tr>
                        @if($cardexpirydata !=[])
                        @foreach($cardexpirydata as $val)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$val->op_cardno_vc}}</td>
                            <td style='text-align:left'>{{$val->op_patientname_vc}}</td>
                            <?php
                            $opdate = $val->op_validupto_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td width='100px'><?php echo $dateformat ?></td>
                            <td style='text-align:left'>{{$val->op_issuedby_vc}}</td>
                            <td>{{$val->op_contactno_vc}}</td>
                            <td style='text-align:left'>{{$val->op_mailid_vc}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr style="text-align:center;"><td colspan=6>No Cards are Expired.</td></tr>
                        @endif
                </table>

@elseif($reportname=='Doctorwise' )

    <center>
        <b> {{$reportname}} Consultancy Report</b><br>
    @if($name !='')
        <b> For Doctor :</b> {{$name}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype !='')
        <b> For Card Type :</b> {{$servicetype}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate !='' )
        <b>Date :</b> {{$fromdate}}  <b> to </b> {{$todate}}
    @endif
    </center>

                    <table style='width:100%;' align='center' frame='border' rules='all'>
                        <tr >
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center;">Op Fees</th>
                            <th style="text-align:center;">Department</th>
                            @if($name =='')
                            <th style="text-align:center;">DoctorName</th>
                            @endif
                        </tr>
@if($data !=[])
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$value->td_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            {{-- <td width='100px'>{{$value->td_opdate_dt}}</td> --}}
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td width='100px'><?php echo $dateformat ?></td>
                            <td style='text-align:left'>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                            <td style='text-align:left'>{{$value->td_department_vc}}</td>
                            @if($name =='')
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            @endif
                        </tr>
                        @endforeach
                        @else
                        <tr style="text-align:center;"><td colspan=7>No Information for this search.</td></tr>
                        @endif
                                            </table>

            @elseif($reportname=='OpCardwise')
    <center>
        <b> {{$reportname}} Consultancy Report</b><br>
    @if($name !='')
        <b> For OpCard No :</b> {{$name}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype !='')
        <b> For Card Type :</b> {{$servicetype}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate !='' )
        <b>Date :</b> {{$fromdate}}  <b> to </b> {{$todate}}
    @endif
    </center>
                    <table style='width:100%;'  align='center' frame='border' rules='all'>
                        <tr>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center;">Department</th>
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                        @if($data !=[])
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            {{-- <td width='100px'>{{$value->td_opdate_dt}}</td> --}}
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td width='100px'><?php echo $dateformat ?></td>
                            <td style='text-align:left'>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            <td style='text-align:left'>{{$value->td_department_vc}}</td>
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr style="text-align:center;"><td colspan=7>No Information for this search.</td></tr>
                        @endif
                                            </table>


            @elseif($reportname=='Patientwise')
    <center>
        <b> {{$reportname}} Consultancy Report</b><br>
    @if($name !='')
        <b> For Patient :</b> {{$name}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype !='')
        <b> For Card Type :</b> {{$servicetype}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate !='' )
        <b>Date :</b> {{$fromdate}}  <b> to </b> {{$todate}}
    @endif
    </center>
                    <table style='width:100%;'  align='center' frame='border' rules='all'>
                        <tr  >
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center; ">Date</th>
                            @if($name == '')
                            <th style="text-align:center;">Patient Name</th>
                            @endif
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center;">Department</th>
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style="text-align:left">{{$value->td_cardno_vc}}</td>
                            {{-- <td width='100px'>{{$value->td_opdate_dt}}</td> --}}
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td width='100px'><?php echo $dateformat ?></td>
                            @if($name == '')
                            <td style="text-align:left">{{$value->op_patientname_vc}}</td>
                            @endif
                            <td style="text-align:left">{{$value->td_hospitalopno_vc}}</td>
                            <td style="text-align:left">{{$value->td_department_vc}}</td>
                            <td style="text-align:left">{{$value->td_consultingdoctor_vc}}</td>
                            <td style="text-align:left">{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                </table>
            @elseif($reportname=='Departmentwise')
    <center>
        <b> {{$reportname}} Consultancy Report</b><br>
    @if($name !='')
        <b> For Department :</b> {{$name}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype !='')
        <b> For Card Type :</b> {{$servicetype}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate !='' )
        <b>Date :</b> {{$fromdate}}  <b> to </b> {{$todate}}
    @endif
    </center>
                <table style='width:100%;'  align='center' frame='border' rules='all'>
                        <tr >
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            @if($name =='')
                            <th style="text-align:center;">Department</th>
                            @endif
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$value->td_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            {{-- <td width='100px'>{{$value->td_opdate_dt}}</td> --}}
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td width='100px'><?php echo $dateformat ?></td>
                            <td width='100px'>{{$value->td_hospitalopno_vc}}</td>
                            @if($name =='')
                            <td  style='text-align:left'>{{$value->td_department_vc}}</th>
                            @endif
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                                            </table>




@elseif($reportname=='Marketingwise')
    <center>
        <b> {{$reportname}} Consultancy Report</b><br>
    @if($name !='')
        <b> For Marketing Person :</b> {{$name}} &nbsp;&nbsp;&nbsp;
    @endif
    @if($servicetype !='')
        <b> For Card Type :</b> {{$servicetype}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate !='' )
        <b>Date :</b> {{$fromdate}}  <b> to </b> {{$todate}}
    @endif
    </center>
                    <table style='width:100%;'  align='center' frame='border' rules='all'>
                        <tr  >
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center; ">Name</th>
                            <th style="text-align:center; ">Issue Date</th>
                            <th style="text-align:center; ">Vaild Upto</th>
                            <th style="text-align:center;">Enrolled By</th>
                            <th style="text-align:center; ">Scheme Type</th>
                            {{-- <th style="text-align:center;">Op Fees</th> --}}
                            @if($name == '')
                            <th style="text-align:center;">Marketing</th>
                            @endif

                        </tr>
                        @foreach($regdata as $val)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$val->op_cardno_vc}}</td>
                            <td style='text-align:left'>{{$val->op_patientname_vc}}</td>
                            <?php
                            $opdate = $val->op_date_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td width='100px'><?php echo $dateformat ?></td>
                            <?php
                            $opdate1 = $val->op_validupto_dt;
                            $dateformat1 = date("d-m-Y", strtotime($opdate1));
                            ?>
                            <td width='100px'><?php echo $dateformat1 ?></td>
                            <td style='text-align:left'>{{$val->op_issuedby_vc}}</td>
                            <td style='text-align:left'>{{$val->op_schmetype_vc}}</td>
                            {{-- <td>{{$val->op_opfees_fl}}</td> --}}
                            @if($name == '')
                            <td style='text-align:left'>{{$val->op_marketedby_vc}}</td>
                            @endif
                        </tr>
                        @endforeach
                                            </table>
           @elseif($reportname=='Dealerwise')
    <center>
    @if($reportname !='')
        <b> {{$reportname}} Consulting Report</b><br> &nbsp;&nbsp;&nbsp;
    @endif
    @if($name !='')
        <b> For Dealer :</b> {{$name}} &nbsp;&nbsp;&nbsp;
            <?php
            $no = DB::select("select count(*) from card_details where cd_status_vc = 'Not Sold' and cd_agentname_vc = '$name' ");
            $yes = DB::select("select count(*) from card_details where cd_status_vc = 'Sold' and cd_agentname_vc = '$name' ");
            $notsoldcards = $no[0]->count;
             $soldcards = $yes[0]->count;

             $totalamount = DB::select("select sum(op_amount_fl) as total from op_registerdata where op_issuedby_vc = '$name'");

             $total = $totalamount[0]->total;

            ?>
    @endif
    @if($servicetype !='')
        <b> For Card Type :</b> {{$servicetype}} &nbsp;&nbsp;&nbsp;
    @endif
        @if($fromdate !='' )
        <b>Date :</b> {{$fromdate}}  <b> to </b> {{$todate}}
    @endif
    @if($status !='')
        <b>Status :</b> {{$status}}
    @endif
    <br>

    </center>
                    <table style='width:100%;'  align='center' frame='border' rules='all'>
                        <tr >
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Issue Date</th>
                            @if($name == '')
                            <th style="text-align:center;">Enrolled By</th>
                            @endif
                            <th style="text-align:center; ">Scheme Type</th>
                            <th style="text-align:center; ">Selling Price</th>
                            <th style="text-align:center;">Status</th>

                        </tr>
                        @foreach($sampledealer as $val)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$val->cd_cardno_vc}}</td>
                            <td style='text-align:left'>{{$val->op_patientname_vc}}</td>
                            <?php
                            $opdate = $val->cd_issuedate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td width='100px'><?php echo $dateformat ?></td>

                             @if($name == '')
                            <td style='text-align:left'>{{$val->cd_agentname_vc}}</td>
                            @endif
                            <td style='text-align:left'>{{$val->cd_servicetype_vc}}</td>
            <?php
            $s1 = $val->cd_agentname_vc;
            $s2 = $val->cd_servicetype_vc;
            $atdata = DB::select("select at_cost_fl from associate_tariffs where at_agentname_vc = '$s1' and at_servicetype_vc = '$s2' ");
            if($atdata ==[])
            {
                $dataval ='';
            }
            else{
            $dataval =  $atdata[0]->at_cost_fl;
            }
            ?>
                            <td><?php echo $dataval ;?></td>
                            <td style='text-align:left'>{{$val->cd_status_vc}}</td>


                        </tr>
                        @endforeach
                </table>
            @else
            <center> Total Patients Data </center>
                <table style='width:100%;'  align='center' frame='border' rules='all'>
                        <tr >
                            <th style="text-align:center;">Card No</th>
                            <th style="text-align:center;">Patient Name</th>
                            <th style="text-align:center; ">Date</th>
                            <th style="text-align:center;">Op no</th>
                            <th style="text-align:center; ">Consulting Doctor</th>
                            <th style="text-align:center;">Department</th>
                            <th style="text-align:center;">Op Fees</th>

                        </tr>
                        @foreach($data as $value)
                        <tr style="text-align:center;">
                            <td style='text-align:left'>{{$value->td_cardno_vc}}</td>
                            <td style='text-align:left'>{{$value->op_patientname_vc}}</td>
                            {{-- <td width='100px'>{{$value->td_opdate_dt}}</td> --}}
                            <?php
                            $opdate = $value->td_opdate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td width='100px'><?php echo $dateformat ?></td>
                            <td style='text-align:left'>{{$value->td_hospitalopno_vc}}</td>
                            <td style='text-align:left'>{{$value->td_consultingdoctor_vc}}</td>
                            <td style='text-align:left'>{{$value->td_department_vc}}</td>
                            <td style='text-align:right'>{{$value->op_opfees_fl}}</td>
                        </tr>
                        @endforeach
                </table>
                                            @endif


                                            <br>
<table align='right'>
<tr>
@if($reportname=='Doctorwise' ||$reportname=='Patientwise' || $reportname=='Departmentwise' || $reportname=='OpCardwise' || $reportname =='')
<td style='text-align:right;'>Total No of Patients : {{$count}}</td>
@endif
@if($reportname=='Marketingwise')
<td style='text-align:right;'>Total No of Patients : {{$opdatacount}}</td>
@endif
@if($reportname=='Dealerwise')
<td style='text-align:right;'>
        Total Cards : {{$dealcount}}  <br>
    No of Cards Not Sold:
    <?php
    print_r($notsoldcards);
    ?><br>
    No of Cards Sold:
    <?php
    print_r($soldcards);
    ?><br>
    Total Amount Collected :
    <?php
    if($total =='')
    {
        print_r(0);
    }
    else
        print_r($total);
    ?>
    </td>
@endif
</tr>
</table>
</body>
</html>
