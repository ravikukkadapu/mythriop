@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
           <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            QUALIFICATION  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">QUALIFICATION</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->

<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form class="form-horizontal" name="qualificationform" id="qualificationform" method="post" action="/mythriop/addqualification">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Qualification</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="qualification" id="qualification">
                            </div>
                        </div>
                    </div>
                    <!--/span-->


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select  class="form-control input-sm" name="status" id="status">
                                        <option value='{{Input::old('status')}}'>{{Input::old('status')}}</option>
                                        <option value='Active'>Active</option>
                                        <option value='Inactive'>Inactive</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Qualification(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Qualification Code</th>
                    <th>Qualification Name</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
             @foreach($data as $val)
                <tr >
                    <td><a href='editquaflica/{{$val->qm_qualificatoncode_vc}}'>{{$val->qm_qualificatoncode_vc}}</td>
                    <td style='text-align:left'>{{$val->qm_qualificaton_vc}}</td>
                    <td style='text-align:left'>{{$val->qm_status_vc}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
                        </div>
                    </div>



@stop
