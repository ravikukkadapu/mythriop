@extends('layout/headfoot')
@section('content')
<style>
.data
{
    font-size:15px;
    color:black;
    background:#EADCAE;
    box-shadow: 2px 3px 5px #888888;
    width:80px;
    height:26px;
    padding-top:3px;
    border-radius: 3px;

}
</style>
    <div id="content">
        <div class="container">
            <div class="row blog-page">
                @include('partials/homemenu')
              <div class="col-md-10 col-xs-10 col-sm-12 blog-boxx" style="padding-right:1em; height:545px; overflow:auto;">
              {{-- <div class="col-md-10 col-xs-10 col-sm-12 blog-boxx" style="padding-right:1em; height:545px; overflow:auto; margin-left:120px; margin-top:10px"> --}}

            @if(Session::has('message'))
                <div class="alert alert-success" style='width:500px; margin-left:245px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <font style="font-size: medium;"><b><center>Patient Op Card Registration</center></b></font>
                <fieldset class='fs' style='margin-top:0px;margin-left:2px;width:960px;height:auto'>
                    <form name="opregistrationform" method="post" action="/mythriop/addopdata" enctype="multipart/form-data" onsubmit ='return carddataprep()'>

                        <table  border="0" style='margin-top:5px; margin-left:5px'>
                            <tr style='display:none' id='headdetails' name='headdetails'>
                                <td width="70"><label><font color='#00008B'>Head Card No</font></label></td>
                                <td width="5"></td>
                                <td width="110"><input type="text" style="width:110px ; height:25px" id='headcardno' name='headcardno' autocomplete=off  value='{{Input::old('headcardno')}}' onchange='getheadname()'></td>

                                <td width="5"></td>
                                <td width="75"><label><font color='#00008B'>Head Name</font></label></td>
                                <td width="0"></td>
                                <td width="100" colspan=5>
<input type="text" style="width:300px ; height:25px" id='headname' name='headname' autocomplete=off  value='{{Input::old('headname')}}' onchange='getheadcardno()'>

                                </td>
                            </tr>

                            <tr>
                                <td width="70"><label><font color='#800000'>Card No</font></label></td>
                                <td width="5"></td>
                                <td width="110"><input type="text" style="width:110px ; height:25px" id='cardno' name='cardno' autocomplete=off  onchange='getallpatientdata()' value='{{Input::old('cardno')}}'></td>

                                <td width="5"></td>
                                <td width="75"><label><font color='#800000'>Service Type</font></label></td>
                                <td width="0"></td>
                                <td width="100">

                                <select style="width:160px ; height:25px" name="servicetype" id="servicetype" onchange='getamount()'>
                                {{-- <option value=''>Select</option> --}}
                                <option value='{{Input::old('servicetype')}}' >{{Input::old('servicetype')}}</option>
                                    @foreach($servicetypes as $val)
                                        <option value='{{$val->stm_servicetype_vc}}' >{{$val->stm_servicetype_vc}}</option>
                                    @endforeach
                                </select>
                                </td>
                                <td width="5"></td>
                                <td width="80"><label><font color='#00008B'>Op Fees</font></label></td>
                                <td width="5"></td>
                                <td width="125" colspan=5>

                                <input type="text" style="width:130px ; height:25px" name="opfees" id="opfees"  value="{{Input::old('opfees')}}" autocomplete=off readonly>


                                </td>

                                <td width='5'></td>
                            <td rowspan="5" style='width:125px; text-align:center;'><img style='height:100px; width:125px; border:1px solid black' id='Preview1' name='Preview1'>
                            <label for="photoid" class='data' align='center'>SELECT</label><input style="display: none;" type="file" id="photoid" name='photoid' value='2' onchange="change1()"></td>
                            </tr>

                            <tr>


                                <td width="60"><label><font color='#00008B'>Amount</font></label></td>
                                <td width="5"></td>
                                <td width="110"><input type="text" style="width:110px ; height:25px" id='amount' name='amount' value= "{{Input::old('amount')}}" autocomplete=off  readonly></td>

<input type='hidden' value='{{$associate}}' name='assname' id='assname'>
                                <td width="5"></td>

                                <td width="40"><label><font color='#800000'>Issue Date</font></label></td>
                                <td width="5"></td>
                                <td width="135"><input type="date"  style="width:160px ; height:25px" name="issuedate" id="issuedate" size="30" value="{{Input::old('issuedate')}}" autocomplete='off' onchange='setyear()'></td>
                                <td width="5"></td>
                                <td width="70"><label><font color='#00008B'>Valid Upto</font></label></td>
                                <td width="10"></td>
                                <td width="135"><input type="text" style="width:130px ; height:25px" name="validupto" id="validupto"  value="{{Input::old('validupto')}}" autocomplete=off readonly></td>
                                <td width="5"></td>
                                <td><label><font color='#800000'>Photo Id</font></label></td>
                            </tr>

                            <tr>

                                <td><label><font color='#800000'>Age</font></label></td>
                                <td width="10"></td>
                                <td><input type="text" style="width:110px ; height:25px" name="age" id="age" size="30" value="{{Input::old('age')}}" autocomplete=off></td>

                                <td width='5'></td>
                                <td><label><font color='#800000'>Gender</font></label></td>
                                <td width="5"></td>
                                <td><select name='sex' id='sex' style='width:160px; height:25px' >
                                        <option value='{{Input::old('sex')}}'>{{Input::old('sex')}}</option>
                                        <option value='Male'>Male</option>
                                        <option value='Female'>Female</option>
                                        {{-- <option value='Both'>Both</option> --}}
                                    </select></td>
                                <td></td>

                                <td width="100"><label><font color='#800000'>Name </font></label>
                                <select name="sufix" id="sufix"  style="width:60px; height:25px">
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Ms">Ms</option>
                                <option value="Dr">Dr</option>
                                <option value="Master">Master</option>
                </select> </td>
                                <td width="10"></td>
                                <td width="280" colspan=3>
                                <input type="text" style="width:220px ; height:25px" name="Name" id="Name" size="30"  autocomplete=off value="{{ Input::old('Name') }}"></td>


                                <td width='5'></td>

                            </tr>

                                <tr>
                                <td width="70"><label><font color='#800000'>Contact No</font></label></td>
                                <td width="10"></td>
                                <td width="105"><input type="text" style="width:110px ; height:25px" name="contactNo" id="contactNo" maxlength="10" value="{{Input::old('contactNo')}}" autocomplete=off></td>
                                <td width="5"></td>
                                <td width="60"><label><font color='#00008B'>Enrolled By</font></label></td>
                                <td width="5"></td>
                                <td width="100"><input type="text" style="width:160px ; height:25px" name="issuedby" id="issuedby"  value="{{Input::old('issuedby')}}" autocomplete=off readonly></td>
                                <td></td>
                                <td width="70"><label><font color='#00008B'>Mail Id</font></label></td>
                                <td width="5"></td>
                                <td width="125" colspan=5><input type="text" style="width:220px ; height:25px" name="mailid" id="mailid"  value="{{Input::old('mailid')}}" autocomplete=off></td>

                                <td width='5'></td>
                            </tr>

                            <tr>
                                <td width="90"><label><font color='#00008B'>Remarks</font></label></td>
                                <td width="10"></td>
                                <td width="135" colspan=6><input type="text" style="width:333px ; height:25px" name="remarks" id="remarks" size="30"  autocomplete='off' ></td>
                               <td width="80"><label><font color='#00008B'>Marketed By</font></label></td>
                                <td width="5"></td>
                                <td width="125" colspan=5><input type="text" style="width:220px ; height:25px" name="marketedBy" id="marketedBy" value='{{Input::old('marketedBy')}}'autocomplete=off></td>

                                <td width='5'></td>
                            </tr>

                        </table>

            <font style="font-size: medium;"><b>Address</b></font>
                <fieldset class='fs' style='margin-top:-20px;margin-left:65px;width:860px;height:auto'>
                        <table>
                        <tr>
                                <td width="45"><label><font color='#800000'>Country</font></label></td>
                                <td width="5"></td>
                                <td width="125">
                                    <select style="width:200px; height:25px" name="country" id="country">
                                        <option value="{{ Input::old('country') }}">{{ Input::old('country') }}</option>
                                        <option value='India' selected>India</option>
                                    </select>
                                </td>
                                <td width="15"></td>
                                <td width="70"><label><font color='#800000'>State</font></label></td>
                                <td width="6"></td>
                                <td width="180">

                            <select style="width:200px; height:25px" name="state" id="state" onchange = 'getDistrict()' >
                                    <option value='{{ Input::old('state') }}'>{{ Input::old('state') }}</option>
                                <option value='Andhra Pradesh'> Andhra Pradesh</option>
                                <option value='Telangana' selected> Telangana</option>
                                <option value='Karnataka'> Karnataka</option>

                            </select>

                                </td>
                                <td width="10"></td>
                                <td width="63"><label><font color='#800000'>District</font></label></td>
                                <td width="2"></td>
                                <td width="175">
                                    <select style="width:200px; height:25px" id="district" name="district" >
                                    <option value="Ranga Reddy"> Ranga Reddy</option>
                                    <option value="{{Input::old('district')}}"> {{Input::old('district')}}</option>
                                @foreach($district as $val)
                                <option value='{{$val->district_name}}'>{{$val->district_name}}</option>
                                @endforeach
                                    </select>
                                </td>
                                <TD>&nbsp;</TD>
                            </tr>

                            <tr>
                                <td width="90"><label><font color='#800000'>City/Town/Village</font></label></td>
                                <td width="2"></td>
                                <td width="205"><input type="text" style="width:200px" name="city" id="city" size="30" value="{{ Input::old('city') }}" autocomplete='off'></td>
                                <td width="15"></td>
                                <td width="45"><label><font color='#800000'>Area</font></label></td>
                                <td width="7"></td>
                                <td width="120"><input type="text" style="width:200px;" name="area" id="area" size="30" value="{{ Input::old('area') }}"  autocomplete='off'></td>
                                <td width="20"></td>
                                <td width="70"><label><font color='#00008B'>Street</font></label></td>
                                <td width="7"></td>
                                <td width="180"><input type="text" style="width:200px" name="street" id="street" size="30" value="{{ Input::old('street') }}" autocomplete='off'></td>
                                <td width="10"></td>

                            </tr>

                            <tr>
                                <td width="70"><label><font color='#00008B'>Houseno</font></label></td>
                                <td width="2"></td>
                                <td width="115"><input type="text" style="width:200px" name="houseNo" id="houseNo" size="30" value="{{ Input::old('houseNo') }}" autocomplete='off'></td>
                                <td width="15"></td>
                                <td width="45"><label><font color='#00008B'>Pincode</font></label></td>
                                <td width="6"></td>
                                <td width="120"><input type="text" style="width:200px;" name="pincode" id="pincode" maxlength="6" value="{{ Input::old('pincode') }}"  autocomplete='off'></td>
                                <td width="6"></td>
                                <td width="60"><label><font color='#00008B'>Landmark </font></label></td>
                                <td width="6"></td>
                                <td width="175"><input type="text" style="width:200px" name="landMark" id="landMark" size="30" value="{{ Input::old('landMark') }}" autocomplete='off'></td>
                                <td>&nbsp;</td>
                        </tr>
                    </table>

                </fieldset>
                    <br>
                        <div id='addtionalname'></div>

                        <input type='checkbox' id='aggrement' name='aggrement'>
                    <b>Terms & condtions </b>

                    </fieldset>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <table align="center" style="height:45px;">
                            <tr>
                                <td width="70px"><input type='submit' value='Save' style="width:65px" onclick='updateopdata()'></td>
                                <td width="70px"><input type='button' value='Edit' style="width:65px" onclick='editopdata()'></td>
                                <td width="70px"><input type="button" value="DELETE" id="Delete" style="width:65px" data-toggle="modal" data-target="#myModal" disabled></td>
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-sm">

                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                            <p>Are you sure to delete?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#" class="btn btn-default"  name="delete" onclick="deleteopdata()" id="ok">Ok</a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <td width="70px"><input type='button' value='Cancel' style="width:65px" onclick='cancel()'></td>
                                <td width="70px"><input type='button' value='Exit' style="width:65px" onclick='exit()'></td>
                                <td width="70px"><input type='button' value='Print' style="width:65px" onclick='getprint()'></td>
                            </tr>
                        </table>
                    @if ($errors->any())
                        <ul class="alert alert-danger" style='width:400px;text-align:center; margin-left:285px'>
                            @foreach ($errors->all() as $error)
                                <li style='text-align:left'>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <br>
                        <table style='width:500px; ' border="1" align='center'>
                            <tr style='height:25px'>
                                <th style="text-align:center;"><font color='#800000'>Sl no</font></th>
                                <th style="text-align:center;" ><font color='#800000'>Service Type</font></th>
                                <th style="text-align:center;" ><font color='#800000'>Remaining Cards</font></th>

                            </tr>
                            <?php $i=1;?>
                                  @foreach($cardinfo as $val)
                                    <tr style='text-align:center'>
                                        <td ><?php echo $i;?></td>
                                        <td style='text-align:left'>{{$val->cd_servicetype_vc}}</td>
                                        <td ><?php
                                        $name=$val->cd_servicetype_vc;
                                        $data = DB::select("Select Count(cd_status_vc) as count from card_details where cd_status_vc= 'Not Sold' and cd_servicetype_vc='$name' ");
                                        echo $data[0]->count;
                                        ?></td>
                                    </tr>
                                        <?php $i++?>
                                    @endforeach
                        </table>
                                            </form>


    </div>

                {{-- @include('partials/ads') --}}

            </div>
        </div>
    </div>
    <script>
var i=0;
    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }


    function HandleBrowseClick1()
    {
        var fileinput = document.getElementById("photoid");
        fileinput.click();
    }

function change1()
{
var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("photoid").files[0]);

    oFReader.onload = function(oFREvent) {
      document.getElementById("Preview1").src = oFREvent.target.result;
      // alert(oFREvent.target.result);
      var filePath =oFREvent.target.results;
            console.log(filePath);
    };
    HandleBrowseClick1();
}

    function editopdata()
    {
        i=1;
    // document.getElementById("cardno").disabled = false;
    document.getElementById("Delete").disabled = false;
    }

    function cancel()
    {
    document.opregistrationform.action = "{{ URL::to('/mythriop/opregcancel')}}";
    document.opregistrationform.submit();
    }

function getallpatientdata()
{
    var cardno = (document.getElementById('cardno').value).toUpperCase();
    var form = document.opregistrationform;
    if(i==1)
    {


    var url = '/mythriop/editopcard/' + cardno ;
        downloadUrl(url, function(data)
            {
form.opfees.value = data[0].op_opfees_fl;
form.Name.value = data[0].op_patientname_vc;
form.Preview1.src = data[0].op_photoid_vc;
form.sufix.value = data[0].op_nameprefix_vc;
form.age.value = data[0].op_age_in;
form.sex.value = data[0].op_sex_vc;
form.mailid.value = data[0].op_mailid_vc;
form.contactNo.value = data[0].op_contactno_vc;
form.issuedate.value = data[0].op_date_dt;
form.validupto.value = data[0].op_validupto_dt;
form.amount.value = data[0].op_amount_fl;
form.issuedby.value = data[0].op_issuedby_vc;
form.servicetype.value = data[0].op_schmetype_vc;
form.remarks.value = data[0].op_remarks_vc;
form.country.value = data[0].op_country_vc;
form.state.value = data[0].op_state_vc;
form.district.value = data[0].op_district_vc;
form.city.value = data[0].op_city_vc;
form.area.value = data[0].op_area_vc;
form.street.value = data[0].op_street_vc;
form.houseNo.value = data[0].op_houseno_vc;
form.pincode.value = data[0].op_pincode_vc;
form.landMark.value = data[0].op_landmark_vc;
form.marketedBy.value = data[0].op_marketedby_vc;

            });
}
else
{
    var url = '/mythriop/getopcard/' + cardno ;

    downloadUrl(url, function(data)
    {
        if(data.length == 0)
            {
            alert("Entered Card No is Wrong, Enter Correct Card Number.")
            }
        else
        {
            console.log(data)
            form.issuedby.value = data[0].cd_agentname_vc;
            form.servicetype.value = data[0].cd_servicetype_vc;
            getamount()
        }
    });

}
}

function updateopdata()
{
    var form = document.opregistrationform;
    if(i==1)
    {
    var cardno = (document.getElementById('cardno').value).toUpperCase();
        form.action = "/mythriop/updateopcard/" + cardno;
    }
}

function deleteopdata()
{
    var form = document.opregistrationform;
    var cardno = (document.getElementById('cardno').value).toUpperCase();
    var url = "/mythriop/deleteopcard/" + cardno;
    console.log(url);
    ok.href = url;
}

function getprint()
{
    var form = document.opregistrationform;
    var cardno = (document.getElementById('cardno').value).toUpperCase();
    form.action = "/mythriop/opcard/" + cardno;
    form.submit();
}

function getDistrict()
{
var state = document.getElementById('state').value;
var url = '/mythriop/district/' + state;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.district_name+'">'+data.district_name+'</option>';
});

$('#district').html(districtData);
});

}

function setyear()
{
    var date = new Date(document.getElementById('issuedate').value);
    date.setFullYear(date.getFullYear() +1);
    var valid =  date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
    document.getElementById("validupto").value = valid;
}

function getamount()
{
    var servicetype = document.getElementById("servicetype").value;
    var assname = document.getElementById("assname").value;
    var form = document.opregistrationform;
    var url = '/mythriop/serviceopamount/' + servicetype +'/'+ assname;
    console.log(url);
        downloadUrl(url, function(data)
            {
                console.log(data);
                // form.amount.value= data[0].at_cost_fl;
                // form.opfees.value = data[0].at_regcharges_fl;
                form.amount.value= data[0].stm_cost_fl;
                form.opfees.value = data[0].stm_opfees_fl;
                enableaddnames(data[0].stm_noofpersons_vc)
            });
}


function exit()
{
    document.opregistrationform.action = "{{ URL::to('/mythriop/exitamsd') }}";
    document.opregistrationform.submit();
}

function getheadname()
{
    var headcardno = (document.getElementById('headcardno').value).toUpperCase();
    var form = document.opregistrationform;
    var url = '/mythriop/getheadname/' + headcardno ;
    console.log(url);
        downloadUrl(url, function(data)
            {
                console.log(data);
                form.headname.value= data[0].op_headname_vc;
            });
}

function getheadcardno()
{
    var headname = document.getElementById("headname").value;
    var form = document.opregistrationform;
    var url = '/mythriop/getheadcardno/' + headname ;
    console.log(url);
        downloadUrl(url, function(data)
            {
                console.log(data);
                form.headcardno.value= data[0].op_headcardno_vc;
            });
}
function enableaddnames(x)
{
if(x>1){
    var page="<label>Additional Names:</label><br><table align='center'style='width:800px' border='1'><tr><th style='text-align:center'>S no</th><th style='text-align:center'>Name</th><th style='text-align:center'>Gender</th><th style='text-align:center'>Age</th><th style='text-align:center'>Photo</th></tr>"
    for(var i=1; i<x; i++)
    {
        page += "<tr><td style='text-align:center;width:50px'>"+i+"</td><td style='width:220px'><input type='text' id='opname"+i+"'  name='opname"+i+"'style='width:220px' /></td><td style='width:160px'><select  id='opgender"+i+"' name='opgender"+i+"'style='height:26px;width:160px'><option></option><option value='male'>MALE</option><option value='female'>FEMALE</option><option value='others'>OTHERS</option></select></td><td style='width:110px'><input  type='number' id='opage"+i+"' name='opage"+i+"'style='width:110px' /></td><td style='width:125px; text-align:center;'><img style='height:80px; width:100px; border:1px solid black' id='Preview1' name='Preview1'><label for='photoid"+i+"' class='data' align='center'>SELECT</label><input style='display: none;' type='file' id='photoid"+i+"' name='photoid"+i+"' onchange='change1()'></td></tr>"
    }

    page +='</table>'
}
else{
   var page='';
}
    $("#addtionalname").html(page);

}


function carddataprep()
{
x=2;

var custominfo="{ data:{"
for(var i=1; i<x; i++)
{
var usname=document.getElementById("opname"+i).value;
var usgender=document.getElementById("opgender"+i).value;
var usage=document.getElementById("opage"+i).value;
// var usphoto=document.getElementById("opphoto"+i).src;

if(usname!="")
{
    if(usgender!="")
    {
        if(usage!="")
        {
            // if (usphoto!="")
            // {
                custominfo +="[ usname:"+usname+", usgender:"+usgender+", usage:"+usage+", usphoto"+usphoto+" ]";
                if(i=x-1)
                {
                    return true;
                }
            // }
            // else
            // {
            //     alert("Please Enter Photo");
            //     return false;
            // }
        }
        else
        {
            alert("Please Enter you Age");
            return false;
        }
    }
    else
    {
        alert("Please Enter Gender");
        return false;
    }
}
else
{
    alert("Please Enter Name");
    return false;}
}
custominfo+="}}"
}
    </script>


{!! HTML::style('mythriop/style/css/global.css') !!}
<script src="/mythriop/style/js/typeahead.js"></script>
<script src="/mythriop/style/js/servicetype_autosuggest.js"></script>
<script src="/mythriop/style/js/city_autosuggest.js"></script>

@stop




{{--
----------PREPARING ARRAY --------
 --}}
