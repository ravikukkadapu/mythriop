@extends('layout/headfoot')
@section('content')
<div id="content">
    <div class="container">
        <div class="row blog-page">
            @include('partials/adminmenu')

            <div class="col-md-10 col-xs-6 col-sm-6 blog-box" style='height:535px'>
            @if(Session::has('message'))
                <div class="alert alert-success" style='width:500px; margin-left:245px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <font style="font-size: medium;"><b><center>Queries List</center></b></font>
                {{-- <div class="row"> --}}
                    <form name="querylistform" id="querylistform" method="post" action="">

                        <table style='width:850px; ' border="1" align='center' id='querytable'>
                            <tr style='height:25px'>
                                <th style="text-align:center;"><font color='#800000'>Name</font></th>
                                <th style="text-align:center;width:100px"><font color='#800000'>Contact No </font></th>
                                <th style="text-align:center;width:150px" ><font color='#800000'>Email ID </font></th>
                                <th style="text-align:center;width:250px" ><font color='#800000'>MESSAGE </font></th>
                                <th style="text-align:center;width:100px" ><font color='#800000'>SENT DATE </font></th>
                                {{-- <th style='text-align:center'><font color='#800000'>Action</font></th> --}}

                            </tr>
                            @if($data !=[])
                            @foreach($data as $users)
                            <tr style="text-align:center;" class='clickable-row'onclick='sendreply({{$users->id}})'>
                                <td style='text-align:left'>{{ $users->qt_name_vc }}</td>
                                <td style='text-align:left'>{{$users->qt_contactno_vc}}</td>
                                <td style='text-align:left'>{{$users->qt_emailid_vc}}</td>
                                <td style='text-align:left'>{{$users->qt_message_vc}}</td>
                            <?php
                            $opdate = $users->qt_senddate_vc;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>

                            <td><?php echo $dateformat ?></td>

                                {{-- <td ><a href='#' onclick="datareply('{{ $users->id }}')">Reply</a></td> --}}
                            </tr>

                            @endforeach
                            @else
                            <tr style='text-align:center'>
                                <td colspan=6>No Queries Avaliable</td>
                            </tr>
                            @endif
                        </table>
                    </form>
                </div>
            </div>
        </div>
        {{-- </div> --}}

<script>
function sendreply(id)
{
    document.querylistform.action = '/mythriop/replyquery/'+id;
    document.querylistform.submit();
}


</script>




@stop
