@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif

            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Department  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Department</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->

<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->

        <form action="adddept" method="post" name="deptmasterform" id="deptmasterform"  class="form-horizontal">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Associate Name</label>
                            <div class="col-md-9">
                                <input type='hidden' class="form-control input-sm" name='name' id='name' value='{{$name}}'>

                                <input type='text' name='associatename' id='associatename' class="form-control input-sm" value='{{$val}}' readonly>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Department Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "DepartmentName" name = "DepartmentName"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Dept Headname</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "DeptHeadname" name = "DeptHeadname">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Room No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "RoomNo" name = "RoomNo"/>
                                <input type = "hidden" id = "DepartmentCode" name = "DepartmentCode">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "PhoneNo" name = "PhoneNo">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Location</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "location" name = "location"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">No Of Doctors</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "NoOfDoctors" name = "NoOfDoctors">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Mobile No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "MobileNo" name = "MobileNo"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">No Of Nurses</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "NoOfNurses" name = "NoOfNurses">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">No Of SuppStaff</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "NoOfSuppStaff" name = "NoOfSuppStaff"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "Remarks" name = "Remarks">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
{{--                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select  class="form-control input-sm" name="status" id="status">
                                        <option value='{{Input::old('status')}}'>{{Input::old('status')}}</option>
                                        <option value='Active'>Active</option>
                                        <option value='Inactive'>Inactive</option>
                                    </select>
                            </div>
                        </div>
                    </div> --}}
                    <!--/span-->
                </div>
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Department(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <td>Department Name</td>
                    <td>Dept Headname</td>
                    <td>Mobile No</td>
                    <td>Room No</td>
                    <td>Location</td>
                </tr>
            </thead>
            <tbody>
            @foreach($depts as $dept)
                <tr onclick="editdetails( '{{$dept->dm_departmentcode_vc}}') " class="clickable-row">
                    <td><a href='editdept/{{$dept->dm_departmentcode_vc}}'>{{$dept->dm_deptname_vc }}</a></td>
                    <td >{{$dept->dm_deptheadname_vc}}</td>
                    <td>{{$dept->dm_mobileno_vc}}</td>
                    <td>{{$dept->dm_roomno_vc}}</td>
                    <td >{{$dept->dm_location_vc}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
                        </div>
                    </div>
{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/departmentname_autosuggest.js"></script>


  <script>

function testcancel()
{

    document.deptmasterform.action = '/mythriop/canceldept';
    document.deptmasterform.submit();
}



</script>

@stop
