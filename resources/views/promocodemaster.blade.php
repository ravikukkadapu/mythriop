
@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Promo Code  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Promo Code</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->

<div class="portlet box red-sunglo">

    <div class="portlet-title">

        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>

                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->

        <form action="addpromocode" class="form-horizontal" name="promocodeform" id="promocodeform" method='post'>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3 danger"><span class='red'>*</span> Promo code</label>
                            <div class="col-md-9">
                            <div class="input-icon right input-small margin-top-10">
                                    {{-- <i class="fa fa-search"></i> --}}
                                <input type="text" class="form-control input-sm" name="promocode" id="promocode">
                            </div>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Discount(%)</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="discount" id="discount"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>

                </div>
            </div>
        </form>
    </div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

<!-- Data Table -->

<div class="row">
                <div class="col-lg-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box green-haze">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-list"></i>List Of Item(s)
                            </div>
                            <div class="tools">

                                <a href="" class="collapse">
                                </a>
                                <a href="javascript:;" class="fullscreen">
                                </a>

                            </div>
                        </div>
                        <div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="sample_1">
    <thead>
                            <tr>
                                <th>Promo Code</th>
                                <th>Discount (%)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $val)
                                <tr >
                                    <td><a href='editpromocode/{{$val->pcd_promocode_vc}}'>{{$val->pcd_promocode_vc}}</a></td>
                                    <td>{{$val->pcd_discount_fl}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                        </div>
                    </div>
{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/promocodes_autosuggest.js"></script>

<script>


//Function to cancel the form

function detailscancel()
{

    document.promocodeform.action = '/mythriop/promocodecancel';
    document.promocodeform.submit();
}

</script>
@stop
