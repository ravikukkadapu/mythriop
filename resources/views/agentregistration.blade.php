@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Card Issue <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Card Issue</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
        <form name="agentregistrationform" method="post" action="/mythriop/addcardissuedata" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Assoc. Name</label>
                            <div class="col-md-9">
                            <input type="text" id='agentname' name='agentname' class="form-control input-sm" value='{{Input::old('agentname')}}' onblur='getareas()'></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Assoc. Area</label>
                            <div class="col-md-9">
                        <select class="form-control input-sm " name="areaname" id="areaname" onchange='getagentid()'>
                            {{-- <option value='' >Select</option> --}}
                        <option value='{{Input::old('areaname')}}' >{{Input::old('areaname')}}</option>

                        </select>
                            </div>
                        </div>
                    </div>
                </div>
                                                <!--/row-->
                <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Associate Id</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="agentid" id="agentid"value='{{Input::old('agentid')}}'></div>
                        </div>
                    </div>
                </div>
                                                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Marketed by</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="marketedBy" id="marketedBy" value='{{Input::old('marketedBy')}}'autocomplete=off readonly></div>
                        </div>
                    </div>
{{--                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control input-sm"></div>
                        </div>
                    </div> --}}

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Service Type</label>
                                <div class="col-md-9">
                            <select class="form-control input-sm " name="servicetype1" id="servicetype1" onchange='getamount()'>
                                                            {{-- <option value='' >Select</option> --}}
                            <option value='{{Input::old('servicetype1')}}' >{{Input::old('servicetype1')}}</option>
                                @foreach($data as $val)
                                <option value='{{$val->stm_servicetype_vc}}'>{{$val->stm_servicetype_vc}}</option>
                                @endforeach
                            </select></div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Charge Per Card</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="charge1" id="charge1"  value="{{Input::old('charge1')}}" autocomplete=off readonly></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> No Of Cards</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="noofcards1" id="noofcards1"  value="{{Input::old('noofcards1')}}" autocomplete=off onchange='gettocardno()'></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">From Card No</label>
                        <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name="fromcard1" id="fromcard1"  value="{{Input::old('fromcard1')}}" autocomplete=off readonly></div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">To Card No</label>
                        <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name="tocard1" id="tocard1"  value="{{Input::old('tocard1')}}" autocomplete=off readonly></div>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Issue Date</label>


                            <div class="col-md-9">
                                 <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                                <input type="text" class="form-control" name="issuedate1" id="issuedate1" value='{{Input::old('issuedate1')}}'readonly>
                                                <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>

                            </div>


                            </div>
                        </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Total Amount</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm"name="totalamount1" id="totalamount1"  value="{{Input::old('totalamount1')}}" autocomplete=off readonly></div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>

                </div>
            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif


                       <!--search data table start -->

            <div class="row">
                <div class="col-lg-12">

                    <div class="portlet box green-haze">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-list"></i>List Of Item(s)
                            </div>
                            <div class="tools">

                                <a href="" class="collapse">
                                </a>
                                <a href="javascript:;" class="fullscreen">
                                </a>

                            </div>
                        </div>
                        <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>
                                     Associate Name
                                </th>

                                <th>
                                     Service Type
                                </th>
                                <th>
                                     Issue Date
                                </th>
                                <th>
                                     No. of Cards
                                </th>
                                <th>
                                     From Card No
                                </th>
                                <th>
                                     To Card No
                                </th>
                                <th>
                                     Total Amount
                                </th>
                                <th>
                                     Marketing by
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($agents as $value)

                            <tr>

<td><?php
    $vali = $value->ci_agentid_vc;

    $query = DB::Select("SELECT * from dealer_master where de_agentid_vc='$vali'");

    echo $query[0]->de_agentname_vc; ?> - <?php echo $query[0]->de_area_vc;
?>
</td>
                            <td>{{$value->ci_servicetype_vc}}</td>

                            <?php
                            $opdate = $value->ci_issuedate_dt;
                            $dateformat = date("d-m-Y", strtotime($opdate));
                            ?>
                            <td><?php echo $dateformat ?></td>
                            <td >{{$value->ci_noofcards_in}}</td>
                            <td >{{$value->ci_fromcard_in}}</td>
                            <td >{{$value->ci_tocard_in}}</td>
                            <td>{{$value->ci_totalamount_fl}}</td>
                            <td>{{$value->ci_marketedby_vc}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                       <!--search data table end -->
{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/dealername_autosuggest.js"></script>


    <script>

function getareas()
{


var agentname = document.getElementById('agentname').value;
var url = '/mythriop/getareas/' + agentname;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '<option value="">Select</option>';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.de_area_vc+'">'+data.de_area_vc+'</option>';
});

$('#areaname').html(districtData);

});

}

   function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }
    function getamount()
    {
        var servicetype1 = document.getElementById("servicetype1").value;
        var agentid = document.getElementById("agentid").value;
        if(servicetype1 !='')
        {
        var form = document.agentregistrationform;
        var url = '/mythriop/getserviceamount/' + servicetype1+'/'+agentid ;
        downloadUrl(url, function(data)
            {
                console.log(data)
                if(data.length > 0){
                form.charge1.value= data[0].at_cost_fl;
            }
            else
            {
                alert('No Traiff Avaliable Please enter tariff');
                cancel();
            }
            });
        }

getcardno();
    }

function getcardno()
{
        var form = document.agentregistrationform;
        var url = '/mythriop/getlastcard';
        console.log(url);
        downloadUrl(url, function(data)
        {
            console.log(data);
            form.fromcard1.value = data[0].last_value;
        });
}

    function gettocardno()
    {
        var form = document.agentregistrationform;
        var servicetype1 = document.getElementById("servicetype1").value;

        if(servicetype1 !='')
        {
        var fromcard1 = document.getElementById('fromcard1').value;
        var noofcards1 = document.getElementById('noofcards1').value;

        if(fromcard1 == "" || isNaN(fromcard1))
        {
            fromcard1val = 0;
        }
        else
        {
            fromcard1val = parseInt(fromcard1)
        }

        if(noofcards1 == "" || isNaN(noofcards1))
        {
            noofcards1val = 0;
        }
        else
        {
            noofcards1val = parseInt(noofcards1)
        }
        $total = fromcard1val+noofcards1val-1;
        form.tocard1.value = $total;
        totalamount();
        }
    }

    function totalamount()
    {
        var form = document.agentregistrationform;
        var servicetype1 = document.getElementById("servicetype1").value;

        if(servicetype1 !='')
        {
        var charge1 = document.getElementById('charge1').value;
        var noofcards1 = document.getElementById('noofcards1').value;

        if(charge1 == "" || isNaN(charge1))
        {
            charge1val = 0;
        }
        else
        {
            charge1val = parseInt(charge1)
        }

        if(noofcards1 == "" || isNaN(noofcards1))
        {
            noofcards1val = 0;
        }
        else
        {
            noofcards1val = parseInt(noofcards1)
        }
        $total = charge1val * noofcards1val;
        form.totalamount1.value = $total;
        }
    }

    function getagentid()
    {
        var form = document.agentregistrationform;
        var agentname = document.getElementById('agentname').value;
    var areaname = document.getElementById('areaname').value;
    var url = '/mythriop/getagentid/' + agentname + '/'+ areaname;        console.log(url);
        downloadUrl(url, function(data)
        {
            form.agentid.value = data[0].de_agentid_vc;
            form.marketedBy.value = data[0].de_marketingby_vc;
        });
    var agentid = document.getElementById('agentid').value;
        getallservicetypecardsbyname(agentid,agentname);
// searchdata(agentname);
    }

    function getagentname()
    {
        var form = document.agentregistrationform;
        var agentid = document.getElementById('agentid').value;
        var url = '/mythriop/getagentname/' + agentid;
        console.log(url);
        downloadUrl(url, function(data)
        {
            form.agentname.value = data[0].de_agentname_vc;
            form.marketedBy.value = data[0].de_marketingby_vc;
        });
    }


    function getallservicetypecardsbyname(agentid,agentname)
    {
    var form = document.agentregistrationform;
    var url = '/mythriop/getallservicetypecards/' + agentid;
    downloadUrl(url, function(data)
        {
            len = data.length;
            console.log(len);
        var district = data;
        var districtData = '<option value="">Select Any</option>';
 if(len > 0)
{
        $.each(district, function(index,data)
        {
            districtData += '<option value = "'+data.at_servicetype_vc+'">'+data.at_servicetype_vc+'</option>';
        });
}
else
{

            alert("Tariffs are not defined for "+agentname+" now define tariffs")
}
        $('#servicetype1').html(districtData);
        });

}

    function cancel()
    {
        document.agentregistrationform.action = "{{ URL::to('/mythriop/agentregcancel')}}";
        document.agentregistrationform.submit();
    }



    </script>


@stop
