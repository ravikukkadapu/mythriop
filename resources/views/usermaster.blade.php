@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
 <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            User  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">User Master</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->

<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form name="userform" id="userform" method="post" action="adduser" class="form-horizontal">
            <div class="form-body">


                <div class="row">

                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"> <span class='red'>*</span> Associate Org</label>
                            <div class="col-md-9">

                                <input type="text" class="form-control input-sm" name="agentname" id="agentname" value="{{Input::old('agentname')}}" onblur='getareas()'/>

                                <input type="hidden" class="form-control input-sm" name="agentid" id="agentid" >
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Assoc. Area</label>
                            <div class="col-md-9">
                        <select class="form-control input-sm " name="areaname" id="areaname" onchange='getagentid()'>
                            {{-- <option value='' >Select</option> --}}
                        <option value='{{Input::old('areaname')}}' >{{Input::old('areaname')}}</option>

                        </select>
                            </div>
                        </div>
                    </div>




                </div>
                <!--/row-->
            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"> <span class='red'>*</span> User Type</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="userType" id="userType">
                                        <option value="{{Input::old('userType')}}"> {{Input::old('userType')}}</option>
                                        <option value="0">Admin</option>
                                        <option value="1">Operator</option>

                                    </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"> <span class='red'>*</span> User Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="userName" id="userName" value="{{Input::old('userName')}}"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                            <label class="control-label col-md-3">Email Id</label>
                            <div class="col-md-9">
                                <input type="email" class="form-control input-sm" name="emailid" id="emailid " value="{{Input::old('emailid')}}">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="status" id="status" >
                                    <option value="{{Input::old('status')}}"> {{Input::old('status')}}</option>
                                    <option value='Active' selected>Active</option>
                                    <option value='Inactive'>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!--/span-->

                    <!--/span-->
                </div>
                <!--/row-->
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif




<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of User(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Associate Name</th>
                    <th>User Name</th>
                    <th>Password</th>
                    <th>User Type</th>
                    <th>Email id</th>
                </tr>
            </thead>
            <tbody>
           @foreach($user as $users)
                            <tr style="text-align:center;">
                                <td ><a href='displayuser/{{$users->um_usertype_in}}/{{$users->um_username_vc}}'>
<?php
    $val = $users->um_associate_vc;
    if($users->um_username_vc != 'admin'){
    $query = DB::Select("SELECT * from dealer_master where de_agentid_vc='$val'");

    echo $query[0]->de_agentname_vc; ?> - <?php echo $query[0]->de_area_vc;}
?>
 </a></td>
                                <td> {{ $users->um_username_vc }} </td>
                                <td >{{$users->um_password_vc}}</td>

                                <td>{{ $users->um_usertype_in }}</td>
                                <td>{{ $users->um_mailid_vc }}</td>
                            </tr>
                            @endforeach
            </tbody>
        </table>
    </div>
</div>


{!! HTML::style('mythriop/style/css/global.css') !!}
<script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/username_autosuggest.js"></script>
<script src="/mythriop/style/js/dealername_autosuggest.js"></script>
<script>
function getareas()
{


var agentname = document.getElementById('agentname').value;
var url = '/mythriop/getareas/' + agentname;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '<option value="">Select</option>';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.de_area_vc+'">'+data.de_area_vc+'</option>';
});

$('#areaname').html(districtData);

});

}




//Function to download data
function downloadUrl(url, callback)
    {
        jQuery.getJSON(url, function(data)
        {
            callback(data);
        });
    }

function getagentid()
{
    var form = document.userform;
    var agentname = document.getElementById('agentname').value;
    var areaname = document.getElementById('areaname').value;
    var url = '/mythriop/getagentid/' + agentname + '/'+ areaname;

    // console.log(url);
        downloadUrl(url, function(data)
        {
            console.log(data[0].de_agentid_vc)
            form.agentid.value = data[0].de_agentid_vc;
        });
// searchdet()
}
</script>


@stop
