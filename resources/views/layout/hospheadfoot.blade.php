<!DOCTYPE html>
<?php
$currentUriPageName = substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
// echo $currentUriPageName;
?>
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.6.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Easy OP Card >> {{ $title }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
        <link href="/mythriop/style/assets/admin/pages/css/search.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="/mythriop/style/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="/mythriop/style/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<style>
.red{
    color:red;
}
</style>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-quick-sidebar-over-content page-header-fixed page-footer-fixed">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{URL::to('/mythriop/')}}">
                        <img src="/mythriop/style/images/EOP.png" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>

                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img alt="" class="img-circle" src="/mythriop/style/images/user.png"/>
                    <span class="username username-hide-on-mobile">
                    <?php echo Session::get('username'); ?> </span>
                    <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
{{--                         <li>
                            <a href="extra_profile.html">
                            <i class="icon-user"></i> My Profile </a>
                        </li> --}}
                        <li>
                            <a href="{{ URL::to('/mythriop/logout')}}">
                            <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>

                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler">
                            </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <li class="sidebar-search-wrapper">
                            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                            <form class="sidebar-search " action="extra_search.html" method="POST">
                                <a href="javascript:;" class="remove">
                                    <i class="icon-close"></i>
                                </a>

                            </form>
                            <!-- END RESPONSIVE QUICK SEARCH FORM -->
                        </li>

                        <li class="start <?php if($currentUriPageName=='user'){ ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/user')}}">
                                <i class="icon-home"></i>
                                <span class="title">Home</span>
                            </a>
                        </li>

                        <li class=" <?php
                        if($currentUriPageName=='departmentmaster' ||
                            $currentUriPageName=='qualificationmaster' ||
                            $currentUriPageName=='doctormaster' ||
                            $currentUriPageName=='associatemarketingstaff'){ ?>active open <?php } ?>">
                            <a href="javascript:;">
                                <i class="icon-grid"></i>
                                <span class="title">Masters</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu ">
                            @if(Session::get('type')=='Hospital')
                        <li class="start <?php if($currentUriPageName=='departmentmaster'){ ?>active open <?php } ?>">
                                    <a href="{{ URL::to('/mythriop/departmentmaster')}}">
                                        Department</a>
                                </li>
                            @endif
                            @if(Session::get('type')=='Hospital')
                        <li class="start <?php if($currentUriPageName=='qualificationmaster'){ ?>active open <?php } ?>">
                                    <a href="{{ URL::to('/mythriop/qualificationmaster')}}">
                                        Quaflication</a>
                                </li>
                            @endif
                            @if(Session::get('type')=='Hospital')
  <li class="  <?php
                        if($currentUriPageName=='doctormaster' )
                        { ?>active open <?php } ?>">
                                    <a href="{{ URL::to('/mythriop/doctormaster')}}">
                                        Doctor</a>
                                </li>
                            @endif
  <li class="  <?php
                        if($currentUriPageName=='associatemarketingstaff' )
                        { ?>active open <?php } ?>">
                                    <a href="{{ URL::to('/mythriop/associatemarketingstaff')}}">
                                        Marketing Staff </a>
                                </li>

                            </ul>

                        </li>

  <li class="  <?php
                        if($currentUriPageName=='opregister' )
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/opregister')}}">
                                <i class="icon-notebook"></i>
                                <span class="title">OP Card Registration</span>
                            </a>
                        </li>

  <li class="  <?php
                        if($currentUriPageName=='optransactions' )
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/optransactions')}}">
                                <i class="icon-home"></i>
                                <span class="title">Visit Information</span>
                            </a>
                        </li>


  <li class="  <?php
                        if($currentUriPageName=='associatereports' ||$currentUriPageName=='associatesearch' )
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/associatereports')}}">
                                <i class="icon-bar-chart"></i>
                                <span class="title">REPORTS</span>
                            </a>
                        </li>

  <li class="  <?php
                        if($currentUriPageName=='printcards' )
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/printcards')}}">
                                <i class="icon-docs"></i>
                                <span class="title">Print Cards</span>
                            </a>
                        </li>

                        <li class="last <?php
                        if($currentUriPageName=='specialoffers' )
                        { ?>active open <?php } ?>">
                            <a href="javascript:;">
                                <i class="icon-grid"></i>
                                <span class="title">Special Offers</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu ">

                                <li <?php if($currentUriPageName=='specialoffers'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/specialoffers')}}">New</a>
                                </li>


                            </ul>

                        </li>




                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                @yield("content")
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
                2014 &copy; Metronic by keenthemes.
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="/mythriop/style/assets/global/plugins/respond.min.js"></script>
        <script src="/mythriop/style/assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
 <script src="/mythriop/style/assets/global/plugins/respond.min.js"></script>
        <script src="/mythriop/style/assets/global/plugins/excanvas.min.js"></script>

        <script src="/mythriop/style/js/jquery-2.1.1.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="/mythriop/style/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <script type="text/javascript" src="/mythriop/style/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="/mythriop/style/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
        <script src="/mythriop/style/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>

<script type="text/javascript" src="/mythriop/style/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="/mythriop/style/assets/admin/pages/scripts/table-advanced.js"></script>

<script src="/mythriop/style/js/typeahead.bundle.js"></script>

        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                Demo.init(); // init demo features
   TableAdvanced.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
