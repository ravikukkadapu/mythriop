<!DOCTYPE html>
<?php
$currentUriPageName = substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
// echo $currentUriPageName;
?>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Easy OP Card >> {{ $title }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/sass/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
        <link href="/mythriop/style/assets/admin/pages/css/search.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="/mythriop/style/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="/mythriop/style/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
        <link href="/mythriop/style/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="/mythriop/style/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<style>
.red{
    color:red;
}
</style>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-quick-sidebar-over-content page-header-fixed page-footer-fixed">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{URL::to('/mythriop/')}}">
                        <img src="/mythriop/style/images/EOP.png" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>

                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img alt="" class="img-circle" src="/mythriop/style/images/user.png"/>
                    <span class="username username-hide-on-mobile">
                    <?php echo Session::get('username'); ?> </span>
                    <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
{{--                         <li>
                            <a href="extra_profile.html">
                            <i class="icon-user"></i> My Profile </a>
                        </li> --}}
                        <li>
                            <a href="{{ URL::to('/mythriop/logout')}}">
                            <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler">
                            </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        <li class="sidebar-search-wrapper">
                            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                            <form class="sidebar-search " action="extra_search.html" method="POST">
                                <a href="javascript:;" class="remove">
                                    <i class="icon-close"></i>
                                </a>

                            </form>
                            <!-- END RESPONSIVE QUICK SEARCH FORM -->
                        </li>
                        <li class="start <?php if($currentUriPageName=='admin'){ ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/admin')}}">
                                <i class="icon-home"></i>
                                <span class="title">Home</span>
                                <!-- <span class="arrow "></span> -->
                            </a>

                        <li class="  <?php
                        if($currentUriPageName=='promocode' ||
                            $currentUriPageName=='departmentfacilities' ||
                        $currentUriPageName=='servicetype' ||
                        $currentUriPageName=='usermaster' ||
                        $currentUriPageName=='categorymaster' ||
                        $currentUriPageName=='dealermaster' ||
                        $currentUriPageName=='marketingstaffmaster')
                        { ?>active open <?php } ?>">
                            <a href="javascript:;">
                                <i class="icon-grid"></i>
                                <span class="title">Masters</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu ">
                                <li <?php if($currentUriPageName=='promocode'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/promocode')}}">Promo Codes</a>
                                </li>

                                <li <?php if($currentUriPageName=='departmentfacilities'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/departmentfacilities')}}">Departments</a>
                                </li>

                                <li <?php if($currentUriPageName=='servicetype'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/servicetype')}}">Service Type</a>
                                </li>

                                <li <?php if($currentUriPageName=='usermaster'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/usermaster')}}">User Master</a>
                                </li>

                                <li <?php if($currentUriPageName=='categorymaster'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/categorymaster')}}">Categories</a>
                                </li>

                                <li <?php if($currentUriPageName=='dealermaster'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/dealermaster')}}">
                                    Network Hospital & Clinics
                                    </a>
                                </li>

                                <li <?php if($currentUriPageName=='marketingstaffmaster'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/marketingstaffmaster')}}">
                                        Marketing Staff </a>
                                </li>
                            </ul>

                        </li>


                        <li class="  <?php
                        if($currentUriPageName=='associatetariffmaster' ||
                            $currentUriPageName=='associatebusinessstariffmaster')
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/associatetariffmaster')}}">
                                <i class="icon-wallet"></i>
                                <span class="title">Tariffs</span>
                                {{-- <span class="arrow "></span> --}}
                            </a>


                        </li>





  <li class="  <?php
                        if($currentUriPageName=='assocaiteverfication' )
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/assocaiteverfication')}}">
                                <i class="icon-note"></i>
                                <span class="title">Associate Verfication</span>
                                <!-- <span class="arrow "></span> -->
                            </a>


                        </li>
  <li class="  <?php
                        if($currentUriPageName=='agentregistration' )
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/agentregistration')}}">
                                <i class="icon-puzzle"></i>
                                <span class="title">Card Issue</span>
                                <!-- <span class="arrow "></span> -->
                            </a>


                        </li>
  <li class="  <?php
                        if($currentUriPageName=='opdata' || $currentUriPageName=='search')
                        { ?>active open <?php } ?>">
                            <a href="{{ URL::to('/mythriop/opdata')}}">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Reports</span>
                                <!-- <span class="arrow "></span> -->
                            </a>
                        </li>




                        <li class="last <?php
                        if($currentUriPageName=='pendingoffers' )
                        { ?>active open <?php } ?>">
                            <a href="javascript:;">
                                <i class="icon-grid"></i>
                                <span class="title">Special Offers</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu ">

                                <li <?php if($currentUriPageName=='specialoffers'){ ?>class="active" <?php } ?>>
                                    <a href="{{ URL::to('/mythriop/pendingoffers')}}">Pending</a>
                                </li>


                            </ul>

                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                @yield("content")
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
                2017 &copy; Technowell. ALL Rights Reserved
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="/mythriop/style/assets/global/plugins/respond.min.js"></script>
        <script src="/mythriop/style/assets/global/plugins/excanvas.min.js"></script>
        <![endif]-->
        <script src="/mythriop/style/assets/global/plugins/respond.min.js"></script>
        <script src="/mythriop/style/assets/global/plugins/excanvas.min.js"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="/mythriop/style/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

<script type="text/javascript" src="/mythriop/style/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script src="/mythriop/style/assets/admin/pages/scripts/table-advanced.js"></script>


<script type="text/javascript" src="/mythriop/style/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/mythriop/style/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="/mythriop/style/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
        <script src="/mythriop/style/assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
        <script src="/mythriop/style/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="/mythriop/style/js/typeahead.bundle.js"></script>

        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                Demo.init(); // init demo features
                var table = $('#sample_1');

        /* Table tools samples: https://www.datatables.net/release-datatables/extras/TableTools/ */

        /* Set tabletools buttons and button container */

        $.extend(true, $.fn.DataTable.TableTools.classes, {
            "container": "btn-group tabletools-dropdown-on-portlet",
            "buttons": {
                "normal": "btn btn-sm default",
                "disabled": "btn btn-sm default disabled"
            },
            "collection": {
                "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
            }
        });

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "tableTools": {
                "sSwfPath": "../../assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "pdf",
                    "sButtonText": "PDF"
                }, {
                    "sExtends": "csv",
                    "sButtonText": "CSV"
                }, {
                    "sExtends": "xls",
                    "sButtonText": "Excel"
                }, {
                    "sExtends": "print",
                    "sButtonText": "Print",
                    "sInfo": 'Please press "CTR+P" to print or "ESC" to quit',
                    "sMessage": "Generated by DataTables"
                }]
            }
        });

        var tableWrapper = $('#sample_1_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
