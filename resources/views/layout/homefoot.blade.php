
<!DOCTYPE html>


<?php
$currentUriPageName = substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["REQUEST_URI"],"/")+1);
// echo $currentUriPageName;
?>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest (the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
  <meta charset="utf-8">
        <title>Easy OP Card >> {{ $title }}</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Metronic Shop UI description" name="description">
  <meta content="Metronic Shop UI keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->
  <link href="/mythriop/style/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/mythriop/style/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END -->

  <!-- Page level plugin styles START -->
  <link href="/mythriop/style/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <link href="/mythriop/style/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
  <link href="/mythriop/style/assets/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="/mythriop/style/assets/global/css/components.css" rel="stylesheet">
  <link href="/mythriop/style/assets/frontend/layout/css/style.css" rel="stylesheet">
  <link href="/mythriop/style/assets/frontend/pages/css/style-revolution-slider.css" rel="stylesheet"><!-- metronic revo slider styles -->
  <link href="/mythriop/style/assets/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="/mythriop/style/assets/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">
  <link href="/mythriop/style/assets/frontend/layout/css/custom.css" rel="stylesheet">
  <!-- Theme styles END -->
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">


    <!-- BEGIN TOP BAR -->
    <div class="pre-header">
        <div class="container">
            <div class="row">
                <!-- BEGIN TOP BAR LEFT PART -->
                <div class="col-md-6 col-sm-6 additional-shop-info">
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-phone"></i><span>040 - 456 671 257</span></li>
                        <li><i class="fa fa-envelope-o"></i><span>info@easyopcard.com</span></li>
                    </ul>
                </div>
                <!-- END TOP BAR LEFT PART -->
                <!-- BEGIN TOP BAR MENU -->
                <div class="col-md-6 col-sm-6 additional-nav">
                    <ul class="list-unstyled list-inline pull-right">
                        <li><a href="{{URL::to('/mythriop/login')}}">Log In</a></li>
                        <li><a href="{{URL::to('/mythriop/feedback')}}">Feedback</a></li>
                    </ul>
                </div>
                <!-- END TOP BAR MENU -->
            </div>
        </div>
    </div>
    <!-- END TOP BAR -->
    <!-- BEGIN HEADER -->
    <div class="header">
      <div class="container">
        <a class="site-logo" href="{{URL::to('/mythriop/')}}"><img src="/mythriop/style/images/EOP.png" alt="Easy Op Card"></a>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit">
          <ul>
                        <li class="start <?php if($currentUriPageName==''){ ?>active open <?php } ?>">
              <a  href="{{URL::to('/mythriop/')}}">
                Home
              </a>
            </li>

                        <li class=" <?php if($currentUriPageName=='searchhospitals'){ ?>active open <?php } ?>">
              <a href="{{URL::to('/mythriop/searchhospitals')}}">
                Search Hospital
              </a>
            </li>

                        <li class=" <?php if($currentUriPageName=='onlineopcard'){ ?>active open <?php } ?>">
              <a  href="{{URL::to('/mythriop/onlineopcard')}}">
                Buy OP Cards Online
              </a>
            </li>

            <!-- END TOP SEARCH -->
          </ul>
        </div>
        <!-- END NAVIGATION -->
      </div>
    </div>
    <!-- Header END -->


    <div class="main">
      <div class="container">

@yield("content")

      </div>
    </div>

  <!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN BOTTOM ABOUT BLOCK -->
          <div class="col-md-4 col-sm-6 pre-footer-col">
            <h2>About us</h2>
            <p>Easy Op Card.</p>

          </div>
          <!-- END BOTTOM ABOUT BLOCK -->

          <!-- BEGIN BOTTOM CONTACTS -->
          <div class="col-md-4 col-sm-6 pre-footer-col">
            <h2>Our Contacts</h2>
            <address class="margin-bottom-40">

            <font color="#074062"><strong>MEHDIPATNAM</strong></font><br>
            Opp. Pillar No. 80, <br>Inner Ring Road, Maruthi Nagar, <br>Hyderabad - 500050. <br>
             Phone :6040 6458 8805 <br>
             Fax : 40212000 /23030381.<br>
              Email: <a href="mailto:info@easyopcard.com">info@easyopcard.com</a><br>
            Email  @ <a href="mailto:info@mythrihospital.com">info@mythrihospital.com</a><br>
              Skype: <a href="skype:easyopcard">easyopcard.com</a>
            </address>
          </div>
          <!-- END BOTTOM CONTACTS -->

          <!-- BEGIN TWITTER BLOCK -->
          <div class="col-md-4 col-sm-6 pre-footer-col">
             <br><br>
            <address class="margin-bottom-40">
<font color="#074062"><strong>CHANDA NAGAR </strong></font><br>
             Plot # 5-4/12-16, <br>Main Road, Chanda Nagar, <br>Hyderabad - 500050. <br>
             Phone :64 63 33 57/58/59 <br>
             Fax : 40212000 /23030381.<br>
              Email: <a href="mailto:info@easyopcard.com">info@easyopcard.com</a><br>
            Email  @ <a href="mailto:info@mythrihospital.com">info@mythrihospital.com</a><br>
              Skype: <a href="skype:easyopcard">easyopcard.com</a>
            </address>
          </div>
          <!-- END TWITTER BLOCK -->
        </div>
      </div>
    </div>
    <!-- END PRE-FOOTER -->
    <!-- BEGIN FOOTER -->
    <div class="footer" style='bottom:0px;'>
      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
          <div class="col-md-6 col-sm-6 padding-top-10">
                2017 &copy; Technowell. ALL Rights Reserved
          </div>
          <!-- END COPYRIGHT -->
          <!-- BEGIN PAYMENTS -->
          <div class="col-md-6 col-sm-6">

          </div>
          <!-- END PAYMENTS -->
        </div>
      </div>
    </div>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="/mythriop/style/assets/global/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/mythriop/style/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="/mythriop/style/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/mythriop/style/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="/mythriop/style/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="/mythriop/style/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

    <!-- BEGIN RevolutionSlider -->

    <script src="/mythriop/style/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
    <script src="/mythriop/style/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
    <script src="/mythriop/style/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
    <script src="/mythriop/style/assets/frontend/pages/scripts/revo-slider-init.js" type="text/javascript"></script>
    <!-- END RevolutionSlider -->

    <script src="/mythriop/style/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/mythriop/style/js/typeahead.bundle.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initOWL();
            RevosliderInit.initRevoSlider();
            Layout.initTwitter();
            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
