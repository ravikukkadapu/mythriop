<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OP Card</title>
</head>

<body>
<table border=1 style='height:200px; width:400px'>
    <tr>
        <td style='width:250px;'>
            <table border=0 style='margin-top:-10px'>
                <tr>
                    <td style='text-align:center;' colspan=2><pre>
<B>Mythri Hospitals</B><br>
<label style='font-size:10px'>(A DIVISION OF SURYAPRAKASH HOSPITALS
& DIAGNOSTICS PVT LTD.)
 E-mail:info@mythrihosiptals
 Phone no:040-66558877</label></pre>
                    </td>
                </tr>

                <tr>
                <td style=' padding-left:15px; font-size:12px; width:125px'>
                <label><b>Card No :</b> {{$data->op_cardno_vc}}</label>
                </td>

                <td style='text-align:center;' rowspan=3>
                <img src ='{{$data->op_photoid_vc}}' style='width:90px; height:90px; border:1px solid black'>
                </td>
                </tr>

                <tr>
                <td style=' padding-left:15px; font-size:12px;'>
                <label><b>Issue Date :</b> {{$data->op_date_dt}}</label>
                </td>
                </tr>

                <tr>
                <td style=' padding-left:15px; font-size:12px;'>
                <label><b>Valid Upto :</b> {{$data->op_validupto_dt}}</label>
                </td>
                </tr>
                <tr>
                <td style=' padding-left:15px; font-size:12px;' colspan=2>
                <label><b>Name :</b> {{$data->op_patientname_vc}}</label>
                </td>
                </tr>
            </table>
        </td>

        <td style='width:240px;'>
            <table border=0 style="top:50px">
                <tr>
                    <td style=' padding-left:15px; font-size:12px;'>
                <label><b>Age :</b> {{$data->op_age_in}}</label>
                    </td>
                    <td style=' padding-left:15px; font-size:12px;'>
                <label><b>Sex :</b> {{$data->op_sex_vc}}</label>
                    </td>
                </tr>
                <tr>
                    <td colspan=2 style=' padding-left:15px; font-size:12px;'>
                <label><b>Address :</b> {{$data->op_houseno_vc}}, {{$data->op_landmark_vc}}, {{$data->op_street_vc}}, {{$data->op_area_vc}}, {{$data->op_city_vc}}, {{$data->op_district_vc}}, {{$data->op_state_vc}}, {{$data->op_country_vc}}-{{$data->op_pincode_vc}}.</label>
                    </td>
                </tr>
                <tr>
                    <td style=' padding-left:15px; font-size:12px;' colspan=2>
                <label><b>Scheme Type :</b> {{$data->op_schmetype_vc}}</label>
                    </td>
                    </tr>
                    <tr>
                    <td style=' padding-left:15px; font-size:12px;' colspan=2>
                <label><b>Issued By :</b> {{$data->op_issuedby_vc}}</label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</body>
</html>
