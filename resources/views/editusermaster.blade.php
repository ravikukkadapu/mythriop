@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
<!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            User  <small>(Edit)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">User Master</a>

                    </li>

                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>EDIT USER MASTER
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form name="userform" id="userform" method="post" action="{{ URL::to('/mythriop/updateuserdetails')}}" class="form-horizontal">
        @foreach($data as $data)
            <div class="form-body">
<?php
    $val = $data->um_associate_vc;
    if($data->um_username_vc != 'admin'){
    $query = DB::Select("SELECT * from dealer_master where de_agentid_vc='$val'");

?>
 <div class="row">

                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"> <span class='red'>*</span> Associate Org</label>
                            <div class="col-md-9">

                                <input type="text" class="form-control input-sm" name="agentname" id="agentname" value=
                                "<?php echo $query[0]->de_agentname_vc; ?> " onblur='getareas()'/>

                                <input type="hidden" class="form-control input-sm" name="agentid" id="agentid" value="{{$data->um_associate_vc}}">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Assoc. Area</label>
                            <div class="col-md-9">
                        <select class="form-control input-sm " name="areaname" id="areaname" onchange='getagentid()'>
                            {{-- <option value='' >Select</option> --}}
                        <option value='<?php echo $query[0]->de_area_vc; ?>'><?php echo $query[0]->de_area_vc; ?></option>

                        </select>
                            </div>
                        </div>
                    </div>
<?php
}
?>

                </div>
                <!--/row-->






            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"> <span class='red'>*</span> User Type</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="userType" id="userType">
                                        <option value="{{$data->um_usertype_in}}"> {{$data->um_usertype_in}}</option>
                                        <option value="0">Admin</option>
                                        <option value="1">Operator</option>

                                    </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"> <span class='red'>*</span> User Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="userName" id="userName" value='{{$data->um_username_vc}}'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                            <label class="control-label col-md-3">Email Id</label>
                            <div class="col-md-9">
                                <input type="email" class="form-control input-sm" name="emailid" id="emailid" value='{{$data->um_mailid_vc}}'>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="status" id="status" >
                                    <option value="{{Input::old('status')}}"> {{Input::old('status')}}</option>
                                    <option value='Active' selected>Active</option>
                                    <option value='Inactive'>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
@endforeach
                    <!--/span-->
                </div>
                <!--/row-->
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

{!! HTML::style('mythriop/style/css/global.css') !!}

<script src="/mythriop/style/js/username_autosuggest.js"></script>


<script>

//Function to cancel the form
function userdetailscancel()
{
    document.userform.action = "{{ URL::to('/mythriop/usercancel')}}";
    document.userform.submit();
}

</script>




@stop
