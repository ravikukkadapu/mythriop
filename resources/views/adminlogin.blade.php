@extends('layout/headfoot')
@section('content')

    <div id="content">
        <div class="container">
            <div class="row blog-page">
                @include('partials/loginmenu')

                <!-- Start -posts -->
                <div class="col-md-8 col-xs-6 col-sm-6 blog-box" style="padding-right: 2em; height:535px">
            @if(Session::has('message'))
                <div class="alert alert-success" style='width:500px; margin-left:245px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
                    <!-- Start Post -->
 {{--                    <div class="col-md-4" style="margin-top: 0.8em">
                        <img src="style/images/cashless.png" style="position: relative; top: 0px; left: 0px; width:230px;
        height: 150px; overflow: hidden;"/>
                    </div>
                    <div class="col-md-8">
                        <p align="justify"><strong>E Bill</strong> is an organization which helps the hospitals to facilitate cashless credit billing facility with different TPA’s and different organizations.Especially for those hospitals not empanelled with TPA’s and not entertaining cashless facility to patients covered with health insurance.E Bill also helps hospitals in empanelment of hospitals for cash less billing with different TPA’s.
                    </div></p>
                    <p align="justify" style="margin-left">These days more and more patients are going in to health insurance coverage either through personal insurance policies or company provided policies or dependent policies where children are including their parents in their health insurance policies. Hence many &nbsp;&nbsp;&nbsp;people with insurance coverage prefer cash cashless facility as they need not pay cash especially in high cost medical treatment.</p>
                    <div class="col-md-6">
                        <p align="justify"> This will in turn increase the turn over of the hospital and hence increase the revenue of the hospital.
                        There are around 12 TPA companies in India . You have to apply with hospital building details, different permissions, facilities and doctors  details.
                        There are no agencies or consultants. E bill will guide and help those hospitals who have signed MOU for credit billing.
                        The hospital need not invest any money. The hospital should have a computer with scanning and internet facility or an android phone with whatsappp facility.
                        It is not difficult. But one has to submit all the necessary building details, tariff details, few  licenses related to hospital.
                        Usually it will take two to three months if all the documents are in order.It will cost nominal amount.
                        There are no agencies or consultants. E bill will guide and help those hospitals who have signed MOU for credit billing.
                        E Bill will not charge any monthly or yearly retainer fee.
                        </p>
                    </div> --}}
                    <div class="col-md-6">
                <div class="row" style='margin-top:10px;'>
                    <font style="font-size: medium; margin-left:245px"><b>ADMIN LOGIN</b></font>
                    <form name="loginform" method="post" action="/mythriop/admindologin">
                        <fieldset class='fs' align='center' style='margin-top:5px; margin-left:235px; width:320px; height:auto;'>

                            <table width="300" border="0" style='margin-top:10px'>
                                <tr>
                                    <td width="70"><label><font color='#800000'>User Name</font></label></td>
                                    <td width="20">:</td>
                                    <td width="180"><input type="text" style="width:170px ; height:25px" name="username" id="username" size="30" value="{{Input::old('username')}}" autocomplete='off'></td>
                                </tr>
                                    <input type="hidden"  name="opdate" id="opdate" >

    <input type="hidden"  name="optime" id="optime" >
                                <tr>
                                    <td width="70"><label><font color='#800000'>Password</font></label></td>
                                    <td width="20">:</td>
                                    <td width="180"><input type="password" style="width:170px ; height:25px" name="Password" id="Password" size="30" value="{{Input::old('Password')}}" autocomplete='off'></td>
                                </tr>

                                <tr>
                                    <td colspan=3 align='center'><input type="checkbox" name="remember_me" id="remember_me" onClick='savepassword()'>&nbsp;&nbsp;<label>Remember my Password.</label></td>
                                </tr>
                                <tr>
                                    <td colspan =3 align='center'><label><font color="#FF0000">  Forgot Password ? </font><font color="#0000FF"><u><a href='#' style='color :blue; ' onclick='forgotpassword()'>Click Here</a></u></font></label></td>
                                </tr>
                            </table>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if ($errors->any())
                        <ul class="alert alert-danger" style='width:285px;text-align:center; '>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                        <table align="center" style="height:45px;">
                            <tr>
                                <td width="145px"><input type='button' value='Change  Password' style="width:140px" onclick='changepassword()' ></td>
                                <td width="70px"><input type='button' value='Submit' style="width:65px" onclick='dologin()'></td>
                                <td width="70px"><input type='button' value='Cancel' style="width:65px" onclick='exit()'></td>
                            </tr>
                        </table>
                        </fieldset>
                    </form>
                </div>

                    </div>
                </div>

                @include('partials/ads')

            </div>
        </div>
    </div>
<script>

    function exit()
    {
        document.loginform.action = "{{ URL::to('/mythriop/loginexit') }}";
        document.loginform.submit();
    }

    function savepassword()
    {
        var username = document.getElementById('username').value;
        var password = document.getElementById('Password').value;
        if(document.getElementById("remember_me").checked == true)
                        {
                            window.localStorage["username_rem"] = username;
                            window.localStorage["password_rem"] = password;
                        }
                        else
                        {
                            window.localStorage["username_rem"] ='';
                            window.localStorage["password_rem"] ='';
                        }
    }
    function forgotpassword()
    {
        // var username = document.getElementById('username').value;
        document.loginform.action = "{{ URL::to('/mythriop/forgotpassword') }}";
        document.loginform.submit();
    }
    function dologin()
    {
        document.loginform.action = "{{ URL::to('/mythriop/admindologin') }}";
        document.loginform.submit();
    }

    function changepassword()
    {
        // var username = document.getElementById('username').value;
        document.loginform.action = "/mythriop/changepassword";
        document.loginform.submit();
    }
    // window.onload=data();
</script>
@stop
