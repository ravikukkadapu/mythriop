@extends('layout/headfoot')
@section('content')
{{--  @if(Session::has('message'))
                <div class="alert alert-success" style='width:500px; margin-left:245px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif --}}
<!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Promo Code  <small>(Edit)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Promo Code</a>

                    </li>

                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">

    <div class="portlet-title">

        <div class="caption">
            <i class="fa fa-plus"></i>EDIT PROMO CODE MASTER
        </div>

                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->

        <form action="{{ URL::to('/mythriop/updatepromocode')}}" class="form-horizontal" name="promocodeform" id="promocodeform" method='post'>
        @foreach($data as $data)
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3 danger"><span class='red'>*</span> Promo code</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="promocode" value='{{$data->pcd_promocode_vc}}' readonly >
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Discount(%)</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="discount" id="discount" value='{{$data->pcd_discount_fl}}'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
            </div>
            @endforeach
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" onclick='detailscancel()' class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>

                </div>
            </div>
        </form>
    </div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

<!-- Data Table -->

{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/promocodes_autosuggest.js"></script>
<script>

//Function to cancel the form

function detailscancel()
{

    document.promocodeform.action = '/mythriop/promocodecancel';
    document.promocodeform.submit();
}

</script>

@stop
