@extends('layout/homefoot')
@section('content')




                <!-- Start -posts -->
            <div class="col-md-12" style="margin-top:1em; height:535px">
<div class="col-md-4">
<font color="#074062"><strong>CHANDA NAGAR </strong></font><br>
             Plot # 5-4/12-16, <br>Main Road, Chanda Nagar, <br>Hyderabad - 500050. <br>
             Phone :64 63 33 57/58/59 <br>
             Fax : 40212000 /23030381.<br>
           <!--<p align="center"><img src="images/about2.png" /> </p>-->
             Email  @ info@mythrihospital.com  <br><br>
            <font color="#074062"><strong>MEHDIPATNAM</strong></font><br>
            Opp. Pillar No. 80, <br>Inner Ring Road, Maruthi Nagar, <br>Hyderabad - 500050. <br>
             Phone :6040 6458 8805 <br>
             Fax : 40212000 /23030381.<br>
           <!--<p align="center"><img src="images/about2.png" /> </p>-->
            Email  @ info@mythrihospital.com
</div>
<div class="col-md-8">
            @if(Session::has('message'))
                <div class="alert alert-success" style='width:350px; margin-left:-25px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
 <div class="main">
 <strong><font size="+1">Make A Query Here..!</font></strong>
  <form name="form" method="post" action="/mythriop/sendquery">

            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3 danger">Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="name" id="name">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3 danger">Mobile No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="mobile" id="mobile">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3 danger">Email Id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="email" id="email">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3 danger">Message</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="message" id="message">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default" onclick='searchcancel()'>Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>

     </form>
<br>
                          @if ($errors->any())
                        <ul class="alert alert-danger" style='width:300px;text-align:center; '>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
 </div> </div>    </div>

                {{-- @include('partials/ads') --}}

@stop
