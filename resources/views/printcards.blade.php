@extends('layout/hospheadfoot')
@section('content')
        <!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Easy OP Card(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Card No</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Contact No</th>

                </tr>
            </thead>
            <tbody>
                @foreach($data as $val)
                <tr>
                    <td > <a href='printcard/{{$val->op_cardno_vc}}/{{$val->op_patientname_vc}}/{{$val->op_age_in}}'>{{$val->op_cardno_vc}}</a></td>
                    <td>{{$val->op_nameprefix_vc}} {{$val->op_patientname_vc}}</td>
                    <td>{{$val->op_age_in}}</td>
                    <td>{{$val->op_sex_vc}}</td>
                    <td>{{$val->op_contactno_vc}}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
                        </div>
                    </div>

<script>


//Function to download data
function downloadUrl(url, callback)
{
    $.getJSON(url, function(data)
    {
        callback(data);
    });
}

function searchdata()
{
    var form=document.getElementById('printcardsform');
    var cardno = document.getElementById('cardno').value;
    var contactno = document.getElementById('contactno').value;
    if(cardno =='' && contactno == '')
    {
        alert("Enter Cardno or Phone Number.")
    }
    else
    {

        if(cardno =='' && contactno !='')
        {
            var url = '/mythriop/getbyphoneno/' + contactno ;
        }

         else if(cardno !='' && contactno =='')
        {
            var url = '/mythriop/getbycardno/' + cardno ;
        }


        downloadUrl(url, function(data)
        {

        var districtData = '<table style="width:600px;" align="center" border="1" id="tablew" ><tr style="height:25px;color:#800000"><th style="text-align:center;">Name</th><th style="text-align:center;">Age</th><th style="text-align:center; ">Gender</th><th style="text-align:center; ">Contact No</th><th style="text-align:center; ">Action</th></tr>';;
        if(data.length <=0)
        {
                alert("Invalid Card number.")
        }
        else{
        $.each(data, function(index,data)
        {
            districtData += '<tr style="text-align:left" ><td >'+data.op_nameprefix_vc+'.'+data.op_patientname_vc+'</td><td>'+data.op_age_in+'</td><td>'+data.op_sex_vc+'</td><td>'+data.op_contactno_vc+'</td><td><a href="#" onclick=printdetails("'+data.op_cardno_vc+'","'+data.op_patientname_vc+'","'+data.op_age_in+'") >print</a></td</tr>';

        });
    }
        districtData +='</table>'

        $('#abc').html(districtData);
        });
    }

}



//Function to cancel the form

function detailscancel()
{

    document.printcardsform.action = '/mythriop/thiscancel';
    document.printcardsform.submit();
}
//Function to exit the form
function exitdetails()
{
    document.printcardsform.action = "{{ URL::to('/mythriop/exitamsd')}}";
    document.printcardsform.submit();
}

function printdetails(no,name,age)
{
    console.log('1');
    var form = document.printcardsform;
    var cardno = (document.getElementById('cardno').value).toUpperCase();
    form.action = "/mythriop/printcard/" + cardno +"/"+ name +"/"+age;
    form.submit();
}
</script>



@stop
