@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif

            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Department  <small>(Edit)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Department</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->


<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>EDIT DEPARTMENT MASTER
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
    @foreach($data as $data)
        <form action="{{ URL::to('/mythriop/updatedept')}}" method="post" name="deptmasterform" id="deptmasterform"  class="form-horizontal">

            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span>Associate Name</label>
                            <div class="col-md-9">
                                <input type='hidden' class="form-control input-sm" name='name' id='name'>

                                <?php    $query = DB::select("select * from dealer_master where de_agentid_vc = '$val'");
        $value = $query[0]->de_agentname_vc .' - '.$query[0]->de_area_vc;?>
                                <input type='text' name='associatename' id='associatename' class="form-control input-sm" value='<?php echo $value; ?>' readonly>

                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span>DepartmentName</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control input-sm" id = "DepartmentName" name = "DepartmentName"  value='{{$data->dm_deptname_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Dept Headname</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "DeptHeadname" name = "DeptHeadname" value='{{$data->dm_deptheadname_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Room No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "RoomNo" name = "RoomNo" value='{{$data->dm_roomno_vc}}'/>
                                <input type = "hidden" id = "DepartmentCode" name = "DepartmentCode" value='{{$data->dm_departmentcode_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "PhoneNo" name = "PhoneNo" value='{{$data-> dm_phoneno_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Location</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "location" name = "location" value='{{$data->dm_location_vc}}'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">No Of Doctors</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "NoOfDoctors" name = "NoOfDoctors" value='{{$data->dm_noofdoctors_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Mobile No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "MobileNo" name = "MobileNo" value='{{$data->dm_mobileno_vc}}'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">No Of Nurses</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "NoOfNurses" name = "NoOfNurses" value='{{$data->dm_noofnurses_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">No Of SuppStaff</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "NoOfSuppStaff" name = "NoOfSuppStaff" value='{{$data->dm_noofsuppstaff_vc}}'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id = "Remarks" name = "Remarks" value='{{$data->dm_remarks_vc}}'>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
{{--                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select  class="form-control input-sm" name="status" id="status">
                                        <option value='{{Input::old('status')}}'>{{Input::old('status')}}</option>
                                        <option value='Active'>Active</option>
                                        <option value='Inactive'>Inactive</option>
                                    </select>
                            </div>
                        </div>
                    </div> --}}
                    <!--/span-->
                </div>
                @endforeach
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default" onclick='testcancel()'>Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->

{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/departmentname_autosuggest.js"></script>

  <script>


function testcancel()
{

    document.deptmasterform.action = '/mythriop/canceldept';
    document.deptmasterform.submit();
}

</script>

@stop
