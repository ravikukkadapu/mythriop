@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif

<style>
.data
{
    font-size:15px;
    color:black;
    background:#EADCAE;
    box-shadow: 2px 3px 5px #888888;
    width:80px;
    height:26px;
    padding-top:3px;
    border-radius: 3px;

}

.dropbtn {
   outline:none;
    background-color:#2b9099;
    border-color:#2b9099;
    margin-bottom:20px;
    color:#FFFFFF;
    width:155px;
    height:25px;
    font-size:14px;
    border-radius:4px;
    box-shadow:2px 2px 2px #888888;
}


.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    margin-top: 0px;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 10px;
    min-height: 150px !important;
    overflow: auto;
    overflow-x: hidden;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #f1f1f1}

.show {display:block;}
</style>
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Network Hospital & Clinics  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Network Hospital & Clinics</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->

<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i> Add New
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form action="adddealer" class="form-horizontal" name="dealermasterform" id="dealermasterform" method="post" enctype="multipart/form-data">
            <div class="form-body">
                <h4 class="form-section">Hospital Details</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Agent Type</label>
                            <div class="col-md-9">
                               <select class="form-control input-sm" name="agenttype" id="agenttype" onchange='disableit()'> <option value="{{Input::old('agenttype')}}">{{Input::old('agenttype')}}</option>
                                <option value='Hospital'>Hospital / Diagnotics</option>
                                <option value='Clinic'> Clinic</option>
                                <option value='Business'>Business Development</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6" id='test'>
                        <div class="form-group">
                            <label class="control-label col-md-3">Category</label>
                            <div class="col-md-9">
                                <select  class="form-control input-sm" name="category" id="category">
<option value='{{Input::old('category')}}'>{{Input::old('category')}}</option>
@foreach($categories as $val)
<option value='{{$val->ctm_categoryname_vc}}'>{{$val->ctm_categoryname_vc}}</option>
@endforeach
</select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Agent Id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="agentid" id="agentid" maxlength="10"  readonly>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span>Agent/Associate Org Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="agentname" id="agentname"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span>Contact Name</label>
                            <div class="col-md-2">
                                <select name="prefix" id="prefix"  class="form-control input-sm">
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Ms">Ms</option>
                                <option value="Dr">Dr</option>
                                <option value="Master">Master</option>
                                </select>
                            </div>
                            <div class="col-md-7">
                                 <input type="text" class="form-control input-sm" name="contactname" id="contactname" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Contact No1</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="contactNo1" id="contactNo1" maxlength="10">
                            </div>
                        </div>
                    </div>
                    <!--/span-->



                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">

                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Contact No 2</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="contactNo2" id="contactNo2"/>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Mail Id</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="mailid" id="mailid">
                            </div>
                        </div>
                    </div>

                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Cst No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="cstno" id="cstno">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Gst No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="gstno" id="gstno"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->




                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Marketed By</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="marketedBy" id="marketedBy"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">24x7 Emergy</label>
                            <div class="col-md-9">
                                <select  class="form-control input-sm" name="emergency" id="emergency" >
                                <option value="{{Input::old('emergency')}}">{{Input::old('emergency')}}</option>
                                <option value='Present'>Present</option>
                                <option value='Absent'>Absent</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->


                <div class="row" id='test1'>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Out Patient Facility</label>
                            <div class="col-md-9">
                                <input type='button' onclick="updatedrop()" class="form-control input-sm" value='Out Patient Facilities'>
                            <div id="myDropdown" class="dropdown-content form-control">
                                @foreach($deptfacilities as $dept)
                                    <a style='height:25px;width:250px'><input type='checkbox' id='{{$dept->dfm_deptname_vc}}' name='color' value='{{$dept->dfm_deptname_vc}}'> {{$dept->dfm_deptname_vc}}</a>
                                @endforeach
                            </div>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">State <span class='red'>*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control input-sm" name="state" id="state" onchange = 'getDistrict()' >
                                        <option value='{{ Input::old('state') }}'>{{ Input::old('state') }}</option>
                                        <option value='Andhra Pradesh'> Andhra Pradesh</option>
                                        <option value='Telangana'> Telangana</option>
                                        <option value='Karnataka'> Karnataka</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Dr Reg no</label>
                            <div class="col-md-9">
                                <input type="text"  class="form-control input-sm" name="drregno" id="drregno"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->



                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Enroll Date</label>
                            <div class="col-md-9">
                                 <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                                <input type="text" class="form-control" name="enrolldt" id="enrolldt" readonly>
                                                <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>

                            </div>
                        </div>
                    </div>
                    <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">MOU</label>
                            <div class="col-md-3">
                                <input type="file" id="browse2" name="documentproof" style="display: none" onChange="Handlechange2();"/>
                                <input type="button" class="form-control input-sm" value="Attach" onclick="HandleBrowseClick2();"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control input-sm" id="filename2" readonly="true"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <h4 class="form-section">Address</h4>
                                                <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Country</label>
                                <div class="col-md-9">
                                    <select class="form-control input-sm" name="country" id="country">
                                        <option value="{{ Input::old('country') }}">{{ Input::old('country') }}</option>
                                        <option value='India' selected>India</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">State <span class='red'>*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control input-sm" name="state" id="state" onchange = 'getDistrict()' >
                                        <option value='{{ Input::old('state') }}'>{{ Input::old('state') }}</option>
                                        <option value='Andhra Pradesh'> Andhra Pradesh</option>
                                        <option value='Telangana'> Telangana</option>
                                        <option value='Karnataka'> Karnataka</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">District <span class='red'>*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control input-sm select2me" id="district" name="district" onchange = 'getdatabytype()'>
                                        <option value="{{Input::old('district')}}"> {{Input::old('district')}}</option>
                                        @foreach($district as $val)
                                            <option value='{{$val->district_name}}'>{{$val->district_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                                                    <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">City/Town/Village<span class='red'>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-sm" name="city" id="city">
                                </div>
                            </div>
                        </div>
                                                    <!--/span-->
                    </div>
                                                <!--/row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Area <span class='red'>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-sm" name="area" id="area">
                                </div>
                            </div>
                        </div>
                                                    <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Street</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-sm" name="street" id="street"/>
                                </div>
                            </div>
                        </div>
                                                    <!--/span-->
                    </div>
                                                <!--/row-->

                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Houseno</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="houseNo" id="houseNo">
                            </div>
                        </div>
                    </div>
                                                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Pincode</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="pincode" id="pincode" />
                            </div>
                        </div>
                    </div>
                                                    <!--/span-->
                </div>
                                                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Landmark</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="landMark" id="landMark">
                        </div>
                    </div>
                </div>


                     <div class="col-md-6">
                        <div class="form-group">

                            <div class="col-md-3">
                                <button type="button" class="btn blue" onclick='getmap()'>Locate</button> <span class='red'>*</span>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-sm" id="latitude" name="latitude" placeholder='Latitude'  value='{{Input::old('latitude')}}' readonly="true"/>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-sm" id="longitude" name="longitude" placeholder='Longitude' value='{{Input::old('longitude')}}' readonly="true"/>
                            </div>
                        </div>
                    </div>
                                                    <!--/span-->
            </div>
            <div class="row" id='mapcon' align="center" style='display:none'>
                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                <div id="map" style='height:400px; width:98%;'></div>
            </div>
                                                <!--/row-->
        </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
</div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Network Hospital and Clinic(s)
                </div>
            <div class="tools">
            {{-- <a href="" class="collapse"></a> --}}
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Agent Name</th>
                            <th>Agent Id</th>
                            <th>Agent Type</th>
                            <th>Category</th>
                            <th>Emergency</th>
                            <th>Contact Name</th>
                            <th>Contact No</th>
                            <th>Marketing By</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $val)
                    <tr >
                        <td><a href='editdealerdetails/{{$val->de_agentid_vc}}'>{{$val->de_agentname_vc}}</a></td>
                        <td>{{$val->de_agentid_vc}}</td>
                        <td>{{$val->de_agenttype_vc}}</td>
                        <td>{{$val->de_hospitalcategory_vc}}</td>
                        <td>{{$val->de_emergency_vc}}</td>
                        <td>{{$val->de_contactname_vc}}</td>
                        <td>{{$val->de_contactno_vc}}</td>
                        <td>{{$val->de_marketingby_vc}}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>

{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/dealername_autosuggest.js"></script>
{{-- <script src="/mythriop/style/js/servicetype_autosuggest.js"></script> --}}
<script src="/mythriop/style/js/citytown_autosuggest.js"></script>
<script src="/mythriop/style/js/dealerarea_autosuggest.js"></script>
{{-- <script src="/mythriop/style/js/category_autosuggest.js"></script> --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXfyHxBWEy8Mhyfnu-nZBc0uYRa5uGtuI&libraries=places&callback=initAutocomplete"
         async defer></script>
    <script>

var map;
var input;
var searchBox;
var lat;
var lng;
var markers = [];
function initAutocomplete()
{
map = new google.maps.Map(document.getElementById('map'),
{
       center: {lat: 17.3850, lng: 78.4867},
        zoom: 13,
        mapTypeId: 'roadmap',
disableDefaultUI: true
});
map.addListener('click', function(event) {
        addMarker(event.latLng);
});



        // Create the search box and link it to the UI element.
        input = document.getElementById('pac-input');
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

map.addListener('bounds_changed', function()
{
          searchBox.setBounds(map.getBounds());
        });

        // Bias the SearchBox results towards current map's viewport.
}



function addMarker(location)
{
lat=location.lat();
lng=location.lng();
document.getElementById("latitude").value=lat;
document.getElementById("longitude").value=lng;
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
setMapOnAll(null);
        markers.push(marker);
      }

      // Sets the map on all markers in the array.
      function setMapOnAll(map)
{
        for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
        }
      }


      function deleteMarkers()
{
        clearMarkers();
        markers = [];
      }


function pantoadd()
{
var places = searchBox.getPlaces();
    if (places.length == 0)
{return;}
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
    if (!place.geometry)
{return;}
if (place.geometry.viewport)
{bounds.union(place.geometry.viewport);}
else{bounds.extend(place.geometry.location);}
    });
    map.fitBounds(bounds);
}


  function assomastersubmit()
{
data=localStorage.assodata;
data=JSON.parse(data);
if(lat!=null && lat!="" && lng!=null && lng!="")
{
data={agenttype:data.agenttype, category:data.category, agentname:data.agentname, cstno:data.cstno, gstno:data.gstno, prefix:data.prefix, contactname:data.contactname, contactNo1:data.contactNo1, contactNo2:data.contactNo2, mailid:data.mailid, marketedBy:data.marketedBy, emergency:data.emergency, drregno:data.drregno, enrolldt:data.enrolldt, documentproof:data.documentproof, country:data.country, state:data.state, district:data.district, city:data.city, area:data.area, street:data.street, landMark:data.landMark, pincode:data.pincode, color:data.color, houseNo:data.houseNo, latitude:lat, longitude:lng}

$.ajax({
                    url:"http://45.55.238.172/proj/adddealer",
                    type:"post",
                    dataType:"json",
jsonp:"callback",
async:true,
                    data:data,
                    ContentType:"application/json",
                    success: function(response)
{
window.location.href="associatemaster.html";
alert(response.message)
},
                    error: function(err)
{alert("Connection error"); console.log(err)}
                });
}else{alert("Please select the location");}
}
</script>

    <script>


function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }


function getDistrict()
{
var state = document.getElementById('state').value;
var url = '/mythriop/district/' + state;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '<option value="">Select</option>';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.district_name+'">'+data.district_name+'</option>';
});

$('#district').html(districtData);
getdatabytype();
});
// getdatabytype();
}

function HandleBrowseClick2()
{
    var fileinput = document.getElementById("browse2");
    fileinput.click();
}
function Handlechange2()
{
var fileinput = document.getElementById("browse2").value;
var res = fileinput.substring(12);
var textinput = document.getElementById("filename2");
textinput.value = res;
}




function disableit()
{
    var vaue = document.getElementById('agenttype').value;
    console.log(vaue)
    if(vaue == 'Business')
    {
        // document.getElementById('hospitaltypevalues').style.display = 'none';
        document.getElementById('test').style.display = 'none';
        document.getElementById('test1').style.display = 'none';

        document.getElementById('prefix').value = 'Mr';
    }
    if(vaue == 'Hospital')
    {
            console.log(vaue)
        // document.getElementById('hospitaltypevalues').style.display = '';
        document.getElementById('test').style.display = '';
        document.getElementById('test1').style.display = '';

        document.getElementById('prefix').value = 'Dr';
    }
    // getdatabytype()

}






// -------------------------------------------------------

    // show update dropdown list
    function updatedrop()
    {    document.getElementById("myDropdown").classList.toggle("show"); }
    // show delete dropdown list
    function deletedrop()
    {    document.getElementById("myDropdown1").classList.toggle("show"); }

    // Close the dropdown if the user clicks outside of it
    $(document).ready(function(e) {
        $("#body").on("click",function(){
    //window.onclick = function(event)
    //{
        if (!event.target.matches('.dropbtn'))
        {   var i;
            var dropdowns = document.getElementsByClassName("dropdown-content");
            for (i = 0; i < dropdowns.length; i++)
            {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show'))
                { openDropdown.classList.remove('show'); }
            }
            var dropdowns = document.getElementsByClassName("dropdown-content1");
            for (i = 0; i < dropdowns.length; i++)
            {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show'))
                { openDropdown.classList.remove('show'); }
            }
        }
    })
});
//---------------end drop down
function checkdealer()
{
    // var agenttype = document.getElementById('agenttype').value;
    var agentname = document.getElementById('agentname').value;
    var contactNo = document.getElementById('contactNo1').value;

    var url = '/mythriop/checkdealer/'+agentname+'/'+contactNo;
// alert(url)
    downloadUrl(url,function(data)
    {
        if(data >0)
        {
            alert("Dealer name already exists");
            document.getElementById("dealermasterform").reset();
        }
    });

}
function getmap()
{
    // alert("in")
    document.getElementById('mapcon').style.display='';
    initAutocomplete();
    setTimeout(function(){alert();
    input = document.getElementById('pac-input');
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    },2000);
}
    </script>


@stop
