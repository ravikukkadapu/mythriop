<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Receipt</title>
</head>

<body>
<table frame="border" rules="all" style='height:200px; width:400px'>
    <tr>
        <td style='text-align:center;' colspan=2><pre>
<label style='font-size:12px'><B>EASY OP CARD</B><br>(A DIVISION OF SURYAPRAKASH HOSPITALS
& DIAGNOSTICS PVT LTD.)
 E-mail:info@easyop
 Phone no:040-66558877</label></pre>
        </td>
    </tr>
    <tr style='height:30px; text-align:center' >
        <td style=' padding-left:15px; font-size:18px;' colspan=2><label><b>RECEIPT </b></label></td>
    </tr>
@foreach($data as $data)
    <tr>
        <td style=' padding-left:15px; font-size:15px;'><label><b>Receipt No:</b>{{$data->td_hospitalopno_vc}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <b>Date :</b>
        <?php
        $date=date_create($data->td_opdate_dt);
        echo date_format($date,"d-m-Y ")
        ?>
        </label></td>
        <td style='text-align:center;width:100px;' rowspan=3>
        <img src ='{{$data->op_photoid_vc}}' style='width:90px; height:90px; border:1px solid black'></td>
    </tr>

    <tr>
        <td style=' padding-left:15px; font-size:15px;'><label><b>Card No :</b> {{$data->op_cardno_vc}}</label></td>
    </tr>

    <tr>
        <td style=' padding-left:15px; font-size:15px;'><label><b>Card Type :</b> {{$data->op_schmetype_vc}}</label></td>
    </tr>

    <tr style='height:30px'>
        <td style=' padding-left:15px; font-size:15px;' colspan=2><label><b>Name :</b> {{$data->op_patientname_vc}}</label></td>
    </tr>

    <tr style='height:30px'>
        <td style=' padding-left:15px; font-size:15px;' colspan=2><label><b>Consulting Doctor :</b> {{$data->td_consultingdoctor_vc}}</label></td>
    </tr >

    <tr style='height:30px'>
        <td style=' padding-left:15px; font-size:15px;' colspan=2><label><b>Op Fees :</b> {{$data->op_opfees_fl}}</label></td>
    </tr>

</table>

@endforeach

</body>
</html>
