
@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Special Offers <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Special Offers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">New</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->

<div class="portlet box red-sunglo">

    <div class="portlet-title">

        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>

                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->

        <form action="addspecialoffers" class="form-horizontal" name="specialoffersform" id="specialoffersform" method='post'>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3 danger"><span class='red'>*</span> Title/Subject</label>
                            <div class="col-md-9">

                                    {{-- <i class="fa fa-search"></i> --}}
                                <input type="text" class="form-control input-sm" name="title" id="title" autocomplete='off'>

                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Description</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="description" id="description" autocomplete='off'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Poster 1</label>
                            <div class="col-md-3">
                                <input type="file" id="browse2" name="documentproof" style="display: none" onChange="Handlechange2();"/>
                                <input type="button" class="form-control input-sm" value="Attach" onclick="HandleBrowseClick2();"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control input-sm" id="filename2" readonly="true"/>
                            </div>
                        </div>
                    </div>

{{-- ---------------------------------------- --}}


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Poster 2</label>
                            <div class="col-md-3">
                                <input type="file" id="browse2" name="documentproof" style="display: none" onChange="Handlechange2();"/>
                                <input type="button" class="form-control input-sm" value="Attach" onclick="HandleBrowseClick2();"/>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control input-sm" id="filename2" readonly="true"/>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>

                </div>
            </div>
        </form>
    </div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

<!-- Data Table -->



<script>


//Function to cancel the form


</script>
@stop
