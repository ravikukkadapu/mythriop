@extends('layout/homefoot')
@section('content')
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>FeedBack Form
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form class="form-horizontal"  name="feedbackform" id="feedbackform" method="post" action="sendfeedback">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Card No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="cardno" id="cardno" onblur='getdata()'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="sa" id="sa"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="phoneno" id="phoneno" onblur ='getdatabyphoneno()'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Hospital/ Clinic</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="hospname" id="hospname" onblur='getcategory()'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Category</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="x" id="x">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Problem Date</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control input-sm"  name="problemdate" id="problemdate"/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Treated By</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="treatedby" id="treatedby">
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Problem Type</label>
                            <div class="col-md-9">
                                <textarea type="text" class="form-control input-sm" id='problemtype' name='problemtype'></textarea>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Treatment</label>
                            <div class="col-md-9">
                                <textarea type="text" class="form-control input-sm" id='treatment' name='treatment'></textarea>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Feed Back / Complaint</label>
                            <div class="col-md-9">
                                <textarea type="text" class="form-control input-sm" id='complaint' name='complaint'></textarea>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>



{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/city_autosuggest.js"></script>
<script src="/mythriop/style/js/feedhosp_autosuggest.js"></script>
<script src="/mythriop/style/js/patientmobile_autosuggest.js"></script>
<script>


    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }

    function feedbackcancel()
    {
        document.feedbackform.action = '/mythriop/feedbackcancel';
        document.feedbackform.submit();
    }



function getcategory()
{
    var sample='';
    var hospname = document.getElementById('hospname').value;
console.log(hospname);
var url = '/mythriop/getcategories/' + hospname;
downloadUrl(url,function(data)
{
var district = data;
len =data.length;

if(len<1)
{
    alert("No Categories are Avaliable in "+ hospname)
}
else if(len == 1)
{
    document.getElementById('x').style.display='none';
var sample = '<input type="text" style="width:190px ; height:25px" name="category" id="category"  value='+data[0].de_hospitalcategory_vc+'>';
}

else if(len>1)
{
    document.getElementById('x').style.display='none';
var sample = '<select style="width:190px ; height:25px" name="category" id="category" ><option value="">Select</option><option value="{{Input::old("category")}}"> {{Input::old("category")}}</option>';

    $.each(district, function(index,data)
    {
        sample += '<option value="'+data.de_hospitalcategory_vc+'">'+data.de_hospitalcategory_vc+'</option>';
    });
sample +='</select>'
    }
$('#abc').html(sample);
});
}

function getdata()
{
    var form = document.feedbackform;
    var cardno = document.getElementById('cardno').value;
    var url = "/mythriop/getbycardno/" + cardno;
    console.log(url);
    var patientdata = '<option Value="">Select</option>';
            downloadUrl(url, function(data)
            {
                if(data.length == 0)
                {
                alert("Entered Card No is Not Registered, Please register first.")
                }
                else if(data.length ==1)
                {
                    document.getElementById('sa').style.display='none';

                    var sample = '<input type="text" style="width:180px ; height:25px" name="patientname" id="patientname"  value='+data[0].op_patientname_vc+'>';
                }
                 else if(data.length >1)
                {
                    document.getElementById('sa').style.display='none';
                    var sample = '<select style="width:180px ; height:25px" name="patientname" id="patientname" ><option value="">Select</option><option value="{{Input::old("patientname")}}"> {{Input::old("patientname")}}</option>';

                    $.each(data, function(index,data)
                    {
                        sample += '<option value="'+data.op_patientname_vc+'">'+data.op_patientname_vc+'</option>';
                    });
                    sample +='</select>'
                }
                $('#qwe').html(sample);

                form.phoneno.value =data[0].op_contactno_vc;
                form.cardno.value = data[0].op_cardno_vc;
            });
        }



function getdatabyphoneno()
{
    var form = document.optransactionform;
    var phoneno = document.getElementById('phoneno').value;
    var url = "/mythriop/getbyphoneno/" + phoneno;
    console.log('1');
    downloadUrl(url, function(data)
        {
        if(data.length == 0)
        {
            alert("Entered Phone No is Not Registered, Please register first.")
        }

        else if(data.length ==1)
        {
            document.getElementById('sa').style.display='none';

            var sample = '<input type="text" style="width:180px ; height:25px" name="name" id="name"  value='+data[0].op_patientname_vc+'>';
        }
        else if(data.length >1)
        {
            document.getElementById('sa').style.display='none';

            var sample = '<select style="width:180px ; height:25px" name="name" id="name" ><option value="">Select</option><option value="{{Input::old("name")}}"> {{Input::old("name")}}</option>';

            $.each(data, function(index,data)
            {
                sample += '<option value="'+data.op_patientname_vc+'">'+data.op_patientname_vc+'</option>';
            });
            sample +='</select>'
        }
        $('#qwe').html(sample);
        form.cardno.value = data[0].op_cardno_vc;
    });
}

</script>
@stop
