@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
<!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Department Facilities  <small>(Edit)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Department Facility</a>
                    </li>

                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>EDIT DEPARTMENT FACILITY MASTER
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form name="deptfacilitiesform" id="deptfacilitiesform" method="post" action="{{ URL::to('/mythriop/updatedeptfacility')}}" class="form-horizontal">
        @foreach($data as $data)
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Department Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="deptname" id="deptname" value='{{$data->dfm_deptname_vc}}' readonly>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="deptstatus" id="deptstatus">
                                    <option value='{{$data->dfm_status_vc}}'>{{$data->dfm_status_vc}}</option>
                                    <option value='Active'>Active</option>
                                    <option value='Inactive'>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->


                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="remarks" id="remarks" value='{{$data->dfm_remarks_vc}}'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default" onclick='detailscancel()'>Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->
{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/departmentfacitiesname_autosuggest.js"></script>



<script>
function detailscancel()
{

    document.deptfacilitiesform.action = '/mythriop/deptfacilitycancel';
    document.deptfacilitiesform.submit();
}

</script>

@stop
