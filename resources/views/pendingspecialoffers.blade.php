@extends('layout/headfoot')
@section('content')
        <!-- Data Table -->
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Special Offers <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Special Offers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Pending</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>Pending Special Offers
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                            <th>Hospital Name</th>
                            <th>Subject</th>
                            <th>Description</th>

                </tr>
            </thead>
            <tbody>
            @foreach($data as $val)
                    <tr >
                        <td><a href='getofferdata/{{$val->so_agentid_vc}}'>{{$val->so_agentname_vc}}</a></td>
                        <td>{{$val->so_subject_vc}}</td>
                        <td>{{$val->so_desc_vc}}</td>

                    </tr>
                    @endforeach
            </tbody>

        </table>
    </div>
</div>
                        </div>
                    </div>

<script>


//Function to download data
function downloadUrl(url, callback)
{
    $.getJSON(url, function(data)
    {
        callback(data);
    });
}




function printdetails(no,name,age)
{
    console.log('1');
    var form = document.printcardsform;
    var cardno = (document.getElementById('cardno').value).toUpperCase();
    form.action = "/mythriop/printcard/" + cardno +"/"+ name +"/"+age;
    form.submit();
}
</script>



@stop
