@extends('layout/headfoot')
@section('content')
<div id="content">
    <div class="container">
        <div class="row blog-page">
            @include('partials/adminmenu')

            <div class="col-md-10 col-xs-6 col-sm-6 blog-box" style='height:535px'>
            @if(Session::has('message'))
                <div class="alert alert-success" style='width:500px; margin-left:245px'>
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
            <font style="font-size: medium;"><b><center>Reply to query</center></b></font>
                {{-- <div class="row"> --}}
                    <form name="datasentform" id="datasentform" method="post" action="/mythriop/sendreply">
@foreach($value as $data)
                        <fieldset class='fs' style='margin-top:0px;margin-left:130px;width:730px;height:auto' >
  <table  border="0">

                            <tr>
                                <td width="80"><label><font color='#800000'>Name</font></label></td>
                                <td width='20'></td>
                                <td width="260"><input type="text" name="name" id="name"  value='{{$data->qt_name_vc}}' readonly style="width:250px;"></td>
                                <td width='20'></td>

                                <td width="120"><label><font color='#800000'>To Address</font></label></td>
                                <td width='20'></td>
                                <td width="260">
                                    <input type="text" name="toaddr" id="toaddr" autocomplete=off value='{{$data->qt_emailid_vc}}' readonly style="width:250px;">
                                </td>

                    <input type="hidden" name="Id" id="Id"  value='{{$data->id}}'>
                            </tr>

                            <tr>
                                <td width="80"><label><font color='#800000'>Message </font></label></td>
                                <td></td>
                                <td rowspan="2">
                                    <b><textarea name="message" style="width:250px; height:75px" id="message" readonly>{{$data->qt_message_vc}}</textarea></b>
                                </td>
@endforeach
                                <td></td>
                                <td width="80"><label><font color='#800000'>Reply </font></label></td>
                                <td></td>
                                <td rowspan="2">
                                    <textarea name="replymessage" style="width:250px; height:75px" id="replymessage" autocomplete = 'off'></textarea>
                                </td>
                            </tr>
                            </table>
                            </fieldset>
                        <table align="center" style="height:45px;width:400px">
                            <tr>
                                <td width="70px" align='right'><input type='submit' value='Save' style="width:65px"  ></td>
                                <td width='10px'></td>
                                <td width="70px"><input type='button' value='Cancel' style="width:65px" onClick="detailscancel()"></td>

                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
        {{-- </div> --}}

<script>
function detailscancel()
{
    document.datasentform.action = '/mythriop/querycancel';
    document.datasentform.submit();
}
</script>




@stop
