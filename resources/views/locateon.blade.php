<!DOCTYPE html>
<html lang="en">
<head>
<title>Associate Master</title>
  <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <link rel="stylesheet" type="text/css" href="css/eopstylesheet.css">
  <link rel="stylesheet" href="css/bootstrap3.3.5.css">
    <script  src="js/jquery-1.10.2.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }
  .pac-div{
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

  .search
  {
position: absolute;
   }

    </style>
</head>
<body>

    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
    <button type="button" class="btn btn-success" onClick="pantoadd()">Search</button>
    <button type="button" class="btn btn-success">Submit</button>
    <div id="map"></div>


    <script>

var map;
var input;
var searchBox;
var lat;
var lng;
var markers = [];
function initAutocomplete()
{
map = new google.maps.Map(document.getElementById('map'),
{
       center: {lat: 17.3850, lng: 78.4867},
        zoom: 13,
        mapTypeId: 'roadmap',
disableDefaultUI: true
});
map.addListener('click', function(event) {
        addMarker(event.latLng);
});



        // Create the search box and link it to the UI element.
        input = document.getElementById('pac-input');
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

map.addListener('bounds_changed', function()
{
          searchBox.setBounds(map.getBounds());
        });

        // Bias the SearchBox results towards current map's viewport.
}



function addMarker(location)
{
lat=location.lat();
lng=location.lng();
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
setMapOnAll(null);
        markers.push(marker);
      }

      // Sets the map on all markers in the array.
      function setMapOnAll(map)
{
        for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
        }
      }


      function deleteMarkers()
{
        clearMarkers();
        markers = [];
      }


function pantoadd()
{
var places = searchBox.getPlaces();
    if (places.length == 0)
{return;}
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
    if (!place.geometry)
{return;}
if (place.geometry.viewport)
{bounds.union(place.geometry.viewport);}
else{bounds.extend(place.geometry.location);}
    });
    map.fitBounds(bounds);
}


  function assomastersubmit()
{
data=localStorage.assodata;
data=JSON.parse(data);
if(lat!=null && lat!="" && lng!=null && lng!="")
{
data={agenttype:data.agenttype, category:data.category, agentname:data.agentname, cstno:data.cstno, gstno:data.gstno, prefix:data.prefix, contactname:data.contactname, contactNo1:data.contactNo1, contactNo2:data.contactNo2, mailid:data.mailid, marketedBy:data.marketedBy, emergency:data.emergency, drregno:data.drregno, enrolldt:data.enrolldt, documentproof:data.documentproof, country:data.country, state:data.state, district:data.district, city:data.city, area:data.area, street:data.street, landMark:data.landMark, pincode:data.pincode, color:data.color, houseNo:data.houseNo, latitude:lat, longitude:lng}

$.ajax({
                    url:"http://45.55.238.172/proj/adddealer",
                    type:"post",
                    dataType:"json",
jsonp:"callback",
async:true,
                    data:data,
                    ContentType:"application/json",
                    success: function(response)
{
window.location.href="associatemaster.html";
alert(response.message)
},
                    error: function(err)
{alert("Connection error"); console.log(err)}
                });
}else{alert("Please select the location");}
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXfyHxBWEy8Mhyfnu-nZBc0uYRa5uGtuI&libraries=places&callback=initAutocomplete"
         async defer></script>
<script type="text/javascript" charset="utf-8" src="cordova.js"></script>

</body>
</html>
