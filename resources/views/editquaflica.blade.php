@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif


            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            QUALIFICATION  <small>(Edit)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">QUALIFICATION</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>EDIT QUALIFICATION MASTER
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
    @foreach($data as $data)
        <form class="form-horizontal" name="qualificationform" id="qualificationform" method="post" action="{{ URL::to('/mythriop/updatequalification')}}">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Qualification</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="qualification" id="qualification" value='{{$data->qm_qualificaton_vc}}'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
<input type="hidden" class="form-control input-sm" name="qualificationcode" id="qualificationcode" value='{{$data->qm_qualificatoncode_vc}}'>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select  class="form-control input-sm" name="status" id="status">
                                        <option value='{{$data->qm_status_vc}}'>{{$data->qm_status_vc}}</option>
                                        <option value='Active'>Active</option>
                                        <option value='Inactive'>Inactive</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!--/row-->
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Update</button>
                        <button type="button" class="btn default" onclick='detailscancel()'>Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->


<script>


//Function to cancel the form

function detailscancel()
{

    document.qualificationform.action = '/mythriop/qualificationcancel';
    document.qualificationform.submit();
}

</script>



@stop
