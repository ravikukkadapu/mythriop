
@extends('layout/hospheadfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif
<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Patient Op Card Registration
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form name="opregistrationform" method="post" action="/mythriop/addopdata" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12" align="center">
                                <img style='height:100px; width:125px; border:1px solid black' id='Preview1' name='Preview1'>
                           </div>
                           <div class="col-md-12" align="center">
                                <label for="photoid" class='btn default' align='center'>SELECT</label>
                                <input style="display: none;" type="file" id="photoid" name='photoid' value='2' onchange="change1()">
                           </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Card No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id='cardno' name='cardno' autocomplete=off  onchange='getallpatientdata()' value='{{Input::old('cardno')}}'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Service Type</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="servicetype" id="servicetype" onchange='getamount()'>
                                <option value='' >Select</option>
                                    @foreach($servicetypes as $val)
                                        <option value='{{$val->stm_servicetype_vc}}' >{{$val->stm_servicetype_vc}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Op Fees</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="opfees" id="opfees"  value="{{Input::old('opfees')}}" autocomplete=off readonly/>
                            </div>
                        </div>


                    </div>
                    <!--/span-->
                </div>
                <!--/row-->



                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Amount</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" id='amount' name='amount' value= "{{Input::old('amount')}}" autocomplete=off  readonly/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Issue Date</label>
                            <div class="col-md-9">
                            <input type="date"  class="form-control input-sm" name="issuedate" id="issuedate" size="30" value="{{Input::old('issuedate')}}" autocomplete='off' onchange='setyear()'/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Valid Upto</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="validupto" id="validupto"  value="{{Input::old('validupto')}}" autocomplete=off readonly/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Age</label>
                            <div class="col-md-6">
                            <input type="text" class="form-control input-sm" name="age" id="age" size="30" value="{{Input::old('age')}}" autocomplete=off>
                            </div>
                            <label class="control-label col-md-3"> in Years</label>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Gender</label>
                            <div class="col-md-9">
                                <select name='sex' id='sex' class="form-control input-sm">
                                        <option value='{{Input::old('sex')}}'>{{Input::old('sex')}}</option>
                                        <option value='Male'>Male</option>
                                        <option value='Female'>Female</option>
                                        {{-- <option value='Both'>Both</option> --}}
                                    </select>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-2">
                               <select class="form-control input-sm" name="sufix" id="sufix" >
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Dr">Dr</option>
                                </select>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control input-sm" id='name' name='name'>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Contact No</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="contactNo" id="contactNo" maxlength="10" value="{{Input::old('contactNo')}}" autocomplete=off/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Enrolled By</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="issuedby" id="issuedby"  value="{{Input::old('issuedby')}}" autocomplete=off readonly/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Mail Id</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control input-sm" name="mailid" id="mailid"  value="{{Input::old('mailid')}}" autocomplete=off/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="remarks" id="remarks" size="30"  autocomplete='off' >
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Marketed By</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control input-sm" name="marketedBy" id="marketedBy" value='{{Input::old('marketedBy')}}'autocomplete=off/>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->


                 <h3 class="form-section">Address</h3>
                                                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Country</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="country" id="country">
                                    <option value="{{ Input::old('country') }}">{{ Input::old('country') }}</option>
                                    <option value='India' selected>India</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">State</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="state" id="state" onchange = 'getDistrict()' >
                                    <option value='{{ Input::old('state') }}'>{{ Input::old('state') }}</option>
                                    <option value='Andhra Pradesh'> Andhra Pradesh</option>
                                    <option value='Telangana'> Telangana</option>
                                    <option value='Karnataka'> Karnataka</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm select2me" id="district" name="district" onchange = 'getdatabytype()'>
                                    <option value="{{Input::old('district')}}"> {{Input::old('district')}}</option>
                                    @foreach($district as $val)
                                        <option value='{{$val->district_name}}'>{{$val->district_name}}</option>
                                    @endforeach
                                </select>
                            </div>
            </div>
        </div>
                                                    <!--/span-->
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">City/Town/Village</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-sm" name="city" id="city">
                </div>
            </div>
        </div>
        <input type='hidden' value='{{$associate}}' id='assname' name='assname'>
                                                    <!--/span-->
    </div>
                                                <!--/row-->
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-3">Area</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-sm" name="area" id="area">
                    </div>
                </div>
            </div>
                                                    <!--/span-->
            <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Street</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control input-sm" name="street" id="street"/>
                                </div>
                            </div>
                        </div>
                                                    <!--/span-->
                    </div>
                                                <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Houseno</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="houseNo" id="houseNo">
                                    </div>
                                </div>
                            </div>
                                                    <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Pincode</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="pincode" id="pincode" />
                                    </div>
                                </div>
                            </div>
                                                    <!--/span-->
                        </div>
                                                <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Landmark</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" name="landMark" id="landMark">
                                    </div>
                                </div>
                            </div>
                                                    <!--/span-->
                        </div>
                                                <!--/row-->
                        <div class="row" align="center">
                        <input  type='checkbox' id='aggrement' name='aggrement'>
                    <a href='#' data-toggle="modal" data-target="#terms" ><b style='font-size:15px'>Terms & conditions</b> </a></div>



            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>

                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif


<div class="modal fade" id="terms" role="dialog">
                                    <div class="modal-dialog modal-lg">

                                    <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h3>Terms And Conditions</h3>
                                                <button type="button" class="close" data-dismiss="modal"></button>
                                            </div>
                                            <div class="modal-body">
                                            <p>All Lists of Terms and conditions to be added.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#" class="btn btn-default" onclick='accept()' name="delete" id="ok">I Accept</a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Decline</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/servicetype_autosuggest.js"></script>
<script src="/mythriop/style/js/city_autosuggest.js"></script>

    <script>
var i=0;
    function downloadUrl(url, callback)
    {
        $.getJSON(url, function(data)
        {
            callback(data);
        });
    }


    function HandleBrowseClick1()
    {
        var fileinput = document.getElementById("photoid");
        fileinput.click();
    }
function accept()
{
    document.getElementById('aggrement').checked ='true';
// $..close();
$('#terms').modal('hide');
}
function change1()
{
var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("photoid").files[0]);

    oFReader.onload = function(oFREvent) {
      document.getElementById("Preview1").src = oFREvent.target.result;
      // alert(oFREvent.target.result);
      var filePath =oFREvent.target.results;
            console.log(filePath);
    };
    HandleBrowseClick1();
}

    function editopdata()
    {
        i=1;
    // document.getElementById("cardno").disabled = false;
    document.getElementById("Delete").disabled = false;

    }

    function cancel()
    {
    document.opregistrationform.action = "{{ URL::to('/mythriop/opregcancel')}}";
    document.opregistrationform.submit();
    }

function getallpatientdata()
{
    var cardno = (document.getElementById('cardno').value).toUpperCase();
    var form = document.opregistrationform;
    if(i==1)
    {


    var url = '/mythriop/editopcard/' + cardno ;
        downloadUrl(url, function(data)
            {
form.opfees.value = data[0].op_opfees_fl;
form.Name.value = data[0].op_patientname_vc;
form.Preview1.src = data[0].op_photoid_vc;
form.sufix.value = data[0].op_nameprefix_vc;
form.age.value = data[0].op_age_in;
form.sex.value = data[0].op_sex_vc;
form.mailid.value = data[0].op_mailid_vc;
form.contactNo.value = data[0].op_contactno_vc;
form.issuedate.value = data[0].op_date_dt;
form.validupto.value = data[0].op_validupto_dt;
form.amount.value = data[0].op_amount_fl;
form.issuedby.value = data[0].op_issuedby_vc;
form.servicetype.value = data[0].op_schmetype_vc;
form.remarks.value = data[0].op_remarks_vc;
form.country.value = data[0].op_country_vc;
form.state.value = data[0].op_state_vc;
form.district.value = data[0].op_district_vc;
form.city.value = data[0].op_city_vc;
form.area.value = data[0].op_area_vc;
form.street.value = data[0].op_street_vc;
form.houseNo.value = data[0].op_houseno_vc;
form.pincode.value = data[0].op_pincode_vc;
form.landMark.value = data[0].op_landmark_vc;
form.marketedBy.value = data[0].op_marketedby_vc;

            });
}
else
{
    var url = '/mythriop/getopcard/' + cardno ;

    downloadUrl(url, function(data)
    {
        if(data.length == 0)
            {
            alert("Entered Card No is Wrong, Enter Correct Card Number.")
            }
        else
        {
            // console.log(data.cd_status_vc)
            if(data[0].cd_status_vc=='Not Sold')
            {
            form.issuedby.value = data[0].cd_agentname_vc;
            form.servicetype.value = data[0].cd_servicetype_vc;
            getamount()
            }
            else
            {
            alert("Entered Card No is Already Registered, Please Check Card Number.")
            }
        }
    });

}
}



function getprint()
{
    var form = document.opregistrationform;
    var cardno = (document.getElementById('cardno').value).toUpperCase();
    form.action = "/mythriop/opcard/" + cardno;
    form.submit();
}

function getDistrict()
{
var state = document.getElementById('state').value;
var url = '/mythriop/district/' + state;
downloadUrl(url,function(data)
{
var district = data;
var districtData = '';

$.each(district, function(index,data)
{
districtData += '<option value="'+data.district_name+'">'+data.district_name+'</option>';
});

$('#district').html(districtData);
});

}

function setyear()
{
    var date = new Date(document.getElementById('issuedate').value);
    date.setFullYear(date.getFullYear() +1);
    var valid =  date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
    document.getElementById("validupto").value = valid;
}

function getamount()
{
    var servicetype = document.getElementById("servicetype").value;
    var assname = document.getElementById("assname").value;
    var form = document.opregistrationform;
    var url = '/mythriop/serviceopamount/' + servicetype +'/'+ assname;
    console.log(url);
        downloadUrl(url, function(data)
            {
                console.log(data);
                // form.amount.value= data[0].at_cost_fl;
                // form.opfees.value = data[0].at_regcharges_fl;
                form.amount.value= data[0].stm_cost_fl;
                form.opfees.value = data[0].stm_opfees_fl;
            });
}


function exit()
{
    document.opregistrationform.action = "{{ URL::to('/mythriop/exitamsd') }}";
    document.opregistrationform.submit();
}

function getheadname()
{
    var headcardno = (document.getElementById('headcardno').value).toUpperCase();
    var form = document.opregistrationform;
    var url = '/mythriop/getheadname/' + headcardno ;
    console.log(url);
        downloadUrl(url, function(data)
            {
                console.log(data);
                form.headname.value= data[0].op_headname_vc;
            });
}

function getheadcardno()
{
    var headname = document.getElementById("headname").value;
    var form = document.opregistrationform;
    var url = '/mythriop/getheadcardno/' + headname ;
    console.log(url);
        downloadUrl(url, function(data)
            {
                console.log(data);
                form.headcardno.value= data[0].op_headcardno_vc;
            });
}

    </script>


@stop
