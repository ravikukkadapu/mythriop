@extends('layout/headfoot')
@section('content')
 @if(Session::has('message'))
                <div class="alert alert-success" >
                    {{ Session::get('message') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
                </div>
            @endif

            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
            Department Facilities  <small>(New/List)</small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Masters</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Department Facility</a>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->

<div class="portlet box red-sunglo">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>Add New
        </div>
        <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body form">
    <!-- BEGIN FORM-->
        <form name="deptfacilitiesform" id="deptfacilitiesform" method="post" action="adddeptfacities" class="form-horizontal">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Department Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="deptname" id="deptname" >
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3"><span class='red'>*</span> Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="deptstatus" id="deptstatus">
                                    <option value='{{Input::old('deptstatus')}}'>{{Input::old('deptstatus')}}</option>
                                    <option value='Active'>Active</option>
                                    <option value='Inactive'>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->


                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="remarks" id="remarks">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn green">Submit</button>
                        <button type="reset" class="btn default">Cancel</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </form>
    </div>
</div>
                    @if ($errors->any())
                        <div class="note note-danger " >
                        <strong>Errors</strong><br>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
<!-- Data Table -->

<div class="row">
    <div class="col-lg-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>List Of Department Facility(s)
                </div>
            <div class="tools">
            <a href="" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th >Department Facility Name</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
           @foreach($data as $val)
                <tr style='text-align:left' >
                    <td><a href='editdeptfacility/{{$val->dfm_deptname_vc}}'>{{$val->dfm_deptname_vc}}</a></td>
                    <td style='text-align:left'>{{$val->dfm_status_vc}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>

{!! HTML::style('mythriop/style/css/global.css') !!}
        <script src="/mythriop/style/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/mythriop/style/js/departmentfacitiesname_autosuggest.js"></script>


<script>

function detailscancel()
{

    document.deptfacilitiesform.action = '/mythriop/deptfacilitycancel';
    document.deptfacilitiesform.submit();
}

</script>


@stop
